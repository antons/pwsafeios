// Generated using Sourcery 0.15.0 — https://github.com/krzysztofzablocki/Sourcery
// DO NOT EDIT

// swiftlint:disable identifier_name type_name
infix operator *~: MultiplicationPrecedence
infix operator |>: AdditionPrecedence

struct Lens<Whole, Part> {
    let get: (Whole) -> Part
    let set: (Part, Whole) -> Whole
}

func * <A, B, C> (lhs: Lens<A, B>, rhs: Lens<B, C>) -> Lens<A, C> {
    return Lens<A, C>(
        get: { a in rhs.get(lhs.get(a)) },
        set: { (c, a) in lhs.set(rhs.set(c, lhs.get(a)), a) }
    )
}

func *~ <A, B> (lhs: Lens<A, B>, rhs: B) -> (A) -> A {
    return { a in lhs.set(rhs, a) }
}

func |> <A, B> (x: A, f: (A) -> B) -> B {
    return f(x)
}

func |> <A, B, C> (f: @escaping (A) -> B, g: @escaping (B) -> C) -> (A) -> C {
    return { g(f($0)) }
}

extension SyncedStorageOperation {
  enum lens {
      static let action = Lens<SyncedStorageOperation, AsyncOperation>(
        get: { $0.action },
        set: { action, syncedstorageoperation in
           SyncedStorageOperation(action: action, state: syncedstorageoperation.state)
        }
      )
      static let state = Lens<SyncedStorageOperation, State>(
        get: { $0.state },
        set: { state, syncedstorageoperation in
           SyncedStorageOperation(action: syncedstorageoperation.action, state: state)
        }
      )
  }
}
