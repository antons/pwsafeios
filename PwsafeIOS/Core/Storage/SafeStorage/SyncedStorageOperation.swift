//
//  SyncedStorageOperation.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 19/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct SyncedStorageOperation: Equatable, AutoLenses {
    enum State: Equatable {
        case new
        case inProgress
        case completed
        case failed(AppError)
    }
    
    let action: AsyncOperation
    var state: State
}
