//
//  OperationManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 25/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct OperationManager: Equatable {
    private(set) var operations: [OperationId: SyncedStorageOperation]
    
    mutating func add(id: OperationId, operation: AsyncOperation) {
        operations[id] = SyncedStorageOperation(action: operation, state: .new)
    }
    
    mutating func complete(id: OperationId) {
        operations[id]?.state = .completed
    }
    
    mutating func failed(id: OperationId, error: AppError) {
        operations[id]?.state = .failed(error)
    }
}

extension OperationManager {
    static func reduce(_ state: OperationManager, _ action: AsyncOperationAction) -> OperationManager {
        var newState = state
        
        switch action {
        case .newOperation(let operationId, let operation):
            newState.add(id: operationId, operation: operation)
            
        case .completeOperation(let operationId, let completion):
            switch completion {
            case .failed(let error):
                newState.failed(id: operationId, error: error)
                
            case .addFileCompleted((let type, let file)):
                newState.complete(id: operationId)
                
            default:
                break
            }
            
        default:
            break
        }
        
        return newState
    }
}
