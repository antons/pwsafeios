//
//  SyncedFile.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 5/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

typealias SyncedFileId = String

struct SyncedFile: Equatable {
    let id: SyncedFileId
    let name: String
    let syncState: FileSyncState
    let linkState: FileLinkState
}

extension SyncedFile: Hashable {
    var hashValue: Int {
        return id.hashValue
    }
}

extension SyncedFile: Comparable {}

func < (lhs: SyncedFile, rhs: SyncedFile) -> Bool {
    return lhs.name < rhs.name
}

extension SyncedFile: CustomStringConvertible {
    var description: String {
        return name
    }
}
