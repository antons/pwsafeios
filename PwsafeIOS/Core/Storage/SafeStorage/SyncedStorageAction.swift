//
//  SyncedStorageAction.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct AddFileParams: Equatable {
    let storageFile: FileEntry
    let storageType: FileStorageType
    let data: Data
    let metadata: FileMetadata?
}

typealias OperationId = UUID
typealias AppResult<R> = Result<R, AppError>
typealias StoredSyncedFile = (FileStorageType, SyncedFile)

enum AsyncOperation: Equatable {
    case addFile(AddFileParams)
    case updateFile(SyncedFileId, data: Data)
    case removeFile(SyncedFileId)
}

enum AsyncOperationCompletion {
    case addFileCompleted(StoredSyncedFile)
    case updateFileCompleted(StoredSyncedFile)
    case removeFileCompleted(SyncedFileId)
    case failed(AppError)
}

enum AsyncOperationAction {
    case newOperation(OperationId, AsyncOperation)
    case operationStarted(OperationId)
    case completeOperation(OperationId, AsyncOperationCompletion)
    case failOperation(OperationId, AppError)
    case removeOperation(OperationId)
}

enum SyncedStorageAction {
    case operation(AsyncOperationAction)

    case filesUpdated([FileStorageType: [SyncedFile]])

    case markAsRemotelyUnlinked(fileId: SyncedFileId)
    
    case syncedToRemoteStorage(fileId: SyncedFileId,
                               syncedLocalVersion: LocalVersion,
                               syncedMetadata: FileMetadata?)
    
    case markAsConflict(fileId: SyncedFileId, remoteData: FileData)
    case updateFromRemoteStorage(fileId: SyncedFileId, remoteData: FileData)
}
