//
//  SyncedStorageState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct SyncedStorageState: Equatable {
    var files: SyncedFileManager
    
    var operations: OperationManager
}

extension SyncedStorageState {
    init(files: [FileStorageType: [SyncedFile]] = [:], operations: [OperationId: SyncedStorageOperation] = [:]) {
        self.files = SyncedFileManager(files: files)
        self.operations = OperationManager(operations: operations)
    }
}

extension SyncedStorageState {
    static func reduce(
        _ state: SyncedStorageState, _ action: SyncedStorageAction) -> SyncedStorageState {

        var newState = state

        switch action {
        case .operation(let operationAction):
            newState.operations = OperationManager.reduce(state.operations, operationAction)
        
        default:
            break
        }

        switch action {
        case .operation(.completeOperation(let operationId, let completion)):
            switch completion {
            case .addFileCompleted((let type, let file)):
                newState.files.addFile(file, of: type)
                
            default:
                break
            }
            
        default:
            break
        }
        
        return newState
    }
}

func == (lhs: [FileStorageType: [SyncedFile]], rhs: [FileStorageType: [SyncedFile]]) -> Bool {
    guard lhs.count == rhs.count else { return false }
    
    for (leftKey, rightKey) in zip(lhs.keys, rhs.keys) {
        guard leftKey == rightKey else { return false }
        guard compare(lhs: lhs[leftKey], rhs: rhs[rightKey], compare: ==)  else { return false }
    }
    
    return true
}
