//
//  SyncedFileStorage.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import PwsafeSwift


protocol SyncedFileStorageProtocol: class {
    var files: Property<[FileStorageType: [SyncedFile]]> { get }

    func addFile(_ storageFile: FileEntry,
                 storageType: FileStorageType,
                 data: Data,
                 metadata: FileMetadata?) -> SignalProducer<SyncedFile, AppError>

    func updateFile(_ fileId: SyncedFileId, data: Data) -> SignalProducer<Void, AppError>

    func removeFile(_ id: SyncedFileId) -> SignalProducer<Void, AppError>

    func fileDataById(_ id: SyncedFileId) -> SignalProducer<Data, AppError>
}

// TODO: NAME @#$?
protocol FileStorageSyncApplier: class {
    func findSyncedFileId(forFile file: FileEntry,
                          storageType: FileStorageType) -> SignalProducer<SyncedFileId, AppError>

    func markAsRemotelyUnlinked(_ fileId: SyncedFileId) -> SignalProducer<Void, AppError>

    func syncedToRemoteStorage(_ fileId: SyncedFileId,
                               syncedLocalVersion: LocalVersion,
                               syncedMetadata: FileMetadata?) -> SignalProducer<Void, AppError>

    func markAsConflict(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError>

    func updatedFromRemoteStorage(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError>
}

class SyncedFileStorage: SyncedFileStorageProtocol {
    let files: Property<[FileStorageType: [SyncedFile]]>

    fileprivate let persistenceService: PersistenceServiceProtocol

    fileprivate let changesSignal: Signal<LocalFileChange, AppError>
    private let changesObserver: Signal<LocalFileChange, AppError>.Observer

    init(persistenceService: PersistenceServiceProtocol) {
        self.persistenceService = persistenceService

        (changesSignal, changesObserver) = Signal.pipe()

        self.files = persistenceService.files.map { files in
            return files
                .group(by: { $0.storageType })
                .valuesMap(SyncedFile.init(record:))
        }
    }

    func addFile(_ storageFile: FileEntry,
                 storageType: FileStorageType,
                 data: Data,
                 metadata: FileMetadata?) -> SignalProducer<SyncedFile, AppError> {
        let syncMetadata = FileSyncMetadata(currentVersion: 1, syncedVersion: 1, syncedMetadata: metadata)
        let syncedFileRecord = SyncedFileRecord(id: UUID().uuidString,
                                    name: storageFile.name,
                                    data: data,
                                    storageType: storageType,
                                    storageFile: storageFile,
                                    linkState: .linked,
                                    syncState: .synced,
                                    syncMetadata: syncMetadata)

        let syncedFile = SyncedFile(record: syncedFileRecord)

        return ensureUnique(for: storageFile, storageType: storageType)
            .then(persistenceService.createRecord(syncedFileRecord))
            .then(SignalProducer<SyncedFile, AppError>(value: syncedFile))
    }

    private func ensureUnique(for file: FileEntry, storageType: FileStorageType) -> SignalProducer<Void, AppError> {
        return fileExists(forFile: file, storageType: storageType)
            .flatMap(.latest) { (exists: Bool) -> SignalProducer<Void, AppError> in
                guard exists else { return .empty }
                return SignalProducer(error: AppError.safeAlreadyExists(name: file.name))
        }
    }

    func updateFile(_ fileId: SyncedFileId, data: Data) -> SignalProducer<Void, AppError> {
        return persistenceService.fetchRecord(fileId)
            .map(updateSyncedFileRecord(data))
            .flatMap(.latest) { updatedSyncedFileRecord in
                return self.persistenceService
                    .update(updatedSyncedFileRecord)
                    .then(SignalProducer<SyncedFileRecord, AppError>(value: updatedSyncedFileRecord))
            }
            .on(value: { self.changesObserver.send(value: LocalFileChange(syncedFileRecord: $0)) })
            .then(SignalProducer<Void, AppError>.empty)
    }

    func removeFile(_ id: SyncedFileId) -> SignalProducer<Void, AppError> {
        return persistenceService.remove(id)
    }

    func fileDataById(_ id: SyncedFileId) -> SignalProducer<Data, AppError> {
        return persistenceService
            .fetchRecord(id).map({ return $0.data })
    }

    fileprivate func fileExists(forFile file: FileEntry,
                                storageType: FileStorageType) -> SignalProducer<Bool, AppError> {
        return persistenceService.list()
            .map { records in
                return records
                    .filter(isSameFile(file, storageType: storageType))
                    .count > 0
            }
    }
}

extension SyncedFileStorage: LocalFileChangeSource {
    func localChanges() -> SignalProducer<LocalFileChange, AppError> {
        return persistenceService.list()
            // can be replaced with flatten?
            .flattenArray(.latest)
            .filter(needsSyncing)
            .map(LocalFileChange.init)
            .concat(SignalProducer(changesSignal))
    }
}

private typealias SyncedFileRecordModifier = (SyncedFileRecord) -> SyncedFileRecord

extension SyncedFileStorage: FileStorageSyncApplier {

    private func findRecord(forFile file: FileEntry, storageType: FileStorageType) -> SignalProducer<SyncedFileRecord, AppError> {
        return persistenceService.list()
            .flatMap(.latest) { records in
                return records
                    .filter(isSameFile(file, storageType: storageType))
                    .first
                    .value(errorIfNil: AppError.safeDoesNotExist(name: file.name))
        }
    }

    func findSyncedFileId(forFile file: FileEntry, storageType: FileStorageType) -> SignalProducer<SyncedFileId, AppError> {
        return findRecord(forFile: file, storageType: storageType)
            .map({ $0.id })
    }

    private func updateRecord(_ fileId: SyncedFileId, modifier: @escaping SyncedFileRecordModifier) -> SignalProducer<Void, AppError> {
        return persistenceService.fetchRecord(fileId)
            .map(modifier)
            .flatMap(.latest, self.persistenceService.update)
    }

    func markAsRemotelyUnlinked(_ fileId: SyncedFileId) -> SignalProducer<Void, AppError> {
        return updateRecord(fileId, modifier: markAsRemotelyUnlinkedModifier)
    }

    func syncedToRemoteStorage(_ fileId: SyncedFileId,
                               syncedLocalVersion: LocalVersion,
                               syncedMetadata: FileMetadata?) -> SignalProducer<Void, AppError> {
        return updateRecord(fileId, modifier:
            processFileSyncedToRemoteStorage(syncedLocalVersion, syncedMetadata: syncedMetadata))
    }

    func markAsConflict(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError> {
        return updateRecord(fileId, modifier: markAsConflictModifier(remoteData))
    }

    func updatedFromRemoteStorage(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError> {
        return updateRecord(fileId, modifier: updateFromRemoteStorageModifier(remoteData))
    }
}

private func updateSyncedFileRecord(_ data: Data) -> (SyncedFileRecord) -> SyncedFileRecord {
    return { original in
        var updated = original
        updated.data = data
        updated.syncState = .notSynced
        updated.syncMetadata.currentVersion += 1
        return updated
    }
}

private func markAsRemotelyUnlinkedModifier(_ syncedFileRecord: SyncedFileRecord) -> SyncedFileRecord {
    var updatedRecord = syncedFileRecord
    updatedRecord.linkState = .remotelyUnlinked
    return updatedRecord
}

private func processFileSyncedToRemoteStorage(_ syncedLocalVersion: LocalVersion, syncedMetadata: FileMetadata?) -> SyncedFileRecordModifier {
    return { syncedFileRecord in
        var updatedRecord = syncedFileRecord

        if syncedFileRecord.syncMetadata.currentVersion == syncedLocalVersion {
            updatedRecord.syncState = .synced
        }

        updatedRecord.syncMetadata.syncedVersion = syncedLocalVersion
        updatedRecord.syncMetadata.syncedMetadata = syncedMetadata

        return updatedRecord
    }
}

private func markAsConflictModifier(_ remoteData: FileData) -> SyncedFileRecordModifier {
    return { syncedFileRecord in
        var updatedRecord = syncedFileRecord

        updatedRecord.syncState = .conflict
        updatedRecord.syncMetadata.remoteFileData = remoteData

        return updatedRecord
    }
}

private func updateFromRemoteStorageModifier(_ remoteData: FileData) -> SyncedFileRecordModifier {
    return { syncedFileRecord in
        var updatedRecord = syncedFileRecord

        if syncedFileRecord.syncState == .synced {
            updatedRecord.data = remoteData.data
            updatedRecord.syncMetadata.currentVersion += 1
            updatedRecord.syncMetadata.syncedVersion += 1
            updatedRecord.syncMetadata.syncedMetadata = remoteData.metadata
            updatedRecord.syncMetadata.remoteFileData = nil
        } else {
            updatedRecord.syncState = .conflict
            updatedRecord.syncMetadata.remoteFileData = remoteData
        }

        return updatedRecord
    }
}

private func needsSyncing(_ record: SyncedFileRecord) -> Bool {
    return record.linkState == .linked && record.syncState == .notSynced
}

private func isSameFile(_ file: FileEntry, storageType: FileStorageType) -> (SyncedFileRecord) -> Bool {
    return { record in
        return record.storageFile == file && record.storageType == storageType
    }
}

private extension SyncedFile {
    init(record: SyncedFileRecord) {
        self.init(
            id: record.id,
            name: record.name,
            syncState: record.syncState,
            linkState: record.linkState)
    }
}
