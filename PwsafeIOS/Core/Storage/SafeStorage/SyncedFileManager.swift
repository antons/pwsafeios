//
//  SyncedFileManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 25/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct SyncedFileManager: Equatable {
    var files: [FileStorageType: [SyncedFile]]
    
    mutating func addFile(_ file: SyncedFile, of type: FileStorageType) {
        var list = files[type] ?? []
        list.append(file)
        files[type] = list
    }
}
