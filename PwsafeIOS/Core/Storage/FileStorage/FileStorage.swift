//
//  FileStorage.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

enum FileStorageType: Int {
    case local
    case dropbox
    case icloud
}

enum FileStorageError: Error {
    case `internal`(Error?)
    case fileNotFound(FileEntry)
    case fileAlreadyExists(FileEntry)

    static var unknownError: FileStorageError = .internal(nil)
}

enum FileUpdateResult {
    case success(newMetadata: FileMetadata?)
    case conflict
}

protocol FileStorage: class {
    var type: FileStorageType { get }

    func fetchData(_ file: FileEntry) -> SignalProducer<FileData, FileStorageError>

    func fetchMetadata(_ file: FileEntry) -> SignalProducer<FileMetadata?, FileStorageError>

    func updateFile(_ file: FileEntry, data: FileData) -> SignalProducer<FileUpdateResult, FileStorageError>

    func createFile(_ path: String, data: Data) -> SignalProducer<FileEntry, FileStorageError>
}
