//
//  FileEntry.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

struct FileEntry: AutoEquatable {
    /// sourcery: skipEquality
    let name: String
    let path: String

    init(path: String) {
        self.init(path: path, name: path)
    }

    init(path: String, name: String) {
        self.path = path
        self.name = name.isEmpty ? path : name
    }
}

extension FileEntry: Comparable {}
func < (lhs: FileEntry, rhs: FileEntry) -> Bool {
    return lhs.path < rhs.path
}

extension FileEntry: Hashable {
    var hashValue: Int {
        return path.hashValue
    }
}
