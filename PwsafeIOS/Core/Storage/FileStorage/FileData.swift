//
//  FileData.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

typealias FileMetadata = String

struct FileData: Equatable {
    let data: Data
    let metadata: FileMetadata?

    init(data: Data, metadata: FileMetadata? = nil) {
        self.data = data
        self.metadata = metadata
    }
}
