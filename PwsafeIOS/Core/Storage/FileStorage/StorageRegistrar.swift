//
//  StorageRegistrar.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol StorageProvider {
    func storage(type: FileStorageType) -> FileStorage?

    func fetchStorage(type: FileStorageType) -> SignalProducer<FileStorage, AppError>
}

extension StorageProvider {
    func fetchStorage(type: FileStorageType) -> SignalProducer<FileStorage, AppError> {
        return storage(type: type).value(errorIfNil: AppError.unknownError)
    }
}

protocol StorageRegistrarProtocol: StorageProvider {
    //swiftlint:disable:next variable_name
    var rac_storages: Property<[FileStorage]> { get }

    func register(storage: FileStorage)

    func unregister(storage: FileStorage)
}

class StorageRegistrar: StorageRegistrarProtocol {
    private let storages: MutableProperty<[FileStorage]> = MutableProperty([])

    //swiftlint:disable:next variable_name
    var rac_storages: Property<[FileStorage]> {
        return storages.map({ $0 })
    }

    func storage(type: FileStorageType) -> FileStorage? {
        return storages.value.filter({ $0.type == type }).first
    }

    func register(storage: FileStorage) {
        var newStorages = storages.value.filter({ $0.type != storage.type })
        newStorages.append(storage)
        storages.value = newStorages
    }

    func unregister(storage: FileStorage) {
        let newStorages = storages.value.filter({ $0 === storage })
        storages.value = newStorages
    }
}
