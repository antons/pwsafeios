//
//  StorageAssembly.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import ReactiveSwift

class StorageAssembly: Assembly {
    func assemble(container: Container) {
        container.register(StorageRegistrarProtocol.self) { _ in
            StorageRegistrar()
        }.inObjectScope(.container)

        container.register(SyncedFileStorage.self) { resolver in
            return SyncedFileStorage(persistenceService: resolver.resolve(PersistenceServiceProtocol.self)!)
        }.inObjectScope(.container)

        container.register(SyncedFileStorageProtocol.self) { resolver in
            return resolver.resolve(SyncedFileStorage.self)!
        }

        container.register(FileStorageSyncApplier.self) { resolver in
            return resolver.resolve(SyncedFileStorage.self)!
        }

        container.register(LocalFileChangeSource.self) { resolver in
            return resolver.resolve(SyncedFileStorage.self)!
        }

        container.register(CryptoSafeService.self) { resolver in
            return CryptoSafeService(syncedFileStorage: resolver.resolve(SyncedFileStorageProtocol.self)!)
        }.inObjectScope(.container)

        container.register(PersistenceService.self) { _ in
            return PersistenceService(
                fileManager: FileManager(),
                rootFolder: self.createFileStorageFolder(),
                scheduler: QueueScheduler(qos: .default, name: "PersistenceService"))
        }.inObjectScope(.container)

        container.register(PersistenceServiceProtocol.self) { resolver in
            return resolver.resolve(PersistenceService.self)!
        }

        container.register(RemoteFileStateProvider.self) { resolver in
            return resolver.resolve(PersistenceService.self)!
        }
    }

    private func createFileStorageFolder() -> String {
        //todo: This is a hack
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.libraryDirectory, .userDomainMask, true).first!
        let folder = cacheDirectory + "/" + "safestorage"
        try! FileManager.default.createDirectory(atPath: folder, withIntermediateDirectories: true, attributes: nil)
        return folder
    }
}
