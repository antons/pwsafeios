//
//  PersistenceService.swift
//  PwrecordIOS
//
//  Created by Anton Selyanin on 9/3/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol PersistenceServiceProtocol {
    var files: Property<[SyncedFileRecord]> { get }

    func list() -> SignalProducer<[SyncedFileRecord], AppError>

    func fetchRecord(_ id: String) -> SignalProducer<SyncedFileRecord, AppError>

    func createRecord(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError>

    func update(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError>

    func remove(_ id: String) -> SignalProducer<Void, AppError>
}

//TODO: rename!
struct RemoteFileState {
    let file: FileEntry
    let storageType: FileStorageType
    let metadata: FileMetadata?
}

//TODO: rename!
protocol RemoteFileStateProvider {
    func listStates() -> SignalProducer<[RemoteFileState], AppError>
}

class PersistenceService: PersistenceServiceProtocol {
    var files: Property<[SyncedFileRecord]> {
        return filesProperty.map({ $0 })
    }
    private let filesProperty: MutableProperty<[SyncedFileRecord]> = MutableProperty([])

    private let fileManager: FileManagerProtocol
    private let rootFolder: String
    private let scheduler: Scheduler

    init(fileManager: FileManagerProtocol, rootFolder: String, scheduler: Scheduler) {
        self.fileManager = fileManager
        self.rootFolder = rootFolder
        self.scheduler = scheduler

        reloadFiles()
    }

    private func reloadFiles() {
        list()
            .on(failed: { assertionFailure("loading files failed with error: \($0)") })
            .ignoreError()
            .on(value: { [unowned self] in
                self.filesProperty.value = $0
            })
            .start(on: scheduler)
            .start()
    }

    func fetchRecord(_ id: String) -> SignalProducer<SyncedFileRecord, AppError> {
        return fileManager.fetchData(fromRoot(id))
            .mapError(AppError.internalError)
            .flatMap(.concat, syncedFileRecordFromData)
            .start(on: scheduler)
    }

    //TODO: this is kind of redundant, but I need a failable way to load files
    func list() -> SignalProducer<[SyncedFileRecord], AppError> {
        return fileManager.listFiles(rootFolder)
            .flatMap(.concat) { [unowned self] files in
                return self.readFiles(files)
            }
            .map({ $0.compactMap(SyncedFileRecord.init) })
            .mapError(AppError.internalError)
    }

    private func readFiles(_ files: [String]) -> SignalProducer<[Data], FileManagerError> {
        return SignalProducer<SignalProducer<Data, FileManagerError>, FileManagerError>(files.map(self.fileManager.fetchData))
            .flatten(.concat).collect()
            .start(on: scheduler)
    }

    func createRecord(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError> {
        return fileManager.create(fromRoot(record.id), data: record.serialize())
            .mapError(AppError.internalError)
            .on(completed: self.reloadFiles)
            .start(on: scheduler)
    }

    func update(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError> {
        return fileManager.update(fromRoot(record.id), data: record.serialize())
            .mapError(AppError.internalError)
            .on(completed: self.reloadFiles)
            .start(on: scheduler)
    }

    func remove(_ id: String) -> SignalProducer<Void, AppError> {
        return fileManager.remove(fromRoot(id))
            .mapError(AppError.internalError)
            .on(completed: self.reloadFiles)
            .start(on: scheduler)
    }

    private func fromRoot(_ path: String) -> String {
        return rootFolder + "/" + path
    }
}

extension PersistenceService: RemoteFileStateProvider {
    func listStates() -> SignalProducer<[RemoteFileState], AppError> {
        return list().map({ $0.map(createRemoteFileState) })
    }
}

private func syncedFileRecordFromData(_ data: Data) -> SignalProducer<SyncedFileRecord, AppError> {
    return SyncedFileRecord(data: data).value(errorIfNil: .unknownError)
}

private func createRemoteFileState(_ record: SyncedFileRecord) -> RemoteFileState {
    return RemoteFileState(file: record.storageFile,
                           storageType: record.storageType,
                           metadata: record.syncMetadata.syncedMetadata)
}
