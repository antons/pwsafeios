//
//  FileSyncMetadata.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

typealias LocalVersion = Int

struct FileSyncMetadata: Equatable {
    var currentVersion: LocalVersion
    var syncedVersion: LocalVersion
    var syncedMetadata: FileMetadata?
    var remoteFileData: FileData?

    init(currentVersion: LocalVersion, syncedVersion: LocalVersion,
         syncedMetadata: FileMetadata?, remoteFileData: FileData? = nil) {
        self.currentVersion = currentVersion
        self.syncedVersion = syncedVersion
        self.syncedMetadata = syncedMetadata
        self.remoteFileData = remoteFileData
    }
}
