//
//  FileLinkState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

enum FileLinkState: Int {
    case linked = 0
    case remotelyUnlinked = 1
    case locallyUnlinked = 2
}
