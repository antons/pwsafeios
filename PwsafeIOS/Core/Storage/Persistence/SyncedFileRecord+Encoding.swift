//
//  SyncedFileRecord+Encoding.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/4/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

protocol Serializable {
    init?(data: Data)

    func serialize() -> Data
}

extension SyncedFileRecord {
    init?(data: Data) {
        guard let coding = NSKeyedUnarchiver.unarchiveObject(with: data) as? SyncedFileRecord.Coding else { return nil }
        self = coding.syncedFileRecord
    }

    func serialize() -> Data {
        return NSKeyedArchiver.archivedData(withRootObject: SyncedFileRecord.Coding(syncedFileRecord: self))
    }
}

extension SyncedFileRecord {
    @objc(_TtCV9PwsafeIOS16SyncedFileRecord6Coding)class Coding: NSObject, NSCoding {
        static let codingVersion: Int = 1
        static let codingVersionKey: String = "versionKey"

        static let idKey: String = "idKey"
        static let nameKey: String = "nameKey"

        static let dataKey: String = "dataKey"
        static let storageTypeKey: String = "storageTypeKey"
        static let storageFilePathKey: String = "storageFilePathKey"
        static let linkStateKey: String = "linkStateKey"

        static let syncStateKey: String = "syncStateKey"

        static let syncedVersionKey: String = "syncedVersionKey"
        static let serverMetadataKey: String = "serverMetadataKey"
        static let currentVersionKey: String = "currentVersionKey"

        static let remoteConflictDataKey: String = "remoteConflictDataKey"
        static let remoteConflictMetadataKey: String = "remoteConflictMetadataKey"

        let syncedFileRecord: SyncedFileRecord

        init(syncedFileRecord: SyncedFileRecord) {
            self.syncedFileRecord = syncedFileRecord
        }

        required init?(coder aDecoder: NSCoder) {
            guard Coding.codingVersion == aDecoder.decodeInteger(forKey: Coding.codingVersionKey) else { return nil }

            let maybeId = aDecoder.decodeObject(forKey: Coding.idKey) as? String
            let maybeName = aDecoder.decodeObject(forKey: Coding.nameKey) as? String
            let maybeData = aDecoder.decodeObject(forKey: Coding.dataKey) as? Data
            let maybeStorageType = FileStorageType(rawValue: aDecoder.decodeInteger(forKey: Coding.storageTypeKey))
            let maybeStorageFilePath = aDecoder.decodeObject(forKey: Coding.storageFilePathKey) as? String
            let linkStateRawValue = aDecoder.decodeInteger(forKey: Coding.linkStateKey)
            let syncStateRawValue = aDecoder.decodeInteger(forKey: Coding.syncStateKey)
            let serverMetadata = aDecoder.decodeObject(forKey: Coding.serverMetadataKey) as? FileMetadata
            let syncedVersion = aDecoder.decodeInteger(forKey: Coding.syncedVersionKey)
            let currentVersion = aDecoder.decodeInteger(forKey: Coding.currentVersionKey)

            guard let id = maybeId,
                let name = maybeName,
                let data = maybeData,
                let storageType = maybeStorageType,
                let storageFilePath = maybeStorageFilePath,
                let linkState = FileLinkState(rawValue: linkStateRawValue),
                let syncState = FileSyncState(rawValue: syncStateRawValue)
                else { return nil }

            let remoteFileData: FileData? = {
                guard let remoteConflictData = aDecoder.decodeObject(forKey: Coding.remoteConflictDataKey) as? Data,
                    let remoteConflictMetadata = aDecoder.decodeObject(forKey: Coding.remoteConflictMetadataKey) as? FileMetadata
                    else { return nil }
                return FileData(data: remoteConflictData, metadata: remoteConflictMetadata)
            }()

            let syncMetadata = FileSyncMetadata(currentVersion: currentVersion,
                                          syncedVersion: syncedVersion,
                                          syncedMetadata: serverMetadata,
                                          remoteFileData: remoteFileData)

            self.syncedFileRecord = SyncedFileRecord(id: id, name: name, data: data,
                                         storageType: storageType, storageFile: FileEntry(path: storageFilePath),
                                         linkState: linkState,
                                         syncState: syncState,
                                         syncMetadata: syncMetadata)
        }

        func encode(with aCoder: NSCoder) {
            aCoder.encode(Coding.codingVersion, forKey: Coding.codingVersionKey)
            aCoder.encode(syncedFileRecord.id, forKey: Coding.idKey)
            aCoder.encode(syncedFileRecord.name, forKey: Coding.nameKey)

            aCoder.encode(syncedFileRecord.data, forKey: Coding.dataKey)
            aCoder.encode(syncedFileRecord.storageType.rawValue, forKey: Coding.storageTypeKey)
            aCoder.encode(syncedFileRecord.storageFile.path, forKey: Coding.storageFilePathKey)
            aCoder.encode(syncedFileRecord.linkState.rawValue, forKey: Coding.linkStateKey)
            aCoder.encode(syncedFileRecord.syncState.rawValue, forKey: Coding.syncStateKey)

            aCoder.encode(syncedFileRecord.syncMetadata.syncedMetadata, forKey: Coding.serverMetadataKey)
            aCoder.encode(syncedFileRecord.syncMetadata.currentVersion, forKey: Coding.currentVersionKey)
            aCoder.encode(syncedFileRecord.syncMetadata.syncedVersion, forKey: Coding.syncedVersionKey)

            aCoder.encode(syncedFileRecord.syncMetadata.remoteFileData?.data, forKey: Coding.remoteConflictDataKey)
            aCoder.encode(syncedFileRecord.syncMetadata.remoteFileData?.metadata, forKey: Coding.remoteConflictMetadataKey)
        }
    }
}
