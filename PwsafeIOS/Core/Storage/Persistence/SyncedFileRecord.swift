//
//  SyncedFileRecord.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/3/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

struct SyncedFileRecord: Equatable {
    let id: String
    let name: String

    var data: Data

    let storageType: FileStorageType
    let storageFile: FileEntry
    var linkState: FileLinkState

    var syncState: FileSyncState
    var syncMetadata: FileSyncMetadata
}
