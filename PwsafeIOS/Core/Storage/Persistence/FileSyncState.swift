//
//  FileSyncState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

enum FileSyncState: Int {
    case synced = 0
    case notSynced = 1
    case conflict = 2
}

extension FileSyncState: CustomStringConvertible {
    var description: String {
        switch self {
        case .synced:
            return "Synced"

        case .notSynced:
            return "Not Synced"

        case .conflict:
            return "Conflict"
        }
    }
}
