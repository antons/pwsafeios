//
//  FileServiceIntegration.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/10/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

protocol FileServiceIntegration {
    var storage: FileStorage { get }

    var changeSource: RemoteFileChangeSource { get }
}
