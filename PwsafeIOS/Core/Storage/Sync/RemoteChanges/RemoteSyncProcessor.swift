//
//  RemoteSyncProcessor.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

class RemoteSyncProcessor {
    private let remoteChangeSource: RemoteFileChangeSource
    private let storageSyncApplier: FileStorageSyncApplier

    init(remoteChangeSource: RemoteFileChangeSource,
         storageSyncApplier: FileStorageSyncApplier) {
        self.remoteChangeSource = remoteChangeSource
        self.storageSyncApplier = storageSyncApplier
    }

    func start() {
        remoteChangeSource.remoteChanges()
            .logEvents(logger: log("RemoteChanges"))
            .flatMap(.merge) { [unowned self] change in
                return self.processRemoteChange(change)
            }
            .start()
    }

    private func processRemoteChange(_ change: RemoteFileChange) -> SignalProducer<Void, AppError> {
        return storageSyncApplier.findSyncedFileId(forFile: change.file, storageType: change.storageType)
            .flatMap(.latest) { [unowned self] (fileId) -> SignalProducer<Void, AppError> in
                switch change.change {
                case .unlinked:
                    return self.storageSyncApplier.markAsRemotelyUnlinked(fileId)

                case .updated(let data):
                    return self.storageSyncApplier.updatedFromRemoteStorage(fileId, remoteData: data)
                }
        }
    }
}
