//
//  RemoteFileChangeSource.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol RemoteFileChangeSource {
    func remoteChanges() -> SignalProducer<RemoteFileChange, AppError>
}
