//
//  RemoteFileChange.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

struct RemoteFileChange: Equatable {
    enum Change: Equatable {
        case unlinked
        case updated(data: FileData)
    }

    let file: FileEntry
    let storageType: FileStorageType
    let change: Change
}

extension RemoteFileChange.Change: CustomStringConvertible {
    var description: String {
        switch self {
        case .unlinked:
            return "RemoteFileChange.Unlinked"

        case .updated:
            return "RemoteFileChange.Updated"
        }
    }
}
