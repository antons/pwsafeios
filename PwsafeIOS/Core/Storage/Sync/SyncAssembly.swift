//
//  SyncAssembly.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject

class SyncAssembly: Assembly {
    func assemble(container: Container) {
        container.register(RemoteSyncProcessor.self) { resolver in
            return RemoteSyncProcessor(
                remoteChangeSource: resolver.resolve(RemoteFileChangeSource.self)!,
                storageSyncApplier: resolver.resolve(FileStorageSyncApplier.self)!)
        }
        .inObjectScope(.container)

        container.register(LocalSyncProcessor.self) { resolver in
            return LocalSyncProcessor(
                localChangeSource: resolver.resolve(LocalFileChangeSource.self)!,
                localChangeProcessor: resolver.resolve(LocalChangeProcessorProtocol.self)!,
                storageSyncApplier: resolver.resolve(FileStorageSyncApplier.self)!)
        }
        .inObjectScope(.container)

        container.register(LocalChangeProcessorProtocol.self) { resolver in
            return LocalChangeProcessor(storageProvider: resolver.resolve(StorageRegistrarProtocol.self)!)
        }
        .inObjectScope(.container)

        container.register(RemoteFileChangeSource.self) { resolver in
            return resolver.resolve(DropboxSyncService.self)!
        }
    }
}
