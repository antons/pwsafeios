//
//  LocalFileChange.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

struct LocalFileChange: Equatable {
    let fileId: String

    let localVersion: LocalVersion
    let data: Data

    let file: FileEntry
    let storageType: FileStorageType
    let syncedFileMetadata: FileMetadata?
}

extension LocalFileChange {
    init(syncedFileRecord: SyncedFileRecord) {
        self.fileId = syncedFileRecord.id
        self.localVersion = syncedFileRecord.syncMetadata.currentVersion
        self.data = syncedFileRecord.data
        self.file = syncedFileRecord.storageFile
        self.storageType = syncedFileRecord.storageType
        self.syncedFileMetadata = syncedFileRecord.syncMetadata.syncedMetadata
    }
}

extension LocalFileChange: CustomStringConvertible {
    var description: String {
        return "\(type(of: self))(fileId: \(fileId), localVersion: \(localVersion), file: \(file), storageType: \(storageType), syncedFileMetadata: \(syncedFileMetadata ?? "<nil>"))"
    }
}
