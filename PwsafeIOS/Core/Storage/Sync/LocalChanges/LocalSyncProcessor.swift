//
//  LocalSyncProcessor.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

//FIXME: use it
class LocalSyncProcessor {
    private let localChangeSource: LocalFileChangeSource
    private let localChangeProcessor: LocalChangeProcessorProtocol
    private let storageSyncApplier: FileStorageSyncApplier

    init(localChangeSource: LocalFileChangeSource,
         localChangeProcessor: LocalChangeProcessorProtocol,
         storageSyncApplier: FileStorageSyncApplier) {
        self.localChangeSource = localChangeSource
        self.localChangeProcessor = localChangeProcessor
        self.storageSyncApplier = storageSyncApplier
    }

    func start() {
        localChangeSource.localChanges()
            .logEvents(logger: log("LocalChanges"))
            .flatMap(.merge) { [unowned self] change in
                self.localChangeProcessor.process(change)
            }
            .flatMap(.merge) { [unowned self] result in
                self.processSyncResult(result)
            }
            .start()
    }

    private func processSyncResult(_ syncResult: FileSyncResult) -> SignalProducer<Void, AppError> {
        switch syncResult {
        case .success(let fileId, let localVersion, let metadata):
            return storageSyncApplier.syncedToRemoteStorage(fileId, syncedLocalVersion: localVersion, syncedMetadata: metadata)

        case .mergeRequired(let fileId, let data):
            return storageSyncApplier.markAsConflict(fileId, remoteData: data)

        case .remotelyUnlinked(let fileId):
            return storageSyncApplier.markAsRemotelyUnlinked(fileId)
        }
    }
}
