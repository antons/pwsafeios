//
//  DataSync.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/17/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol LocalChangeProcessorProtocol {
    func process(_ change: LocalFileChange) -> SignalProducer<FileSyncResult, AppError>
}

class LocalChangeProcessor: LocalChangeProcessorProtocol {
    private let storageProvider: StorageProvider

    init(storageProvider: StorageProvider) {
        self.storageProvider = storageProvider
    }

    func process(_ change: LocalFileChange) -> SignalProducer<FileSyncResult, AppError> {
        guard let storage = storageProvider.storage(type: change.storageType) else {
            return SignalProducer(error: AppError.unknownError)
        }

        let fileData = FileData(data: change.data, metadata: change.syncedFileMetadata)

        return storage.updateFile(change.file, data: fileData)
            .flatMap(.latest, processUpdateResult(change, storage: storage))
            .flatMapError { (error) -> SignalProducer<FileSyncResult, FileStorageError> in
                guard case .fileNotFound = error else {
                    return SignalProducer(error: error)
                }

                return SignalProducer(value: .remotelyUnlinked(fileId: change.fileId))
            }
            .mapError(AppError.internalError)
    }

    private func processUpdateResult(_ change: LocalFileChange, storage: FileStorage) -> (_ updateResult: FileUpdateResult)
            -> SignalProducer<FileSyncResult, FileStorageError> {
                return { updateResult in
                    switch updateResult {
                    case .success(newMetadata: let metadata):
                        return SignalProducer(value: .success(fileId: change.fileId, localVersion: change.localVersion, serverMetadata: metadata))
                    default:
                        return storage.fetchData(change.file)
                            .map { data in
                                return .mergeRequired(fileId: change.fileId, remoteData: data)
                            }
                    }
                }
    }
}
