//
//  FileSyncResult.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

enum FileSyncResult: Equatable {
    case success(fileId: SyncedFileId, localVersion: LocalVersion, serverMetadata: FileMetadata?)
    case mergeRequired(fileId: SyncedFileId, remoteData: FileData)
    case remotelyUnlinked(fileId: SyncedFileId)
}
