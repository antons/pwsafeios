//
//  OpenSafeInteractor.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import PwsafeSwift

protocol CryptoSafeServiceProtocol {
    func loadSafe(from syncedFile: SyncedFile, password: String) -> SignalProducer<DecryptedSafe, AppError>

    func storeSafe(_ safe: DecryptedSafe) -> SignalProducer<Void, AppError>
}

class CryptoSafeService: CryptoSafeServiceProtocol {
    let syncedFileStorage: SyncedFileStorageProtocol

    init(syncedFileStorage: SyncedFileStorageProtocol) {
        self.syncedFileStorage = syncedFileStorage
    }

    func loadSafe(from syncedFile: SyncedFile, password: String) -> SignalProducer<DecryptedSafe, AppError> {
        return syncedFileStorage.fileDataById(syncedFile.id)
            .flatMap(.latest) { data in
                return Pwsafe.fromData(data, password: password)
            }.map { pwsafe in
                return DecryptedSafe(syncedFile: syncedFile, pwsafe: pwsafe, password: password)
            }
    }

    func storeSafe(_ safe: DecryptedSafe) -> SignalProducer<Void, AppError> {
        return safe.pwsafe
            .toDataProducer(with: safe.password)
            .flatMap(.latest) { data in
                self.syncedFileStorage.updateFile(safe.syncedFile.id, data: data)
            }
    }
}
