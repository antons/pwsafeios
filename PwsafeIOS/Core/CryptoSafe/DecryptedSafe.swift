//
//  DecryptedSafe .swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift

typealias PwsafeRecord = PwsafeSwift.Record
typealias PwsafeGroup = PwsafeSwift.Group
typealias PwsafeHeader = PwsafeSwift.Header
typealias PwsafeRecordId = UUID

struct DecryptedSafe: Equatable {
    let syncedFile: SyncedFile
    var pwsafe: Pwsafe
    var password: String

    func record(by id: PwsafeRecordId) -> PwsafeRecord? {
        return pwsafe[id]
    }

    // TODO: no unit-tests
    func records(by filter: SafeItemFilter) -> [PwsafeRecord] {
        return pwsafe
            .records(in: filter.selectedGroup)
            .filter(filterText(by: filter.searchText, keyPath: \.title))
            .sorted(by: sortText(by: \.title))
    }

    func groups(by filter: SafeItemFilter) -> [PwsafeGroup] {
        return pwsafe
            .subgroups(at: filter.selectedGroup)
            .filter(filterText(by: filter.searchText, keyPath: \.title))
            .sorted(by: sortText(by: \.title))
    }
}

private func filterText<T>(by text: String?, keyPath: KeyPath<T, String?>) -> (T) -> Bool {
    return { element in
        guard let searchText = text, !searchText.isEmpty else { return true }
        let title = element[keyPath: keyPath] ?? ""
        return title.localizedCaseInsensitiveContains(searchText)
    }
}

private func sortText<T>(by keyPath: KeyPath<T, String?>) -> (T, T) -> Bool {
    return { lhsElement, rhsElement in
        let lhs = lhsElement[keyPath: keyPath] ?? ""
        let rhs = rhsElement[keyPath: keyPath] ?? ""

        return lhs < rhs
    }
}
