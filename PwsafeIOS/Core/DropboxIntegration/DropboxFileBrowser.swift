//
//  DropboxFileBrowser.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 02/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import SwiftyDropbox
import ReactiveSwift

class DropboxFileBrowser: FileBrowsable {
    static let rootFolder: FileEntry = FileEntry(path: "", name: "Dropbox")

    func listEntries(_ fileEntry: FileEntry) -> SignalProducer<[FolderEntry], FileBrowserError> {
        return SignalProducer { observer, disposable in
            guard let client = DropboxClientsManager.authorizedClient else {
                observer.send(error: .unknownError)
                return
            }

            let response = client.files.listFolder(path: fileEntry.path).response { result, error in
                guard let result = result else {
                    observer.send(error: .unknownError)
                    return
                }

                observer.send(value: result.entries.compactMap(convertFileMetadataToFileEntry))
                observer.sendCompleted()
            }
            
            disposable.observeEnded {
                response.cancel()
            }
        }
    }
}

private func convertFileMetadataToFileEntry(_ file: Files.Metadata) -> FolderEntry? {
    guard let path = file.pathLower else { return nil }

    let fileEntry = FileEntry(path: path, name: file.name)

    switch file {
    case is Files.FolderMetadata:
        return .folder(fileEntry)

    case is Files.FileMetadata:
        return .file(fileEntry)

    default:
        return nil
    }
}
