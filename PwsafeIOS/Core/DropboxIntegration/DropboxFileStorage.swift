//
//  DropboxFileStorage.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 16/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import SwiftyDropbox

class DropboxFileStorage: FileStorage {
    let type: FileStorageType = .dropbox

    func fetchData(_ file: FileEntry) -> SignalProducer<FileData, FileStorageError> {
        return SignalProducer { observer, disposable in
            guard let client = DropboxClientsManager.authorizedClient else {
                observer.send(error: .unknownError)
                return
            }

            let destination: (URL, HTTPURLResponse) -> URL = { temporaryURL, response in
                let fileManager = FileManager.default
                // TODO: store it in 'tmp' directory
                // TODO: remove data
                let directoryURL = fileManager.urls(for: .cachesDirectory, in: .userDomainMask)[0]
                // generate a unique name for this file in case we've seen it before
                let uuid = UUID().uuidString
                let pathComponent = "\(uuid)-\(response.suggestedFilename!)"
                return directoryURL.appendingPathComponent(pathComponent)
            }

            let response = client.files.download(path: file.path, destination: destination).response { response, error in
                guard let (metadata, url) = response, let data = NSData(contentsOf: url) else {
                    observer.send(error: .unknownError)
                    return
                }
                observer.send(value: FileData(data: data as Data, metadata: metadata.rev))
                observer.sendCompleted()
            }

            disposable.observeEnded {
                response.cancel()
            }
        }
    }

    func fetchMetadata(_ file: FileEntry) -> SignalProducer<FileMetadata?, FileStorageError> {
        return SignalProducer { observer, _ in
            guard let client = DropboxClientsManager.authorizedClient else {
                observer.send(error: .unknownError)
                return
            }

            client.files.getMetadata(path: file.path).response { metadata, error in
                guard let metadata = metadata as? Files.FileMetadata else {
                    observer.send(error: analyze(error: error, forFile: file))
                    return
                }

                observer.send(value: metadata.rev)
                observer.sendCompleted()
            }
        }
    }

    func createFile(_ path: String, data: Data) -> SignalProducer<FileEntry, FileStorageError> {
        fatalError()
//        return SignalProducer { observer, disposable in
//            guard !self.fileManager.fileExistsAtPath(self.pathFromRoot(path)) else {
//                observer.sendFailed(Error.SafeAlreadyExists(name: path))
//                return
//            }
//            
//            let result = self.fileManager.createFileAtPath(self.pathFromRoot(path), contents: data, attributes: nil)
//            guard result else {
//                observer.sendFailed(Error.CouldNotCreateFile)
//                return
//            }
//            
//            observer.sendCompleted()
//        }
    }

    func updateFile(_ file: FileEntry, data: FileData) -> SignalProducer<FileUpdateResult, FileStorageError> {
        return SignalProducer { observer, _ in
            guard let client = DropboxClientsManager.authorizedClient else {
                observer.send(error: .unknownError)
                return
            }

            guard let revision = data.metadata else {
                observer.send(error: .unknownError)
                return
            }

            client.files.upload(path: file.path, mode: .update(revision), input: data.data).response { metadata, error in
                guard let metadata = metadata, error == nil else {
                    observer.send(error: .unknownError)
                    return
                }
                observer.send(value: .success(newMetadata: metadata.rev))
                observer.sendCompleted()
            }
        }
    }
}

private func analyze<T>(error: CallError<T>?, forFile file: FileEntry) -> FileStorageError {
    guard case .some(CallError.routeError(let box, _, _, _)) = error else { return .unknownError }

    switch box.unboxed {
    case let dropboxError as Files.GetMetadataError:
        return analyzeGetMetadataError(dropboxError, file: file)

    case let dropboxError as Files.UploadError:
        return analyzeUpdateError(dropboxError, file: file)

    default:
        return .unknownError
    }
}

private func analyzeGetMetadataError(_ error: Files.GetMetadataError, file: FileEntry) -> FileStorageError {
    guard case Files.GetMetadataError.path(.notFound) = error else {
            return .unknownError
    }

    return .fileNotFound(file)
}

private func analyzeUpdateError(_ error: Files.UploadError, file: FileEntry) -> FileStorageError {
    guard case Files.UploadError.path(let writeError) = error, case .conflict = writeError.reason else {
        return .unknownError
    }

    // this is conflict error
    return .unknownError
}
