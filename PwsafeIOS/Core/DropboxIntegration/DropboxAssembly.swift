//
//  DropboxAssembly.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject

class DropboxAssembly: Assembly {
    func assemble(container: Container) {
        container.register(DropboxIntegration.self) { resolver in
            return DropboxIntegration(storageRegistrar: resolver.resolve(StorageRegistrarProtocol.self)!,
                dropboxFileStorage: resolver.resolve(DropboxFileStorage.self)!)
        }
        .inObjectScope(.container)

        container.register(DropboxIntegrationProtocol.self) { resolver in
            return resolver.resolve(DropboxIntegration.self)!
        }

        container.register(DropboxLinkerProtocol.self) { resolver in
            return resolver.resolve(DropboxIntegration.self)!
        }

        container.register(DropboxFileStorage.self) { _ in
            return DropboxFileStorage()
        }
        .inObjectScope(.container)

        container.register(DropboxSyncService.self) { resolver in
            return DropboxSyncService(
                fileStateProvider: resolver.resolve(RemoteFileStateProvider.self)!,
                fileStorage: resolver.resolve(DropboxFileStorage.self)!)
        }
        .inObjectScope(.container)
    }
}
