//
//  DropboxSyncService.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

class DropboxSyncService: RemoteFileChangeSource {
    private let fileStateProvider: RemoteFileStateProvider
    private let fileStorage: FileStorage

    private let changesSignal: Signal<RemoteFileChange, AppError>
    private let changesObserver: Signal<RemoteFileChange, AppError>.Observer

    init(fileStateProvider: RemoteFileStateProvider, fileStorage: FileStorage) {
        self.fileStateProvider = fileStateProvider
        self.fileStorage = fileStorage

        (changesSignal, changesObserver) = Signal.pipe()
    }

    func remoteChanges() -> SignalProducer<RemoteFileChange, AppError> {
        return SignalProducer(changesSignal)
    }

    func sync() -> SignalProducer<Void, AppError> {
        return fileStateProvider.listStates().flattenArray(.latest)
            .filter({ $0.storageType == .dropbox })
            .flatMap(.merge, self.checkRemoteFileState)
            .on(value: { self.changesObserver.send(value: $0) })
            .logEvents(logger: log("DropboxSyncService.sync"))
            .then(SignalProducer<Void, AppError>.empty)
    }

    private func checkRemoteFileState(_ state: RemoteFileState) -> SignalProducer<RemoteFileChange, AppError> {
        return fileStorage.fetchMetadata(state.file)
            .flatMap(.merge) { [unowned self] (metadata) -> SignalProducer<RemoteFileChange, FileStorageError> in
                guard state.metadata != metadata else { return .empty }

                return self.fileStorage.fetchData(state.file)
                    .map({ RemoteFileChange(file: state.file, storageType: state.storageType, change: .updated(data: $0)) })
            }
            .flatMapError { (error: FileStorageError) -> SignalProducer<RemoteFileChange, FileStorageError> in
                guard case .fileNotFound = error else { return SignalProducer(error: error) }
                let change = RemoteFileChange(file: state.file, storageType: state.storageType, change: .unlinked)
                return SignalProducer(value: change)
            }
            .mapError(AppError.internalError)
    }
}
