//
//  DropboxIntegration.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import SwiftyDropbox

protocol DropboxIntegrationProtocol {
    func setup()

    func handleUrl(_ url: URL)
}

protocol DropboxLinkerProtocol {
    func link(fromController controller: UIViewController)

    func unlink()
}

class DropboxIntegration: DropboxIntegrationProtocol {
    private static let apiKey: String = "lfeke1c6kfs6ulh"

    fileprivate let storageRegistrar: StorageRegistrarProtocol
    fileprivate let dropboxFileStorage: DropboxFileStorage

    init(storageRegistrar: StorageRegistrarProtocol, dropboxFileStorage: DropboxFileStorage) {
        self.storageRegistrar = storageRegistrar
        self.dropboxFileStorage = dropboxFileStorage
    }

    func setup() {
        DropboxClientsManager.setupWithAppKey(DropboxIntegration.apiKey)
        tryToRegisterStorage()
    }

    func handleUrl(_ url: URL) {
        guard let authResult = DropboxClientsManager.handleRedirectURL(url) else { return }

        switch authResult {
        case .success(let token):
            print("Success! User is logged into Dropbox with token: \(token)")
            tryToRegisterStorage()

        case .error(let error, let description):
            print("Error \(error): \(description)")

        case .cancel:
            print("User cancelled")
        }
    }

    fileprivate func tryToRegisterStorage() {
        if DropboxClientsManager.authorizedClient != nil {
            storageRegistrar.register(storage: dropboxFileStorage)
        }
    }
}

extension DropboxIntegration: DropboxLinkerProtocol {
    func link(fromController controller: UIViewController) {
        guard DropboxClientsManager.authorizedClient == nil else { return }

        DropboxClientsManager.authorizeFromController(
            UIApplication.shared,
            controller: controller,
            openURL: { url in UIApplication.shared.openURL(url) })
    }

    func unlink() {
        DropboxClientsManager.unlinkClients()
        storageRegistrar.unregister(storage: dropboxFileStorage)
    }
}
