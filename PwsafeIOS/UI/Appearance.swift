//
//  Colors.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

class Appearance {
    static func setup() {
        with(UINavigationBar.appearance()) {
            $0.barTintColor = Colors.navigationBarBackground
            $0.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
            $0.tintColor = .white
        }

        with(UIToolbar.appearance()) {
            $0.backgroundColor = Colors.toolBarBackground
        }
    }
}

enum Colors {
    static let background = UIColor(rgb: 0xc1e0f0)
    static let textFieldBackground = UIColor(rgb: 0xb6d0dd)
    static let navigationBarBackground = UIColor(rgb: 0x188dff)
    static let toolBarBackground = navigationBarBackground
}
