//
//  OpenSafeController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 19/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

final class OpenSafeController: UIViewController, UITextFieldDelegate {
    // TODO: add progress
    struct Props {
        let password: String?
        let isValid: Bool

        let onPasswordUpdated: CommandWith<String?>
        let onOpenSafe: Command
        let onCloseTapped: Command
        let onDispose: Command

        init(password: String? = nil,
             isValid: Bool = false,
             onPasswordUpdated: CommandWith<String?> = .nop,
             onOpenSafe: Command = .nop,
             onCloseTapped: Command = .nop,
             onDispose: Command = .nop) {
            self.password = password
            self.isValid = isValid
            self.onPasswordUpdated = onPasswordUpdated
            self.onOpenSafe = onOpenSafe
            self.onCloseTapped = onCloseTapped
            self.onDispose = onDispose
        }
    }

    var props: Props = .init() {
        didSet {
            view.setNeedsLayout()
        }
    }

    private let passwordTextField: UITextField = with(UITextField()) {
        $0.placeholder = "Enter the safe password here"
        $0.backgroundColor = Colors.textFieldBackground
        $0.autocapitalizationType = .none
        $0.autocorrectionType = .no
        $0.isSecureTextEntry = true
        $0.returnKeyType = .done
    }

    private let openButton: UIButton = with(UIButton(type: .system)) {
        $0.setTitle("Open Safe", for: .normal)
    }

    private lazy var progressHUD: ProgressHUD = ProgressHUD(view: self.view)

    deinit {
        props.onDispose.perform()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Colors.background

        layout()
        bindActions()

        DispatchQueue.main.async {
            self.passwordTextField.becomeFirstResponder()
        }
    }

    private func layout() {
        passwordTextField.autoSetDimension(.height, toSize: 40)

        let stack = UIStackView(arrangedSubviews: [passwordTextField, openButton])
        view.addSubview(stack)

        stack.axis = .vertical
        stack.spacing = 10
        stack.autoPinEdge(toSuperviewEdge: .left).constant = 20
        stack.autoPinEdge(toSuperviewEdge: .right).constant = -20
        stack.autoCenterInSuperview()
    }

    private func bindActions() {
        let closeButton = UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(handleCloseTapped))
        navigationItem.leftBarButtonItem = closeButton

        passwordTextField.addTarget(self, action: #selector(handleValueChange), for: .editingChanged)
        passwordTextField.delegate = self
        openButton.addTarget(self, action: #selector(handleOpenTapped), for: .touchUpInside)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        passwordTextField.text = props.password
        openButton.isEnabled = props.isValid
    }

    // MARK: UITextFieldDelegate
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        props.onOpenSafe.perform()
        return true
    }

    // MARK: Actions
    @objc
    private func handleCloseTapped() {
        props.onCloseTapped.perform()
    }

    @objc
    private func handleValueChange() {
        props.onPasswordUpdated.perform(with: passwordTextField.text)
    }

    @objc
    private func handleOpenTapped() {
        props.onOpenSafe.perform()
    }
}
