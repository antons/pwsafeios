//
//  OpenSafeState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct OpenSafeState: Equatable {
    let file: SyncedFile
    var enteredPassword: String?
    var isValid: Bool
}
