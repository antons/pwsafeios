//
//  OpenSafeState+Reduce.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func reduce(state: inout OpenSafeState, action: Action2) {
    switch action {
    case let action as OpenSafeUpdatePassword:
        state.enteredPassword = action.passwordText
        state.isValid = !action.passwordText.isEmptyOrNil

    default: break
    }
}
