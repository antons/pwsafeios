//
//  OpenSafeCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit
import ReactiveSwift


protocol OpenSafeCommands: class {
    func passwordUpdated() -> CommandWith<String?>

    func unlockSafe(file: SyncedFile, password: String) -> Command

    func close() -> Command

    func dispose() -> Command
}

final class OpenSafeCommandsProvider: OpenSafeCommands {
    private let dispatcher: Dispatcher
    private let cryptoService: CryptoSafeServiceProtocol
    private let resultObserver: Signal<OpenSafeResult, Never>.Observer

    init(dispatcher: Dispatcher,
         cryptoService: CryptoSafeServiceProtocol,
         resultObserver: Signal<OpenSafeResult, Never>.Observer) {
        self.dispatcher = dispatcher
        self.cryptoService = cryptoService
        self.resultObserver = resultObserver
    }

    func passwordUpdated() -> CommandWith<String?> {
        return CommandWith { [dispatcher] password in
            dispatcher.dispatch(OpenSafeUpdatePassword(passwordText: password))
        }
    }

    func unlockSafe(file: SyncedFile, password: String) -> Command {
        return Command { [resultObserver, cryptoService] in
            cryptoService.loadSafe(from: file, password: password)
                .on(value: { safe in
                    resultObserver.send(value: .openSafe(safe))
                })
                .start()
        }
    }

    func close() -> Command {
        return Command { [resultObserver] in
            resultObserver.send(value: .close)
        }
    }

    func dispose() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(OpenSafeDispose())
        }
    }
}
