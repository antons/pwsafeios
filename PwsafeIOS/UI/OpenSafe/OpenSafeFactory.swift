//
//  OpenSafeFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 27/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import ReactiveSwift

class OpenSafeFactory {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func openSafe(_ syncedFile: SyncedFile) -> (OpenSafeOutput, UIViewController) {
        let store = StateStore.store

        let (signal, observer) = Signal<OpenSafeResult, Never>.pipe()

        let output = OpenSafeOutput(resultSignal: signal)

        let commands = OpenSafeCommandsProvider(
            dispatcher: store,
            cryptoService: parentContainer.resolve(CryptoSafeService.self)!,
            resultObserver: observer)

        let controller = OpenSafeController()

        controller.connect(to: store, commands: commands)

        return (output, controller)
    }
}
