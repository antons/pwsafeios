//
//  OpenSafeUpdatePassword.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct OpenSafeUpdatePassword: Action2 {
    let passwordText: String?
}
