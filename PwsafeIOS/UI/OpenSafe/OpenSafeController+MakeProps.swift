//
//  OpenSafeController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from state: OpenSafeState?, commands: OpenSafeCommands) -> OpenSafeController.Props {
    guard let state = state else { return .init() }

    return OpenSafeController.Props(
        password: state.enteredPassword,
        isValid: state.isValid,
        onPasswordUpdated: commands.passwordUpdated(),
        onOpenSafe: commands.unlockSafe(file: state.file, password: state.enteredPassword ?? ""),
        onCloseTapped: commands.close(),
        onDispose: commands.dispose())
}
