//
//  OpenSafeOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 11/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

//protocol OpenSafeOutput: class {
//    var resultSignal: Signal<OpenSafeResult, Never> { get }
//}

enum OpenSafeResult {
    case openSafe(DecryptedSafe)
    case close
}

struct OpenSafeOutput {
    let resultSignal: Signal<OpenSafeResult, Never>
}
