//
//  OpenSafeController+Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 21/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension OpenSafeController {
    func connect(to store: Store<AppState>, commands: OpenSafeCommands) {
        let observer = CommandWith<AppState> { [weak self] state in
            self?.props = makeProps(from: state.openSafe, commands: commands)
        }

        // TODO: store cancellation token
        store.observe(with: observer)
    }
}
