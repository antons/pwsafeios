//
//  ManageRecordController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from state: ManageSafeState?, screenId: UUID, commands: ManageSafeCommands) -> ManageSafeController.Props? {
    typealias Section = ManageSafeController.Props.Section

    func makeGroupsSection(_ groups: [PwsafeGroup]) -> Section? {
        guard !groups.isEmpty else { return nil }

        let groupItems = groups.map {
            return SafeRecordItem(title: $0.title, onSelected: commands.select(group: $0))
        }
        return Section(type: .groups, items: groupItems)
    }

    func makeRecordsSection(_ records: [PwsafeRecord]) -> Section? {
        guard !records.isEmpty else { return nil }

        let recordItems = records.map {
            return SafeRecordItem(title: $0.title, onSelected: commands.select(record: $0))
        }
        return Section(type: .records, items: recordItems)
    }

    guard let state = state,
        let filter = state.screen(by: screenId)?.filter else { return nil }

    let groups = state.decryptedSafe.groups(by: filter)
    let records = state.decryptedSafe.records(by: filter)

    let sections = [makeGroupsSection(groups), makeRecordsSection(records)].compactMap(identity)

    return ManageSafeController.Props(
        sections: sections,
        onSearchTextUpdated: commands.searchTextUpdate(screenId: screenId),
        onSearchCancelled: commands.searchCancelled(screenId: screenId),
        onInfoTapped: commands.safeInfo(),
        onAddTapped: commands.addRecord(),
        onDisposed: commands.dispose(screenId: screenId))
}
