//
//  RecordFilterUpdated.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 27/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct RecordFilterUpdated: Action2 {
    let screenId: UUID
    let text: String?
}
