//
//  ManageSafe.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 07/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct ManageSafe: Action2 {
    let decryptedSafe: DecryptedSafe
}
