//
//  OpenGroup.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 09/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct OpenGroup: Action2 {
    let screenId: UUID
    let filter: SafeItemFilter
}
