//
//  ManagerSafeTableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class ManageSafeTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var items: [ManageSafeController.Props.Section] = [] {
        didSet {
            updateItems(old: oldValue, new: items)
        }
    }

    private let tableView: UITableView

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.setup()
    }

    private func setup() {
        tableView.register(cellClass: SafeRecordCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }

    private func item(for indexPath: IndexPath) -> SafeRecordItem {
        return items[indexPath.section].items[indexPath.item]
    }

    private func updateItems(old: [ManageSafeController.Props.Section], new: [ManageSafeController.Props.Section]) {
        tableView.reloadData()
    }

    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellItem = item(for: indexPath)
        let cell = tableView.dequeueCell(for: indexPath) as SafeRecordCell
        cell.configure(cellItem)
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch items[section].type {
        case .groups:
            return "Groups"
            
        case .records:
            return "Records"
        }
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SelectableCell {
            cell.didSelect()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
