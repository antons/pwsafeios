//
//  File.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 08/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

protocol ManageSafeRouterProtocol: class {
    func manageRecord(id: UUID)

    func open(group: PwsafeGroup, screenId: UUID)

    func openSafeSettings()
}

final class ManageSafeRouter: ManageSafeRouterProtocol {
    weak var controller: UIViewController?

    private let context: ManageSafeContextProtocol

    init(context: ManageSafeContextProtocol) {
        self.context = context
    }

    func manageRecord(id: UUID) {
        let vc = context.manageRecord(id: id)
        controller?.push(vc)
    }

    func open(group: PwsafeGroup, screenId: UUID) {
        let vc = context.safeRecordList(screenId: screenId, filter: .contained(in: group))
        controller?.push(vc)
    }

    func openSafeSettings() {
        let vc = context.safeSettings()
        controller?.push(vc)
    }
}
