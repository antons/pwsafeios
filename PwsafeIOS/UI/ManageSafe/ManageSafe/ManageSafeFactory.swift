//
//  ManageSafeFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/27/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import UIKit
import ReactiveSwift

protocol ManageSafeFactoryProtocol: class {
    func manageSafe(screenId: UUID, filter: SafeItemFilter, service: CryptoSafeServiceProtocol) -> UIViewController
}

class ManageSafeFactory: ManageSafeFactoryProtocol {
    func manageSafe(screenId: UUID, filter: SafeItemFilter, service: CryptoSafeServiceProtocol) -> UIViewController {

        let store = StateStore.store

        let context = ManageSafeContext(cryptoSafeService: service)
        let router = ManageSafeRouter(context: context)
        let controller = ManageSafeController()
        router.controller = controller
        let commands = ManageSafeCommandsProvider(dispatcher: store, router: router)

        controller.connect(to: store, screenId: screenId, commands: commands)

        return controller
    }
}
