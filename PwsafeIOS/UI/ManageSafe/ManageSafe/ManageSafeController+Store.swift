//
//  ManageSafeController+Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension ManageSafeController {
    func connect(to store: Store<AppState>, screenId: UUID, commands: ManageSafeCommands) {
        let observer = CommandWith<AppState> { [weak self] state in
            self?.props = makeProps(from: state.manageSafe, screenId: screenId, commands: commands)
        }

        // TODO: store cancellation token
        store.observe(with: observer)
    }
}
