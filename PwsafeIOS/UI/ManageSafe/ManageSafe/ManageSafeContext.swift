//
//  ManageSafeContext.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 23/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveCocoa
import Swinject

protocol ManageSafeContextProtocol: class {
    func safeRecordList(screenId: UUID, filter: SafeItemFilter) -> UIViewController

    func safeSettings() -> UIViewController
    
    func manageRecord(id: UUID) -> UIViewController
}

class ManageSafeContext: ManageSafeContextProtocol {
    private let cryptoSafeService: CryptoSafeServiceProtocol

    init(cryptoSafeService: CryptoSafeServiceProtocol) {
        self.cryptoSafeService = cryptoSafeService
    }

    func safeRecordList(screenId: UUID, filter: SafeItemFilter) -> UIViewController {
        return ManageSafeFactory().manageSafe(screenId: screenId, filter: filter, service: cryptoSafeService)
    }

    func safeSettings() -> UIViewController {
        return SafeSettingsFactory(cryptoSafeService: cryptoSafeService).create()
    }
    
    func manageRecord(id: UUID) -> UIViewController {
        return ManageRecordFactory().manageRecord(id: id, service: cryptoSafeService)
    }
}
