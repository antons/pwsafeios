//
//  ManageSafeController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class ManageSafeController: UITableViewController, UISearchBarDelegate, UISearchResultsUpdating {
    struct Props {
        enum SectionType: Equatable {
            case groups
            case records
        }

        struct Section: Equatable {
            let type: SectionType
            let items: [SafeRecordItem]
        }

        let sections: [Section]

        let onSearchTextUpdated: CommandWith<String?>
        let onSearchCancelled: Command
        let onInfoTapped: Command
        let onAddTapped: Command
        let onDisposed: Command
    }

    var props: Props? {
        didSet {
            update(old: oldValue, new: props)
        }
    }

    private lazy var tableManager: ManageSafeTableManager = .init(tableView: self.tableView)
    private let infoButton: UIBarButtonItem = .init(title: "Info", style: .plain, target: nil, action: nil)
    private let addButton: UIBarButtonItem = .init(barButtonSystemItem: .add, target: nil, action: nil)
    private let searchController: UISearchController = .init(searchResultsController: nil)

    convenience init() {
        self.init(style: .grouped)
    }

    deinit {
        props?.onDisposed.perform()
    }

    private func update(old: Props?, new: Props?) {
        tableManager.items = new?.sections ?? []
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        navigationItem.rightBarButtonItem = infoButton

        bindActions()

        toolbarItems = [addButton]

        configureSearchBar()
    }

    private func bindActions() {
        infoButton.target = self
        infoButton.action = #selector(infoTapped)

        addButton.target = self
        addButton.action = #selector(addTapped)
    }

    private func configureSearchBar() {
        tableView.tableHeaderView = searchController.searchBar
        searchController.searchBar.delegate = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
    }

    // MARK: UISearchBarDelegate
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        props?.onSearchTextUpdated.perform(with: searchText)
    }

    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        props?.onSearchCancelled.perform()
    }

    // MARK: UISearchResultsUpdating
    func updateSearchResults(for searchController: UISearchController) {
        // TODO: use this method for updating search text?
    }

    // MARK: Actions
    @objc
    private func infoTapped() {
        props?.onInfoTapped.perform()
    }

    @objc
    private func addTapped() {
        props?.onAddTapped.perform()
    }
}
