//
//  ManageSafeCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

protocol ManageSafeCommands {
    func select(record: PwsafeRecord) -> Command

    func select(group: PwsafeGroup) -> Command

    func safeInfo() -> Command

    func addRecord() -> Command

    func searchTextUpdate(screenId: UUID) -> CommandWith<String?>

    func searchCancelled(screenId: UUID) -> Command

    func dispose(screenId: UUID) -> Command
}

final class ManageSafeCommandsProvider: ManageSafeCommands {
    private let dispatcher: Dispatcher
    private let router: ManageSafeRouterProtocol

    init(dispatcher: Dispatcher, router: ManageSafeRouterProtocol) {
        self.dispatcher = dispatcher
        self.router = router
    }

    func select(record: PwsafeRecord) -> Command {
        return Command { [dispatcher, router] in
            dispatcher.dispatch(OpenRecord(record: record))
            router.manageRecord(id: record.uuid)
        }
    }

    func select(group: PwsafeGroup) -> Command {
        return Command { [dispatcher, router] in
            let screenId = UUID()
            dispatcher.dispatch(OpenGroup(screenId: screenId, filter: .contained(in: group)))
            router.open(group: group, screenId: screenId)
        }
    }

    func safeInfo() -> Command {
        return Command { [dispatcher, router] in
            dispatcher.dispatch(OpenSafeSettings())
            router.openSafeSettings()
        }
    }

    func addRecord() -> Command {
        return Command { [dispatcher, router] in
            let recordId = UUID()
            dispatcher.dispatch(AddRecord(recordId: recordId))
            router.manageRecord(id: recordId)
        }
    }

    func searchTextUpdate(screenId: UUID) -> CommandWith<String?> {
        return CommandWith { [dispatcher] searchText in
            dispatcher.dispatch(RecordFilterUpdated(screenId: screenId, text: searchText))
        }
    }

    func searchCancelled(screenId: UUID) -> Command {
        return CommandWith { [dispatcher] in
            dispatcher.dispatch(RecordFilterUpdated(screenId: screenId, text: nil))
        }
    }

    func dispose(screenId: UUID) -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(SafeDidClose(screenId: screenId))
        }
    }
}
