//
//  ManageSafeState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct ManageSafeState {
    var decryptedSafe: DecryptedSafe
    var manageRecord: ManageRecordState?
    var safeSettings: SafeSettingsState?
    var changePassword: ChangePasswordState?
    var screens: [UUID: ManageSafeScreen]

    init(decryptedSafe: DecryptedSafe,
         manageRecord: ManageRecordState? = nil,
         safeSettings: SafeSettingsState? = nil,
         changePassword: ChangePasswordState? = nil,
         screens: [UUID: ManageSafeScreen] = [:]) {
        self.decryptedSafe = decryptedSafe
        self.manageRecord = manageRecord
        self.safeSettings = safeSettings
        self.changePassword = changePassword
        self.screens = screens
    }

    mutating func updateSafe(_ safe: DecryptedSafe) {
        decryptedSafe = safe
        if let manageRecord = manageRecord, let record = safe.pwsafe[manageRecord.record.uuid] {
            self.manageRecord?.update(record)
        }

        if safeSettings != nil {
            safeSettings = SafeSettingsState(header: safe.pwsafe.header)
        }
    }

    mutating func openScreen(id screenId: UUID, filter: SafeItemFilter) {
        screens[screenId] = ManageSafeScreen(id: screenId, filter: filter)
    }

    func screen(by id: UUID) -> ManageSafeScreen? {
        return screens[id]
    }

    mutating func removeScreen(by id: UUID) {
        screens.removeValue(forKey: id)
    }
}

struct ManageSafeScreen: Equatable {
    // TODO: remove id?
//    let id: UUID
    var filter: SafeItemFilter

    init(id: UUID, filter: SafeItemFilter) {
        self.filter = filter
    }
}
