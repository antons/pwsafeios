//
//  ManageSafeState+Reduce.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

//swiftlint:disable:next cyclomatic_complexity
func reduce(state: inout ManageSafeState, action: Action2) {
    switch action {
    case let action as OpenRecord:
        guard let record = state.decryptedSafe.record(by: action.record.uuid) else { break }
        state.manageRecord = ManageRecordState.view(record: record)

    case let action as AddRecord:
        state.manageRecord = ManageRecordState.edit(record: PwsafeRecord(uuid: action.recordId))

    case let action as OpenGroup:
        state.openScreen(id: action.screenId, filter: action.filter)

    case let action as SafeDidClose:
        state.removeScreen(by: action.screenId)

    case is ManageRecordDisposed:
        state.manageRecord = nil

    case let action as UpdateSafe:
        state.updateSafe(action.safe)

    case is OpenSafeSettings:
        let safe = state.decryptedSafe.pwsafe
        state.safeSettings = SafeSettingsState(header: safe.header)

    case is CloseSafeSettings:
        state.safeSettings = nil

    case is OpenChangePassword:
        state.changePassword = ChangePasswordState(minPasswordLength: 4, updatedPassword: "")

    case is CloseChangePassword:
        state.changePassword = nil

    case let action as RecordFilterUpdated:
        state.screens[action.screenId]?.filter.searchText = action.text

    default:
        break
    }

    if var manageRecord = state.manageRecord {
        reduce(state: &manageRecord, action: action)
        state.manageRecord = manageRecord
    }

    if var changePassword = state.changePassword {
        reduce(state: &changePassword, action: action)
        state.changePassword = changePassword
    }
}
