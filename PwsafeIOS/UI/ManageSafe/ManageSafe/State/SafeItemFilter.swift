//
//  RecordListFilter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 24/02/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift

struct SafeItemFilter: Equatable {
    struct FilterType: OptionSet {
        let rawValue: Int
        
        static let records = FilterType(rawValue: 1 << 0)
        static let groups = FilterType(rawValue: 1 << 1)
        
        static let all: FilterType = [.records, .groups]
    }

    static let all: SafeItemFilter = .init(type: .all, selectedGroup: Pwsafe.rootGroup, searchText: nil)
    
    let type: FilterType
    let selectedGroup: Group
    var searchText: String?

    init(type: FilterType, selectedGroup: PwsafeGroup, searchText: String? = nil) {
        self.type = type
        self.selectedGroup = selectedGroup
        self.searchText = searchText
    }
}

extension SafeItemFilter {
    static func contained(in group: Group) -> SafeItemFilter {
        return SafeItemFilter(type: .all, selectedGroup: group, searchText: nil)
    }
}
