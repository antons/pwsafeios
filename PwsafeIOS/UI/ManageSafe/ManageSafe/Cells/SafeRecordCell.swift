//
//  SafeRecordCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

struct SafeRecordItem: Equatable {
    let title: String?
    let onSelected: Command
}

final class SafeRecordCell: UITableViewCell, ConfigurableCell {
    private var props: SafeRecordItem?

    func configure(_ item: SafeRecordItem) {
        self.props = item
        self.textLabel?.text = item.title
    }

    func didSelect() {
        props?.onSelected.perform()
    }
}
