//
//  SafeSettingsController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class SafeSettingsController: UIViewController {
    struct Props {
        let menu: [MenuSection]
        let onDisposed: Command
    }

    var props: Props? {
        didSet {
            update(old: oldValue, new: props)
        }
    }

    private let tableView: UITableView = .init(frame: .zero, style: .grouped)
    private lazy var tableManager: MenuTableManager = .init(tableView: self.tableView)

    deinit {
        props?.onDisposed.perform()
    }

    private func update(old: Props?, new: Props?) {
        tableManager.sections = new?.menu ?? []
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()
    }
}
