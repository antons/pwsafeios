//
//  SafeSettingsCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

protocol SafeSettingsCommands: class {
    func changePassword() -> Command

    func dispose() -> Command
}

class SafeSettingsCommandsProvider: SafeSettingsCommands {
    private let dispatcher: Dispatcher
    private let router: SafeSettingsRouterProtocol

    init(dispatcher: Dispatcher, router: SafeSettingsRouterProtocol) {
        self.dispatcher = dispatcher
        self.router = router
    }

    func changePassword() -> Command {
        return Command { [dispatcher, router] in
            let output = ChangePasswordOutput {
                // TODO: nop
            }

            dispatcher.dispatch(OpenChangePassword())
            router.changePassword(output: output)
        }
    }

    func dispose() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(CloseSafeSettings())
        }
    }
}
