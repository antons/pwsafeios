//
//  File.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/12/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import PwsafeSwift


class SafeSettingsFactory {
    private let cryptoSafeService: CryptoSafeServiceProtocol

    init(cryptoSafeService: CryptoSafeServiceProtocol) {
        self.cryptoSafeService = cryptoSafeService
    }

    func create() -> UIViewController {
        let store = StateStore.store

        let context = SafeSettingsContext(cryptoSafeService: cryptoSafeService)
        let router = SafeSettingsRouter(context: context)
        let commands = SafeSettingsCommandsProvider(dispatcher: store, router: router)

        let controller = SafeSettingsController()
        controller.connect(to: store, commands: commands)

        router.controller = controller

        return controller
    }
}
