//
//  SafeSettingsContext.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 08/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

protocol SafeSettingsContextProtocol: class {
    func changePassword(output: ChangePasswordOutput) -> UIViewController
}

class SafeSettingsContext: SafeSettingsContextProtocol {
    private let cryptoSafeService: CryptoSafeServiceProtocol

    init(cryptoSafeService: CryptoSafeServiceProtocol) {
        self.cryptoSafeService = cryptoSafeService
    }

    func changePassword(output: ChangePasswordOutput) -> UIViewController {
        return ChangePasswordFactory(cryptoSafeService: cryptoSafeService).create(output: output)
    }
}
