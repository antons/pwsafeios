//
//  SafeSettingsRouter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

protocol SafeSettingsRouterProtocol: class {
    func changePassword(output: ChangePasswordOutput)
}

final class SafeSettingsRouter: SafeSettingsRouterProtocol {
    weak var controller: UIViewController?

    private let context: SafeSettingsContext

    init(context: SafeSettingsContext) {
        self.context = context
    }

    func changePassword(output: ChangePasswordOutput) {
        let vc = context.changePassword(output: output)
        controller?.navigationController?.pushViewController(vc, animated: true)
    }
}
