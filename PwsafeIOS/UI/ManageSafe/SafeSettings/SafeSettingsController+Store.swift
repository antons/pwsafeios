//
//  SafeSettingsController+Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension SafeSettingsController {
    func connect(to store: Store<AppState>, commands: SafeSettingsCommands) {
        let observer = CommandWith<AppState> { [weak self] state in
            self?.props = makeProps(from: state.manageSafe?.safeSettings, commands: commands)
        }

        // TODO: store cancellation token
        store.observe(with: observer)
    }
}
