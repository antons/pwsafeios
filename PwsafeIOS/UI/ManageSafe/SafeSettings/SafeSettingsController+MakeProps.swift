//
//  SafeSettingsController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from state: SafeSettingsState?, commands: SafeSettingsCommands) -> SafeSettingsController.Props? {
    guard let state = state else { return nil }

    let lastSave = state.timestampOfLastSave.map({ $0.description }) ?? ""

    let menu = [
        MenuSection(items: [
            MenuItem(title: "Change Password", onSelected: commands.changePassword())
        ]),
        MenuSection(items: [
            MenuItem(title: "Last Save: \(lastSave)", onSelected: .nop),
            MenuItem(title: "Name: \(state.databaseName ?? "")", onSelected: .nop)
        ])
    ]

    return SafeSettingsController.Props(menu: menu, onDisposed: commands.dispose())
}
