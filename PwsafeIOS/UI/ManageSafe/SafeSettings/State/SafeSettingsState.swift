//
//  SafeSettingsState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct SafeSettingsState: Equatable {
    var timestampOfLastSave: Date?
    var whatPerformedLastSave: String?
    var databaseName: String?
    var databaseDescription: String?
}

extension SafeSettingsState {
    init(header: PwsafeHeader) {
        self.timestampOfLastSave = header.timestampOfLastSave
        self.whatPerformedLastSave = header.whatPerformedLastSave
        self.databaseName = header.databaseName
        self.databaseDescription = header.databaseDescription
    }
}
