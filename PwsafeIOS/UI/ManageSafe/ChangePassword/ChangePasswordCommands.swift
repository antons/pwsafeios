//
//  ChangePasswordCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol ChangePasswordCommands {
    func passwordFieldUpdate() -> CommandWith<String?>

    func updateSafe(_ makeSafe: @escaping () -> DecryptedSafe) -> Command

    func dispose() -> Command
}

final class ChangePasswordCommandsProvider: ChangePasswordCommands {
    private let dispatcher: Dispatcher
    private let service: CryptoSafeServiceProtocol
    private let router: ChangePasswordRouterProtocol

    init(dispatcher: Dispatcher, service: CryptoSafeServiceProtocol, router: ChangePasswordRouterProtocol) {
        self.dispatcher = dispatcher
        self.service = service
        self.router = router
    }

    func passwordFieldUpdate() -> CommandWith<String?> {
        return CommandWith { [dispatcher] password in
            dispatcher.dispatch(ChangePasswordUpdateField(passwordText: password))
        }
    }

    func updateSafe(_ makeSafe: @escaping () -> DecryptedSafe) -> Command {
        return Command { [service, dispatcher, router] in
            let safe = makeSafe()
            service.storeSafe(safe)
                .observe(on: UIScheduler())
                .on(completed: {
                    dispatcher.dispatch(UpdateSafe(safe: safe))
                    router.dismiss()
                })
                .start()
        }
    }

    func dispose() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(CloseChangePassword())
        }
    }
}
