//
//  ChangePasswordController+Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension ChangePasswordController {
    func connect(to store: Store<AppState>, commands: ChangePasswordCommands) {
        let observer = CommandWith<AppState> { [weak self] state in
            self?.props = makeProps(from: state.manageSafe, commands: commands)
        }

        // TODO: store cancellation token
        store.observe(with: observer)
    }
}
