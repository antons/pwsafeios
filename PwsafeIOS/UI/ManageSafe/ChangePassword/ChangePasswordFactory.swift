//
//  ChangePasswordFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 07/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

class ChangePasswordFactory {
    private let cryptoSafeService: CryptoSafeServiceProtocol

    init(cryptoSafeService: CryptoSafeServiceProtocol) {
        self.cryptoSafeService = cryptoSafeService
    }

    func create(output: ChangePasswordOutput) -> UIViewController {
        let store = StateStore.store

        let controller = ChangePasswordController()
        let router = ChangePasswordRouter()
        router.controller = controller
        let commands = ChangePasswordCommandsProvider(dispatcher: store, service: cryptoSafeService, router: router)
        controller.connect(to: store, commands: commands)

        return controller
    }
}
