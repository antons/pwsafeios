//
//  ChangePasswordState+Reduce.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func reduce(state: inout ChangePasswordState, action: Action2) {
    switch action {
    case let action as ChangePasswordUpdateField:
        state.updatedPassword = action.passwordText ?? ""

    default: break
    }
}
