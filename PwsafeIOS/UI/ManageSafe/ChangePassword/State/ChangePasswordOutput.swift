//
//  ChangePasswordOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 11/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

struct ChangePasswordOutput {
    let passwordUpdated: () -> Void
}
