//
//  ChangePasswordFieldUpdated.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct ChangePasswordUpdateField: Action2 {
    let passwordText: String?
}
