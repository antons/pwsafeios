//
//  ChangePasswordController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from safeState: ManageSafeState?, commands: ChangePasswordCommands) -> ChangePasswordController.Props? {
    guard let state = safeState?.changePassword, let safe = safeState?.decryptedSafe else { return nil }

    let changeTapped = commands.updateSafe {
        var updated = safe
        updated.password = state.updatedPassword
        return updated
    }

    return ChangePasswordController.Props(
        passwordText: state.updatedPassword,
        isPasswordAcceptable: state.isPasswordValid,
        onPasswordUpdated: commands.passwordFieldUpdate(),
        onPasswordChangeTapped: changeTapped,
        onDisposed: commands.dispose())
}
