//
//  ChangePasswordRouter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

protocol ChangePasswordRouterProtocol: class {
    func dismiss()
}

final class ChangePasswordRouter: ChangePasswordRouterProtocol {
    weak var controller: UIViewController?

    func dismiss() {
        controller?.navigationController?.popViewController(animated: true)
    }
}
