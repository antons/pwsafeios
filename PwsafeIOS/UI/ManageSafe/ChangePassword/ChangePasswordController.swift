//
//  ChangePasswordController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class ChangePasswordController: UIViewController {
    struct Props: Equatable {
        static let initial: Props = .init(passwordText: "", isPasswordAcceptable: false, onPasswordUpdated: .nop, onDisposed: .nop)

        let passwordText: String
        let isPasswordAcceptable: Bool
        let onPasswordUpdated: CommandWith<String?>
        let onPasswordChangeTapped: Command
        let onDisposed: Command

        var invalidPasswordText: String? {
            if isPasswordAcceptable {
                return nil
            } else {
                return "Password should be at least 4 characters long."
            }
        }

        init(passwordText: String,
             isPasswordAcceptable: Bool,
             onPasswordUpdated: CommandWith<String?> = .nop,
             onPasswordChangeTapped: Command = .nop,
             onDisposed: Command = .nop) {
            self.passwordText = passwordText
            self.isPasswordAcceptable = isPasswordAcceptable
            self.onPasswordUpdated = onPasswordUpdated
            self.onPasswordChangeTapped = onPasswordChangeTapped
            self.onDisposed = onDisposed
        }
    }

    var props: Props? {
        didSet {
            view.setNeedsLayout()
        }
    }

    private let changeButton: UIButton = with(UIButton(type: .system)) {
        $0.setTitle("Change Password", for: .normal)
    }

    private let errorLabel: UILabel = UILabel()

    private let passwordTextField: UITextField = with(UITextField()) {
        $0.backgroundColor = Colors.textFieldBackground
        $0.autocapitalizationType = .none
        $0.autocorrectionType = .no
        $0.returnKeyType = .done
    }

    deinit {
        props?.onDisposed.perform()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = Colors.background

        passwordTextField.addTarget(self, action: #selector(handleValueChange), for: .editingChanged)
        changeButton.addTarget(self, action: #selector(handleChangeTapped), for: .touchUpInside)

        passwordTextField.autoSetDimension(.height, toSize: 40)
        errorLabel.autoSetDimension(.height, toSize: 40)

        let stack = with(UIStackView(arrangedSubviews: [passwordTextField, errorLabel, changeButton])) {
            $0.axis = .vertical
            $0.spacing = 4
        }

        view.addSubview(stack)
        stack.autoAlignAxis(toSuperviewAxis: .horizontal)
        stack.autoPinEdge(toSuperviewEdge: .left)
        stack.autoPinEdge(toSuperviewEdge: .right)
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        let props = self.props ?? .initial

        passwordTextField.text = props.passwordText
        errorLabel.text = props.invalidPasswordText
        errorLabel.isHidden = props.isPasswordAcceptable
        changeButton.isEnabled = props.isPasswordAcceptable
    }

    // MARK: Actions
    @objc
    private func handleValueChange() {
        props?.onPasswordUpdated.perform(with: passwordTextField.text)
    }

    @objc
    private func handleChangeTapped() {
        props?.onPasswordChangeTapped.perform()
    }
}
