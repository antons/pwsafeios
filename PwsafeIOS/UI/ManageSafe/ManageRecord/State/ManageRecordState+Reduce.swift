//
//  ManageRecordState+Reduce.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func reduce(state: inout ManageRecordState, action: Action2) {
    // Reset one-time actions
    state.shouldShowConfirmDeleteAlert = false

    switch action {
    case is SwitchToEditRecord:
        state = .edit(record: state.record)
    case is SwitchToViewRecord:
        state = .view(record: state.record)
    case is AskToDeleteRecord:
        state.shouldShowConfirmDeleteAlert = true

    default: break
    }

    switch state.mode {
    case .view(var viewState):
        reduce(state: &viewState, action: action)
        state.mode = .view(viewState)

    case .edit(var editState):
        reduce(state: &editState, action: action)
        state.mode = .edit(editState)
    }
}

func reduce(state: inout ViewRecordState, action: Action2) {
    switch action {
    case let action as SelectRecordField:
        state.showMenu(for: action.fieldId)

    case let action as SelectFieldMenuItem:
        if action.menu == .reveal {
            state.setFieldHidden(id: action.fieldId, hidden: false)
        } else if action.menu == .conceal {
            state.setFieldHidden(id: action.fieldId, hidden: true)
        }
        state.hideMenu()

    default: break
    }
}

func reduce(state: inout EditRecordState, action: Action2) {
    switch action {
    case let action as EditRecordUpdateField:
        state.updateField(id: action.fieldId, value: action.value)

    default: break
    }
}
