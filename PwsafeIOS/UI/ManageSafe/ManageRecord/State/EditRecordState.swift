//
//  EditRecordState2.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift

struct EditRecordState: Equatable {
    struct Field: Equatable {
        var value: String?
    }

    let recordId: PwsafeRecordId
    var fields: [RecordFieldId: Field]

    static func make(from record: Record) -> EditRecordState {
        let fields: [RecordFieldId: Field] = [
            .title: Field(value: record.value(forKey: RecordKey.title)),
            .username: Field(value: record.value(forKey: RecordKey.username)),
            .password: Field(value: record.value(forKey: RecordKey.password)),
            .email: Field(value: record.value(forKey: RecordKey.email)),
            .website: Field(value: record.value(forKey: RecordKey.url)),
            .notes: Field(value: record.value(forKey: RecordKey.notes))
        ]

        return EditRecordState(recordId: record.uuid, fields: fields)
    }

    mutating func updateField(id: RecordFieldId, value: String?) {
        fields[id]?.value = value
    }
}
