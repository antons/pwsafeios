//
//  ManageRecordState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct ManageRecordState: Equatable {
    enum Mode: Equatable {
        case view(ViewRecordState)
        case edit(EditRecordState)
    }

    var record: PwsafeRecord
    var mode: Mode

    var shouldShowConfirmDeleteAlert: Bool

    init(record: PwsafeRecord, mode: Mode, shouldShowConfirmDeleteAlert: Bool = false) {
        self.record = record
        self.mode = mode
        self.shouldShowConfirmDeleteAlert = shouldShowConfirmDeleteAlert
    }

    static func view(record: PwsafeRecord) -> ManageRecordState {
        return ManageRecordState(
            record: record,
            mode: .view(.make(from: record)))
    }

    static func edit(record: PwsafeRecord) -> ManageRecordState {
        return ManageRecordState(
            record: record,
            mode: .edit(.make(from: record)))
    }

    mutating func update(_ record: PwsafeRecord) {
        switch mode {
        case .view:
            self = .view(record: record)
        case .edit:
            self = .edit(record: record)
        }
    }
}
