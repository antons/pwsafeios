//
//  ViewRecordState2.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift

enum FieldContextMenu: Equatable {
    case reveal
    case conceal
    case copy
}

struct ViewRecordState: Equatable {
    struct Menu: Equatable {
        let fieldId: RecordFieldId
        let items: [FieldContextMenu]
    }

    enum FieldValue: Equatable {
        case string(String?)
        case protectedString(String?, hidden: Bool)

        var visibleValue: String? {
            switch self {
            case .string(let value):
                return value
            case .protectedString(let value, let isHidden):
                return isHidden ? nil : value
            }
        }

        var stringValue: String? {
            switch self {
            case .string(let value):
                return value
            case .protectedString(let value, _):
                return value
            }
        }

        var isProtected: Bool {
            if case .protectedString = self {
                return true
            } else {
                return false
            }
        }
    }

    var fields: [RecordFieldId: FieldValue]
    var menu: Menu?

    static func make(from record: Record) -> ViewRecordState {
        let fields: [RecordFieldId: FieldValue] = [
            .title: .string(record.value(forKey: RecordKey.title)),
            .username: .string(record.value(forKey: RecordKey.username)),
            .password: .protectedString(record.value(forKey: RecordKey.password), hidden: true),
            .email: .string(record.value(forKey: RecordKey.email)),
            .website: .string(record.value(forKey: RecordKey.url)),
            .notes: .string(record.value(forKey: RecordKey.notes))
        ]

        return ViewRecordState(fields: fields, menu: nil)
    }

    mutating func showMenu(for fieldId: RecordFieldId) {
        guard let field = fields[fieldId] else { return }

        let items: [FieldContextMenu]
        switch field {
        case .string:
            items = [.copy]

        case .protectedString(_, hidden: true):
            items = [.copy, .reveal]

        case .protectedString(_, hidden: false):
            items = [.copy, .conceal]
        }

        menu = Menu(fieldId: fieldId, items: items)
    }

    mutating func setFieldHidden(id fieldId: RecordFieldId, hidden: Bool) {
        guard let field = fields[fieldId],
            case .protectedString(let value, _) = field else { return }

        fields[fieldId] = .protectedString(value, hidden: hidden)
    }

    mutating func hideMenu() {
        menu = nil
    }
}

enum RecordFieldId {
    case title
    case username
    case password
    case email
    case website
    case notes
}
