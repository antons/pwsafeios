//
//  ManageRecordCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

protocol ManageRecordCommands {
    func switchToEdit() -> Command

    func cancelEdit() -> Command

    func dispose() -> Command

    func updateSafe(_ makeSafe: @escaping () -> DecryptedSafe) -> Command

    func deleteRecord(recordId: PwsafeRecordId, updateSafe: @escaping () -> DecryptedSafe) -> Command

    func viewCommands() -> ViewRecordCommands

    func editCommands() -> EditRecordCommands
}

protocol ViewRecordCommands {
    func selectField(id: RecordFieldId) -> Command

    func selectMenuItem(fieldId: RecordFieldId, value: String?) -> CommandWith<FieldContextMenu>
}

protocol EditRecordCommands {
    func updateFieldValue(fieldId: RecordFieldId, value: String?) -> CommandWith<String?>

    func askToDeleteRecord(id: PwsafeRecordId) -> Command
}

final class ManageRecordCommandsProvider: ManageRecordCommands {
    private let dispatcher: Dispatcher
    private let viewCommandsProvider: ViewRecordCommands
    private let editCommandsProvider: EditRecordCommands
    private let service: CryptoSafeServiceProtocol
    private let router: ManageRecordRouterProtocol

    init(dispatcher: Dispatcher,
         viewCommands: ViewRecordCommands,
         editCommands: EditRecordCommands,
         service: CryptoSafeServiceProtocol,
         router: ManageRecordRouterProtocol) {
        self.dispatcher = dispatcher
        self.viewCommandsProvider = viewCommands
        self.editCommandsProvider = editCommands
        self.service = service
        self.router = router
    }

    func switchToEdit() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(SwitchToEditRecord())
        }
    }

    func cancelEdit() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(SwitchToViewRecord())
        }
    }

    func dispose() -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(ManageRecordDisposed())
        }
    }

    func updateSafe(_ makeSafe: @escaping () -> DecryptedSafe) -> Command {
        return Command { [service, dispatcher] in
            let safe = makeSafe()
            service.storeSafe(safe)
                .observe(on: UIScheduler())
                .on(completed: {
                    dispatcher.dispatch(UpdateSafe(safe: safe))
                    dispatcher.dispatch(SwitchToViewRecord())
                })
                .start()
        }
    }

    func deleteRecord(recordId: PwsafeRecordId, updateSafe: @escaping () -> DecryptedSafe) -> Command {
        return Command { [service, dispatcher, router] in
            let safe = updateSafe()
            service.storeSafe(safe)
                .observe(on: UIScheduler())
                .on(completed: {
                    dispatcher.dispatch(UpdateSafe(safe: safe))
                    dispatcher.dispatch(DeleteRecord(id: recordId))
                    router.dismissView()
                })
                .start()
        }
    }

    func viewCommands() -> ViewRecordCommands {
        return viewCommandsProvider
    }

    func editCommands() -> EditRecordCommands {
        return editCommandsProvider
    }
}

final class ViewRecordCommandsProvider: ViewRecordCommands {
    private let dispatcher: Dispatcher

    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }

    func selectField(id: RecordFieldId) -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(SelectRecordField(fieldId: id))
        }
    }

    func selectMenuItem(fieldId: RecordFieldId, value: String?) -> CommandWith<FieldContextMenu> {
        return CommandWith { [dispatcher] item in
            dispatcher.dispatch(SelectFieldMenuItem(fieldId: fieldId, menu: item))

            if item == .copy {
                let pasteboard = UIPasteboard.general
                pasteboard.string = value
                // TODO: set expiration date
            }
        }
    }
}

final class EditRecordCommandsProvider: EditRecordCommands {
    private let dispatcher: Dispatcher

    init(dispatcher: Dispatcher) {
        self.dispatcher = dispatcher
    }

    func updateFieldValue(fieldId: RecordFieldId, value: String?) -> CommandWith<String?> {
        return CommandWith<String?> { [dispatcher] value in
            dispatcher.dispatch(EditRecordUpdateField(fieldId: fieldId, value: value))
        }
    }

    func askToDeleteRecord(id: PwsafeRecordId) -> Command {
        return Command { [dispatcher] in
            dispatcher.dispatch(AskToDeleteRecord(id: id))
        }
    }
}
