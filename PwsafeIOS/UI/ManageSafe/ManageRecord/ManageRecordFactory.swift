//
//  ManageRecordFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/03/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import UIKit
import PwsafeSwift

class ManageRecordFactory {
    func manageRecord(id recordId: UUID, service: CryptoSafeServiceProtocol) -> UIViewController {
        let controller = ManageRecordController()

        let store = StateStore.store

        let viewCommands = ViewRecordCommandsProvider(dispatcher: store)
        let editCommands = EditRecordCommandsProvider(dispatcher: store)

        let router = ManageRecordRouter()

        let commands = ManageRecordCommandsProvider(
            dispatcher: store,
            viewCommands: viewCommands,
            editCommands: editCommands,
            service: service,
            router: router)

        router.controller = controller

        controller.connect(to: store, commands: commands)

        return controller
    }
}
