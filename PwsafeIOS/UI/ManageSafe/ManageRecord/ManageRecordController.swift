//
//  ManageRecordController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 24/04/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class ManageRecordController: UIViewController {
    struct Props {
        enum Mode {
            case edit(EditRecordController.Props, cancel: Command, save: Command)
            case view(ViewRecordController.Props, switchToEdit: Command)
        }

        enum Alert {
            case confirmDelete(delete: Command)
        }

        var editCommand: Command? {
            guard case .view(_, let command) = mode else { return nil }
            return command
        }

        var cancelCommand: Command? {
            guard case .edit(_, let command, _) = mode else { return nil }
            return command
        }

        var saveCommand: Command? {
            guard case .edit(_, _, let command) = mode else { return nil }
            return command
        }

        let mode: Mode
        let alert: Alert?
        let onDisposed: Command

        init(mode: Mode = .view(.empty, switchToEdit: .nop), alert: Alert? = nil, onDisposed: Command = .nop) {
            self.mode = mode
            self.alert = alert
            self.onDisposed = onDisposed
        }
    }

    var props: Props? {
        didSet {
            update(old: oldValue, new: props)
        }
    }

    private var currentController: UIViewController?

    private let viewRecordController: ViewRecordController = .init()
    private let editRecordController: EditRecordController = .init()

    private lazy var editButton: UIBarButtonItem = .init(title: "Edit", style: .plain, target: self, action: #selector(editTapped))
    private lazy var saveButton: UIBarButtonItem = .init(title: "Save", style: .plain, target: self, action: #selector(saveTapped))
    private lazy var cancelButton: UIBarButtonItem = .init(title: "Cancel", style: .plain, target: self, action: #selector(cancelTapped))

    deinit {
        props?.onDisposed.perform()
    }

    private func update(old: Props?, new: Props?) {
        guard let new = new else {
            removeCurrentController()
            return
        }

        switch new.mode {
        case .edit(let editProps, _, _):
            setController(editRecordController)
            editRecordController.props = editProps
            navigationItem.rightBarButtonItem = cancelButton
            navigationItem.leftBarButtonItem = saveButton

        case .view(let viewProps, _):
            view.endEditing(true)
            setController(viewRecordController)
            viewRecordController.props = viewProps
            navigationItem.rightBarButtonItem = editButton
            navigationItem.leftBarButtonItem = nil
        }

        switch new.alert {
        case .confirmDelete(let deleteCommand)?:
            showConfirmDelete(deleteCommand)
        case .none:
            break
        }
    }

    private func setController(_ controller: UIViewController) {
        guard controller != currentController else { return }
        removeCurrentController()
        addController(controller)
    }

    private func removeCurrentController() {
        removeController(currentController)
    }

    private func addController(_ controller: UIViewController) {
        addChild(controller)
        controller.view.frame = view.bounds
        view.addSubview(controller.view)
        controller.didMove(toParent: self)
    }

    private func removeController(_ controller: UIViewController?) {
        controller?.willMove(toParent: nil)
        controller?.view.removeFromSuperview()
        controller?.removeFromParent()
    }

    // MARK: Actions
    @objc
    private func cancelTapped() {
        props?.cancelCommand?.perform()
    }

    @objc
    private func saveTapped() {
        props?.saveCommand?.perform()
    }

    @objc
    private func editTapped() {
        props?.editCommand?.perform()
    }

    // MARK: Alert
    private func showConfirmDelete(_ delete: Command) {
        let alert = UIAlertController(
            title: "Delete Record",
            message: "Do you want to delete the record?",
            preferredStyle: .alert)

        alert.addCancelAction()
        alert.addAction(title: "Delete", style: .destructive, command: delete)

        present(alert, animated: true, completion: nil)
    }
}
