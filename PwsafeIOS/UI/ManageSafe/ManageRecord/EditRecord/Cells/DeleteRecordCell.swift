//
//  DeleteRecordCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/01/2019.
//  Copyright © 2019 Anton Selyanin. All rights reserved.
//

import UIKit

final class DeleteRecordCell: UITableViewCell, ConfigurableCell {
    struct Props {
        let delete: Command

        init(delete: Command = .nop) {
            self.delete = delete
        }
    }

    private let deleteButton: UIButton = with(.init(type: .system)) {
        $0.setTitle("Delete Record", for: .normal)
    }

    private var props: Props = .init()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        contentView.addSubview(deleteButton)

        with(deleteButton) {
            $0.autoPinEdgesToSuperviewEdges()

//            with($0.widthAnchor.constraint(equalToConstant: 60)) {
//                $0.priority = .defaultHigh
//                $0.isActive = true
//            }
            $0.autoSetDimension(.height, toSize: 44)
        }

        deleteButton.addTarget(self, action: #selector(handleDeleteTapped), for: .touchUpInside)
    }

    func configure(_ props: Props) {
        self.props = props
    }

    // MARK: Actions
    @objc
    private func handleDeleteTapped() {
        props.delete.perform()
    }
}
