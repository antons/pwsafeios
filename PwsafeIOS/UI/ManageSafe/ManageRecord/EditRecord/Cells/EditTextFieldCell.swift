//
//  EditFieldCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 03/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit
import PureLayout
import ReactiveSwift
import ReactiveCocoa

struct EditTextField {
    static let initial: EditTextField = .init(id: .title, title: "", text: nil, updateText: .nop)

    let id: RecordFieldId
    let title: String
    let text: String?
    let updateText: CommandWith<String?>
}

final class EditTextFieldCell: UITableViewCell, ConfigurableCell {
    private var props: EditTextField = .initial

    private let titleLabel: UILabel = with(UILabel()) {
        $0.textAlignment = .left
        $0.text = "Title"
    }

    private let valueField: UITextField = with(UITextField()) {
        $0.textAlignment = .left
        $0.autocapitalizationType = .none
        $0.autocorrectionType = .no
        $0.spellCheckingType = .no
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func configure(_ item: EditTextField) {
        let old = self.props
        self.props = item
        update(old: old, new: item)
    }

    func update(old: EditTextField, new: EditTextField) {
        setNeedsLayout()
    }

    private func setup() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(valueField)

        selectionStyle = .none

        valueField.addTarget(self, action: #selector(handleValueChange), for: .editingChanged)

        with(titleLabel) {
            $0.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .left, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .right)
        }

        with(valueField) {
            $0.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 8)
            $0.autoPinEdge(toSuperviewEdge: .left, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .right, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8)
            $0.autoSetDimension(.height, toSize: 24)
        }
    }

    override func layoutSubviews() {
        titleLabel.text = props.title
        valueField.text = props.text

        super.layoutSubviews()
    }

    // MARK: Actions
    @objc
    private func handleValueChange() {
        props.updateText.perform(with: valueField.text)
    }
}
