//
//  EditRecord+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 03/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from state: EditRecordState, commands: EditRecordCommands) -> EditRecordController.Props {
    func makeField(title: String, id: RecordFieldId) -> EditTextField {
        let value = state.fields[id]?.value

        return EditTextField(
            id: id,
            title: title,
            text: value,
            updateText: commands.updateFieldValue(fieldId: id, value: value))
    }

    let items: [[EditTextField]] = [
        [
            makeField(title: "Title", id: .title)
        ],
        [
            makeField(title: "Username", id: .username),
            makeField(title: "Password", id: .password)
        ],
        [
            makeField(title: "Email", id: .email)
        ],
        [
            makeField(title: "Website", id: .website)
        ],
        [
            makeField(title: "Notes", id: .notes)
        ]
    ]

    return .init(items: items, delete: commands.askToDeleteRecord(id: state.recordId))
}
