//
//  EditRecordTableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 03/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class EditRecordTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var items: [[EditTextField]] = [] {
        didSet {
            updateItems(old: oldValue, new: items)
        }
    }

    var deleteCommand: Command = .nop

    private var cells: [RecordFieldId: EditTextFieldCell] = [:]
    private let tableView: UITableView

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()

        setup()
    }

    private func setup() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44

        tableView.register(cellClass: DeleteRecordCell.self)
    }

    private func item(for indexPath: IndexPath) -> EditTextField {
        return items[indexPath.section][indexPath.item]
    }

    private func updateItems(old: [[EditTextField]], new: [[EditTextField]]) {
        if old.count != new.count {
            tableView.reloadData()
        } else {
            new.joined().forEach {
                cells[$0.id]?.configure($0)
            }
        }
    }

    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count + 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section < items.count {
            return items[section].count
        } else {
            return 1
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section < items.count {
            return configureItem(for: indexPath)
        }

        let deleteCell: DeleteRecordCell = tableView.dequeueCell(for: indexPath)
        deleteCell.configure(.init(delete: deleteCommand))
        return deleteCell
    }

    private func configureItem(for indexPath: IndexPath) -> UITableViewCell {
        let cellItem = item(for: indexPath)
        let cell = cells[cellItem.id, default: EditTextFieldCell(style: .default, reuseIdentifier: nil)]
        cells[cellItem.id] = cell
        cell.configure(cellItem)
        return cell
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SelectableCell {
            cell.didSelect()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
