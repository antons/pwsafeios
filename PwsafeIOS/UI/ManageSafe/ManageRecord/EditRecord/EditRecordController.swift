//
//  EditRecordController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 02/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class EditRecordController: UITableViewController {
    struct Props {
        let items: [[EditTextField]]
        let delete: Command

        init(items: [[EditTextField]] = [], delete: Command = .nop) {
            self.items = items
            self.delete = delete
        }
    }

    var props: Props = .init() {
        didSet {
            update(old: oldValue, new: props)
        }
    }

    convenience init() {
        self.init(style: .grouped)
    }

    private lazy var tableManager: EditRecordTableManager = .init(tableView: self.tableView)

    private func update(old: Props, new: Props) {
        tableManager.items = new.items
        tableManager.deleteCommand = new.delete
    }
}
