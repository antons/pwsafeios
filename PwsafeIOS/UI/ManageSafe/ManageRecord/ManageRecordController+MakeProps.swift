//
//  ManageRecordController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from safeState: ManageSafeState?, commands: ManageRecordCommands) -> ManageRecordController.Props? {
    guard let state = safeState?.manageRecord, let safe = safeState?.decryptedSafe else { return nil }

    func makeMode() -> ManageRecordController.Props.Mode {
        switch state.mode {
        case .view(let viewState):
            return .view(makeProps(from: viewState, commands: commands.viewCommands()), switchToEdit: commands.switchToEdit())

        case .edit(let editState):
            let record = state.record
            let fields = editState.fields
            let save = commands.updateSafe {
                let updatedRecord = update(record: record, from: fields)
                var updated = safe
                updated.pwsafe[updatedRecord.uuid] = updatedRecord
                return updated
            }

            return .edit(makeProps(from: editState, commands: commands.editCommands()), cancel: commands.cancelEdit(), save: save)
        }
    }

    let alert: ManageRecordController.Props.Alert? = {
        guard state.shouldShowConfirmDeleteAlert else { return nil }

        let deleteCommand = commands.deleteRecord(recordId: state.record.uuid) {
            var updated = safe
            updated.pwsafe.removeRecord(by: state.record.uuid)
            return updated
        }

        return .confirmDelete(delete: deleteCommand)
    }()

    return .init(mode: makeMode(), alert: alert, onDisposed: commands.dispose())
}

private func update(record: PwsafeRecord, from fields: [RecordFieldId: EditRecordState.Field]) -> PwsafeRecord {
    var updated = record

    for (fieldId, field) in fields {
        switch fieldId {
        case .title:
            updated.title = field.value
        case .username:
            updated.username = field.value
        case .password:
            updated.password = field.value
        case .email:
            updated.email = field.value
        case .website:
            updated.url = field.value
        case .notes:
            updated.notes = field.value
        }
    }

    return updated
}
