//
//  ManageRecordRouter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/01/2019.
//  Copyright © 2019 Anton Selyanin. All rights reserved.
//

import UIKit

protocol ManageRecordRouterProtocol: class {
    func dismissView()
}

final class ManageRecordRouter: ManageRecordRouterProtocol {
    weak var controller: UIViewController?

    func dismissView() {
        controller?.navigationController?.popController()
    }
}
