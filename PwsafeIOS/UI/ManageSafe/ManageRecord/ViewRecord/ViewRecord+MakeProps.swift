//
//  ViewRecord+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 02/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

func makeProps(from state: ViewRecordState, commands: ViewRecordCommands) -> ViewRecordController.Props {
    func makeField(title: String, id: RecordFieldId) -> ViewTextField {
        let menu: ViewTextField.Menu?
        if let stateMenu = state.menu, stateMenu.fieldId == id {
            let onSelectItem = commands.selectMenuItem(
                fieldId: id,
                value: state.fields[id]?.stringValue)

            menu = ViewTextField.Menu(
                items: stateMenu.items,
                onSelectItem: onSelectItem)
        } else {
            menu = nil
        }

        let fieldValue: ViewTextField.Value = {
            switch state.fields[id] {
            case .protectedString(let value, hidden: false)?:
                return .value(value)

            case .protectedString(_, hidden: true)?:
                return .concealed

            case .string(let value)?:
                return .value(value)

            case nil:
                return .value(nil)
            }
        }()

        return ViewTextField(
            id: id,
            title: title,
            value: fieldValue,
            menu: menu,
            onSelect: commands.selectField(id: id))
    }

    let items: [[ViewTextField]] = [
        [
            makeField(title: "Title", id: .title)
        ],
        [
            makeField(title: "Username", id: .username),
            makeField(title: "Password", id: .password)
        ],
        [
            makeField(title: "Email", id: .email)
        ],
        [
            makeField(title: "Website", id: .website)
        ],
        [
            makeField(title: "Notes", id: .notes)
        ]
    ]

    return .init(items: items)
}
