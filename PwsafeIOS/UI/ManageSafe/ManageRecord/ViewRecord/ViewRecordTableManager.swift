//
//  ViewRecordTableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/04/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class ViewRecordTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var items: [[ViewTextField]] = [] {
        didSet {
            updateItems(old: oldValue, new: items)
        }
    }

    private var cells: [RecordFieldId: TextFieldCell] = [:]
    private let tableView: UITableView

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()

        setup()
    }

    private func setup() {
        tableView.dataSource = self
        tableView.delegate = self

        tableView.rowHeight = UITableView.automaticDimension
        tableView.estimatedRowHeight = 44
    }

    private func item(for indexPath: IndexPath) -> ViewTextField {
        return items[indexPath.section][indexPath.item]
    }

    private func updateItems(old: [[ViewTextField]], new: [[ViewTextField]]) {
        if old.count != new.count {
            tableView.reloadData()
        } else {
            new.joined().forEach {
                cells[$0.id]?.configure($0)
            }
        }
    }

    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellItem = item(for: indexPath)
        let cell = cells[cellItem.id, default: TextFieldCell(style: .default, reuseIdentifier: nil)]
        cells[cellItem.id] = cell
        cell.configure(cellItem)
        return cell
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SelectableCell {
            cell.didSelect()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
