//
//  TextFieldCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/04/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import PureLayout
import ReactiveSwift
import ReactiveCocoa

struct ViewTextField {
    static let initial: ViewTextField = .init(id: .title, title: "", value: .concealed, menu: nil, onSelect: .nop)

    struct Menu {
        let items: [FieldContextMenu]
        let onSelectItem: CommandWith<FieldContextMenu>?
    }

    enum Value {
        case value(String?)
        case concealed
    }

    let id: RecordFieldId
    let title: String
    let value: Value
    let menu: Menu?
    let onSelect: Command

    var text: String? {
        switch value {
        case .value(let value):
            return value

        case .concealed:
            return "**********"
        }
    }
}

class TextFieldCell: UITableViewCell, ConfigurableCell {
    private static let actionSettings: [FieldContextMenu: (title: String, selector: Selector)] = [
        .copy: (title: "copy", selector: #selector(TextFieldCell.doCopy)),
        .reveal: (title: "reveal", selector: #selector(TextFieldCell.reveal)),
        .conceal: (title: "conceal", selector: #selector(TextFieldCell.conceal))
    ]

    private var props: ViewTextField = .initial

    private let titleLabel: UILabel = with(UILabel()) {
        $0.textAlignment = .left
    }

    let valueField: UILabel = with(UILabel()) {
        $0.textAlignment = .left
        $0.numberOfLines = 0
    }

    init(title: String) {
        super.init(style: .default, reuseIdentifier: nil)
        titleLabel.text = title
        setup()
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        layout()
    }

    private func layout() {
        contentView.addSubview(titleLabel)
        contentView.addSubview(valueField)

        with(titleLabel) {
            $0.autoPinEdge(toSuperviewEdge: .top, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .left, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .right)
        }

        with(valueField) {
            $0.autoPinEdge(.top, to: .bottom, of: titleLabel, withOffset: 8)
            $0.autoPinEdge(toSuperviewEdge: .left, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .right, withInset: 8)
            $0.autoPinEdge(toSuperviewEdge: .bottom, withInset: 8)

            $0.heightAnchor.constraint(greaterThanOrEqualToConstant: 24).isActive = true
        }
    }

    func configure(_ item: ViewTextField) {
        let old = self.props
        self.props = item
        update(old: old, new: item)
        setNeedsUpdateConstraints()
    }

    func update(old: ViewTextField, new: ViewTextField) {
        titleLabel.text = new.title
        valueField.text = new.text

        if new.menu != nil, (old.menu?.items != old.menu?.items || !UIMenuController.shared.isMenuVisible) {
            // TODO: This is a hack! Because I used `reloadData()` for updating table view, `superview` property is nil. So we need to skip current execute frame
            DispatchQueue.main.async {
                self.showMenu()
            }
        }
    }

    func didSelect() {
        props.onSelect.perform()
    }

    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(reveal) || action == #selector(conceal) || action == #selector(doCopy)
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    fileprivate func showMenu() {
        guard let menu = props.menu else { return }

        becomeFirstResponder()

        let menuController = UIMenuController.shared

        menuController.menuItems = menu.items.compactMap { (action: FieldContextMenu) in
            guard let settings = TextFieldCell.actionSettings[action] else { return nil }
            return UIMenuItem(title: settings.title, action: settings.selector)
        }

        menuController.setTargetRect(frame, in: superview!)
        menuController.setMenuVisible(true, animated: true)
    }

    @objc
    private func reveal() {
        props.menu?.onSelectItem?.perform(with: .reveal)
    }

    @objc
    private func conceal() {
        props.menu?.onSelectItem?.perform(with: .conceal)
    }

    @objc
    private func doCopy() {
        props.menu?.onSelectItem?.perform(with: .copy)
    }
}
