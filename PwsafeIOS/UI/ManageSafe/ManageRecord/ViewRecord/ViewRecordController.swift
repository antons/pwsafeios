//
//  ViewRecordControlelr.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit
import PureLayout

final class ViewRecordController: UIViewController {
    struct Props {
        let items: [[ViewTextField]]

        static let empty: Props = .init(items: [])
    }

    var props: Props? {
        didSet {
            update(old: oldValue, new: props)
        }
    }

    private let tableView: UITableView = .init(frame: .zero, style: .grouped)
    private lazy var tableManager: ViewRecordTableManager = .init(tableView: self.tableView)

    private func update(old: Props?, new: Props?) {
        tableManager.items = new?.items ?? []
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()
    }
}
