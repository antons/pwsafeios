//
//  AskToDeleteRecord.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 10/01/2019.
//  Copyright © 2019 Anton Selyanin. All rights reserved.
//

import Foundation

struct AskToDeleteRecord: Action2 {
    let id: PwsafeRecordId
}
