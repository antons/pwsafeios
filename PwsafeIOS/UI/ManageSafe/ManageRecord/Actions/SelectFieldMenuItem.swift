//
//  TappedMenuOnField.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 02/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct SelectFieldMenuItem: Action2 {
    let fieldId: RecordFieldId
    let menu: FieldContextMenu
}
