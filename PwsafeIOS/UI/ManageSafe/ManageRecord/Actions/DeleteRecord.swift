//
//  DeleteRecord.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 11/01/2019.
//  Copyright © 2019 Anton Selyanin. All rights reserved.
//

import Foundation

struct DeleteRecord: Action2 {
    let id: PwsafeRecordId
}
