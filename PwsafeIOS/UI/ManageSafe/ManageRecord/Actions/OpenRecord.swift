//
//  OpenRecord.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct OpenRecord: Action2 {
    let record: PwsafeRecord
}
