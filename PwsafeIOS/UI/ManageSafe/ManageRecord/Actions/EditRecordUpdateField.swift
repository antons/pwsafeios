//
//  EditRecordUpdateField.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct EditRecordUpdateField: Action2 {
    let fieldId: RecordFieldId
    let value: String?
}
