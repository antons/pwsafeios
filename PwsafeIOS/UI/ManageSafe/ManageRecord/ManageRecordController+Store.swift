//
//  ManageRecordController+Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension ManageRecordController {
    func connect(to store: Store<AppState>, commands: ManageRecordCommands) {
        let observer = CommandWith<AppState> { [weak self] state in
            self?.props = makeProps(from: state.manageSafe, commands: commands)
        }

        // TODO: store cancellation token
        store.observe(with: observer)
    }
}
