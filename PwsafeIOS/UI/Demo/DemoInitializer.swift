//
//  DemoInitializer.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import PwsafeSwift
import Swinject

final class DemoInitializer: Assembly {
    func assemble(container: Container) {
        // nop
    }

    func loaded(resolver: Resolver) {
        let storage = resolver.resolve(SyncedFileStorageProtocol.self)!

        setupDemoSafe(storage)
            .start()
    }
}

private func setupDemoSafe(_ syncedStorage: SyncedFileStorageProtocol) -> SignalProducer<Never, AppError> {
    let demoSafe = "demo"
    let password = "test"

    func containsDemoSafe(_ files: [FileStorageType: [SyncedFile]]) -> Bool {
        if let localFiles = files[.local], localFiles.contains(where: { $0.name.contains(demoSafe) }) {
            return true
        } else {
            return false
        }
    }

    return syncedStorage.files.producer
        .take(first: 1)
        .flatMap(.latest) { files -> SignalProducer<Never, AppError> in
            guard !containsDemoSafe(files) else { return .empty }

            return makeDemoSafe()
                .toDataProducer(with: password)
                .flatMap(.latest) { data -> SignalProducer<Never, AppError> in
                    return syncedStorage
                        .addFile(FileEntry(path: demoSafe), storageType: .local, data: data, metadata: "")
                        .then(SignalProducer<Never, AppError>.empty)
                }
        }
}

private func makeDemoSafe() -> Pwsafe {
    var safe = Pwsafe()

    safe.header.timestampOfLastSave = Date()
    safe.header.databaseName = "Demo Password Database"

    let record1 = with(Record()) {
        $0.title = "Record1"
        $0.username = "username"
        $0.password = "password"
        $0.email = "email@none.no1"
        $0.url = "https://apple.com"
    }

    safe[record1.uuid] = record1

    let record2 = with(Record()) {
        $0.title = "Record2"
        $0.username = "email@none.no1"
        $0.password = "pwd"
        $0.group = Group(segments: ["level0", "level1"])
    }

    safe[record2.uuid] = record2

    let record3 = with(Record()) {
        $0.title = "Record3"
        $0.username = "email@none.no1"
        $0.password = "pwd"
        $0.group = Group(segments: ["level0"])
    }

    safe[record3.uuid] = record3

    return safe
}
