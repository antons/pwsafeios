//
//  LinkSafeCoordinator.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/11/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol LinkSafeOutput: class {
    var successSignal: Signal<SyncedFile, Never> { get }

    var errorSignal: Signal<String, Never> { get }
}

class LinkSafeCoordinator: Coordinator, LinkSafeOutput {
    let successSignal: Signal<SyncedFile, Never>
    private let successObserver: Signal<SyncedFile, Never>.Observer

    let errorSignal: Signal<String, Never>
    private let errorObserver: Signal<String, Never>.Observer

    private let storageType: FileStorageType
    private let storageRegistrar: StorageRegistrarProtocol
    private let fileEntry: FileEntry
    private let syncedFileStorage: SyncedFileStorageProtocol

    init(storageType: FileStorageType, storageRegistrar: StorageRegistrarProtocol,
         fileEntry: FileEntry, syncedFileStorage: SyncedFileStorageProtocol) {
        (successSignal, successObserver) = Signal.pipe()
        (errorSignal, errorObserver) = Signal.pipe()

        self.storageType = storageType
        self.storageRegistrar = storageRegistrar
        self.fileEntry = fileEntry
        self.syncedFileStorage = syncedFileStorage
    }

    func start() {
        guard let fileStorage: FileStorage = storageRegistrar.storage(type: storageType) else {
            errorObserver.send(value: "File storage of type '\(storageType)' is not available")
            errorObserver.sendCompleted()
            return
        }

        fileStorage.fetchData(fileEntry)
            .mapError(AppError.internalError)
            .flatMap(.concat, self.addSafe)
            .startWithResult { result in
                guard let value = result.value else {
                    self.errorObserver.send(value: "Can't fetch file from file storage")
                    self.errorObserver.sendCompleted()
                    return
                }

                self.successObserver.send(value: value)
                self.successObserver.sendCompleted()
            }
    }

    private func addSafe(_ fileData: FileData) -> SignalProducer<SyncedFile, AppError> {
        return self.syncedFileStorage.addFile(
            fileEntry, storageType: storageType, data: fileData.data, metadata: fileData.metadata)
    }
}
