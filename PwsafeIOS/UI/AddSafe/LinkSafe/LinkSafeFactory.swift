//
//  LinkSafeFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/11/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import ReactiveCocoa


class LinkSafeFactory {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func linkSafe(_ fileEntry: FileEntry, storage: FileStorageType) -> Coordinator & LinkSafeOutput {
        let storageRegistrar = parentContainer.resolve(StorageRegistrarProtocol.self)!
        let syncedFileStorage = parentContainer.resolve(SyncedFileStorageProtocol.self)!

        return LinkSafeCoordinator(storageType: storage,
                                   storageRegistrar: storageRegistrar,
                                   fileEntry: fileEntry, syncedFileStorage: syncedFileStorage)
    }
}
