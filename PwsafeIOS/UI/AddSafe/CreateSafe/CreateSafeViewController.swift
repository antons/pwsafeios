//
//  CreateSafeViewController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import ReactiveCocoa

class CreateSafeViewController: UIViewController {
    private let nameTextField: UITextField = with(UITextField()) {
        $0.placeholder = "Name"
        $0.backgroundColor = Colors.textFieldBackground
    }

    private let passwordTextField: UITextField = with(UITextField()) {
        $0.placeholder = "Enter you password here"
        $0.backgroundColor = Colors.textFieldBackground
        $0.autocapitalizationType = .none
        $0.autocorrectionType = .no
    }

    private let closeButton: UIBarButtonItem
    private let createButton: UIButton = with(UIButton(type: .system)) {
        $0.setTitle("Create Safe", for: .normal)
    }

    private let viewModel: CreateSafeViewModel

    init(viewModel: CreateSafeViewModel) {
        self.viewModel = viewModel
        self.closeButton = UIBarButtonItem(title: "Cancel", action: viewModel.closeAction)

        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = Colors.background

        nameTextField.autoSetDimension(.height, toSize: 40)
        passwordTextField.autoSetDimension(.height, toSize: 40)

        let stack = UIStackView(arrangedSubviews: [nameTextField, passwordTextField, createButton])
        view.addSubview(stack)

        stack.axis = .vertical
        stack.spacing = 10
        stack.autoPinEdge(toSuperviewEdge: .left).constant = 20
        stack.autoPinEdge(toSuperviewEdge: .right).constant = -20
        stack.autoCenterInSuperview()

        bindViewModel()
    }

    private func bindViewModel() {
        viewModel.name <~ nameTextField.reactive.continuousTextValues
        viewModel.password <~ passwordTextField.reactive.continuousTextValues
        createButton.reactive.pressed = CocoaAction(viewModel.createAction)

        navigationItem.rightBarButtonItem = closeButton
    }
}
