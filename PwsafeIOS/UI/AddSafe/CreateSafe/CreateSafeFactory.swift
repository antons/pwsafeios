//
//  CreateSafeFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 27/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import UIKit

class CreateSafeFactory {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func createSafe() -> (CreateSafeOutput, UIViewController) {
        let container = Container(parent: parentContainer)
        assemble(container)

        let output = container.resolve(CreateSafeViewModel.self)!
        let controller = container.resolve(CreateSafeViewController.self)!

        return (output, controller)
    }

    private func assemble(_ container: Container) {
        container.register(CreateSafeInteractor.self) { resolver in
            CreateSafeInteractor(syncedFileStorage: resolver.resolve(SyncedFileStorageProtocol.self)!)
        }.inObjectScope(.container)

        container.register(CreateSafeViewModel.self) { resolver in
            CreateSafeViewModel(interactor: resolver.resolve(CreateSafeInteractor.self)!)
        }.inObjectScope(.container)

        container.register(CreateSafeViewController.self) { resolver in
            CreateSafeViewController(viewModel: resolver.resolve(CreateSafeViewModel.self)!)
        }.inObjectScope(.container)
    }
}
