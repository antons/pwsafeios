//
//  CreateSafeInteractor.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import PwsafeSwift

class CreateSafeInteractor {
    private let syncedFileStorage: SyncedFileStorageProtocol
//    private let parentFolder: FileEntry

    init(syncedFileStorage: SyncedFileStorageProtocol) {
        self.syncedFileStorage = syncedFileStorage
    }

    func createPwsafe(name: String, password: String) -> SignalProducer<DecryptedSafe, AppError> {
        let pwsafe = Pwsafe()

        return pwsafe.toDataProducer(with: password)
            .flatMap(.latest) { data in
                return self.syncedFileStorage.addFile(FileEntry(path: name), storageType: .local, data: data, metadata: "")
            }
            .map { syncedFile in
                return DecryptedSafe(syncedFile: syncedFile, pwsafe: pwsafe, password: password)
            }
    }
}
