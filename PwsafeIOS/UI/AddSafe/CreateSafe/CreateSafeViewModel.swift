//
//  CreateSafeViewModel.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


class CreateSafeViewModel: CreateSafeOutput {
    let name: MutableProperty<String?>
    let password: MutableProperty<String?>

    var createSafeSignal: Signal<DecryptedSafe, Never> {
        return createAction.values
    }

    var errorSignal: Signal<String, Never> {
        return createAction.errors.map({ _ in "Error" })
    }

    var closeSignal: Signal<Void, Never> {
        return closeAction.completed
    }

    let createAction: Action<Void, DecryptedSafe, AppError>
    let closeAction: VoidAction = .empty

    init(interactor: CreateSafeInteractor) {
        let name: MutableProperty<String?> = MutableProperty("")
        let password: MutableProperty<String?> = MutableProperty("")

        self.name = name
        self.password = password

        let enabledProducer = SignalProducer.combineLatest(name.producer.notEmpty(), password.producer.notEmpty())
            .map { $0 && $1 }

        let enabled = Property(initial: false, then: enabledProducer)

        createAction = Action(enabledIf: enabled) { _ in
            return interactor.createPwsafe(name: name.value ?? "", password: password.value ?? "")
        }
    }
}
