//
//  CreateSafeOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 11/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol CreateSafeOutput: class {
    var createSafeSignal: Signal<DecryptedSafe, Never> { get }

    var errorSignal: Signal<String, Never> { get }

    var closeSignal: Signal<Void, Never> { get }
}
