//
//  AddSafeFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import Swinject

class AddSafeFactory {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func addSafe() -> (AddSafeOutput, UIViewController) {
        let router = WeakRouterContainer()
        let context = AddSafeContext(parentContainer: parentContainer)
        let coordinator = AddSafeCoordinator(context: context, router: router)

        let controller = SafeSourceListFactory()
            .list(title: "Safe Source", output: coordinator, items: [.createOnDevice, .linkFromDropbox, .createInDropbox])

        router.router = controller

        return (coordinator, controller)
    }
}
