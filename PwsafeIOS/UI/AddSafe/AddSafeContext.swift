//
//  AddSafeContext.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import PwsafeSwift
import ReactiveCocoa


protocol AddSafeContextProtocol {
    func selectFile(storage: FileStorageType) -> (Coordinator & FileBrowserOutput, UIViewController)

    func createSafe(storage: FileStorageType, folder: FileEntry?) -> (CreateSafeOutput, UIViewController)
}

class AddSafeContext: AddSafeContextProtocol {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func selectFile(storage: FileStorageType) -> (Coordinator & FileBrowserOutput, UIViewController) {
        return FileBrowserFactory(browsable: DropboxFileBrowser()).browseFiles()
    }

    func createSafe(storage: FileStorageType, folder: FileEntry?) -> (CreateSafeOutput, UIViewController) {
        return CreateSafeFactory(parentContainer: parentContainer).createSafe()
    }

    func linkSafe(storage: FileStorageType, file: FileEntry) -> Coordinator & LinkSafeOutput {
        return LinkSafeFactory(parentContainer: parentContainer).linkSafe(file, storage: storage)
    }
}
