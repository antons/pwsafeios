//
//  AddSafeCoordinator.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


class AddSafeCoordinator: Coordinator, AddSafeOutput, ListOutput {
    let safeAddedSignal: Signal<DecryptedSafe, Never>
    private let safeAddedObserver: Signal<DecryptedSafe, Never>.Observer

    let safeLinkedSignal: Signal<SyncedFile, Never>
    private let safeLinkedObserver: Signal<SyncedFile, Never>.Observer

    let cancelSignal: VoidSignal
    private let cancelObserver: VoidObserver

    private let context: AddSafeContext
    private let router: RouterProtocol

    init(context: AddSafeContext, router: RouterProtocol) {
        self.context = context
        self.router = router
        (safeAddedSignal, safeAddedObserver) = Signal.pipe()
        (safeLinkedSignal, safeLinkedObserver) = Signal.pipe()
        (cancelSignal, cancelObserver) = Signal.pipe()
    }

    func start() {
        //nop?
    }

    func selectedItem(_ item: SafeSourceType) {
        switch item {
        case .createOnDevice:
            createSafe(storage: .local, folder: nil)

        case .createInDropbox:
            selectFile(storage: .dropbox)

        case .linkFromDropbox:
            linkFile(storage: .dropbox)
        }
    }

    func listClosed() {
        cancelObserver.send(value: ())
    }

    private func createSafe(storage: FileStorageType, folder: FileEntry?) {
        let (output, controller) = context.createSafe(storage: storage, folder: folder)

        output.closeSignal.observeValues { [weak self] in
            self?.router.dismissController()
        }

        output.createSafeSignal
            .observe(safeAddedObserver)

        router.push(controller)
    }

    private func selectFile(storage: FileStorageType) {
        let (coordinator, controller) = context.selectFile(storage: storage)

        coordinator.cancelSignal.observeValues { [weak self] in
            self?.router.dismissController()
        }

        coordinator.selectedFileSignal.observeValues { [weak self] fileEntry in
            self?.createSafe(storage: storage, folder: fileEntry)
        }

        coordinator.start()

        router.push(controller)
    }

    private func linkFile(storage: FileStorageType) {
        let (coordinator, controller) = context.selectFile(storage: storage)

        coordinator.cancelSignal.observeValues { [weak self] in
            self?.router.dismissController()
        }

        coordinator.selectedFileSignal.observeValues { [weak self] fileEntry in
            self?.linkSafe(storage: storage, file: fileEntry)
        }

        coordinator.start()

        router.push(controller)
    }

    private func linkSafe(storage: FileStorageType, file: FileEntry) {
        let coordinator = context.linkSafe(storage: storage, file: file)

        coordinator.successSignal.observe(safeLinkedObserver)

        coordinator.errorSignal.observeValues { _ in
            //TODO: show error
        }

        coordinator.start()
    }
}
