//
//  SafeSourcesListOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveCocoa


enum SafeSourceType {
    case createOnDevice
    case linkFromDropbox
    case createInDropbox
}

extension SafeSourceType: CustomStringConvertible {
    var description: String {
        switch self {
        case .createOnDevice:
            return "Create on device"

        case .linkFromDropbox:
            return "Link from Dropbox"

        case .createInDropbox:
            return "Create in Dropbox"
        }
    }
}

typealias SafeSourceListFactory = ListViewFactory<SafeSourceType, StringTableCell<SafeSourceType>>
