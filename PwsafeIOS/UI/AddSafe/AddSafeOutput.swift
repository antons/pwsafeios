//
//  AddSafeOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 11/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol AddSafeOutput {
    var safeAddedSignal: Signal<DecryptedSafe, Never> { get }

    var safeLinkedSignal: Signal<SyncedFile, Never> { get }

    var cancelSignal: VoidSignal { get }
}
