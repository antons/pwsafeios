//
//  SafeListController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 15/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

struct SafeListSection2: Equatable {
    let name: String
    let items: [SafeListItem]
}

struct SafeListItem: Equatable {
    enum State {
        case synced
        case notSynced
        case conflict
    }

    let name: String
    let state: State
    let onSelect: Command
}

final class SafeListController: UIViewController {
    struct Props {
        let sections: [SafeListSection2]

        let onSync: Command
        let onAddSafe: Command
        let onLinkDropbox: CommandWith<UIViewController>

        init(sections: [SafeListSection2] = [],
             onSync: Command = .nop,
             onAddSafe: Command = .nop,
             onLinkDropbox: CommandWith<UIViewController> = .nop) {
            self.sections = sections
            self.onSync = onSync
            self.onAddSafe = onAddSafe
            self.onLinkDropbox = onLinkDropbox
        }
    }

    var props: Props = .init() {
        didSet {
            tableManager.items = props.sections
        }
    }

    private let tableView: UITableView = .init(frame: .zero, style: .grouped)
    private lazy var tableManager: SafeListTableManager = .init(tableView: self.tableView)

    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()

        let addSafeButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(handleAddSafe))
        let linkDropboxButton = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(handleLinkDropbox))
        let syncButton = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(handleSync))

        toolbarItems = [
            addSafeButton,
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace),
            linkDropboxButton,
            syncButton]

        tableView.rowHeight = UITableView.automaticDimension
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        navigationController?.isToolbarHidden = false
    }

    @objc
    private func handleLinkDropbox() {
        props.onLinkDropbox.perform(with: self)
    }

    @objc
    private func handleAddSafe() {
        props.onAddSafe.perform()
    }

    @objc
    private func handleSync() {
        props.onSync.perform()
    }
}
