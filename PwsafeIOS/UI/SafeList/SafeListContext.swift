//
//  SafeListContext.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 25/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import ReactiveSwift

protocol SafeListContextProtocol {
    func openSafe(_ syncedFile: SyncedFile) -> (OpenSafeOutput, UIViewController)

    func manageSafe(_ decryptedSafe: DecryptedSafe) -> UIViewController

    func addSafe() -> (AddSafeOutput, UIViewController)
}

class SafeListContext: SafeListContextProtocol {
    private let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func openSafe(_ syncedFile: SyncedFile) -> (OpenSafeOutput, UIViewController) {
        return OpenSafeFactory(parentContainer: parentContainer).openSafe(syncedFile)
    }

    func manageSafe(_ decryptedSafe: DecryptedSafe) -> UIViewController {
        // TODO: extract dependency
        let store = StateStore.store
        let screenId = UUID()
        store.dispatch(ManageSafe(decryptedSafe: decryptedSafe))
        store.dispatch(OpenGroup(screenId: screenId, filter: .all))

        let service = parentContainer.resolve(CryptoSafeService.self)!

        let controller = ManageSafeFactory().manageSafe(screenId: screenId, filter: .all, service: service)

        return controller
    }

    func addSafe() -> (AddSafeOutput, UIViewController) {
        return AddSafeFactory(parentContainer: parentContainer).addSafe()
    }
}
