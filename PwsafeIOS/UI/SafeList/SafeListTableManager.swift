//
//  SafeListTableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 15/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class SafeListTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var items: [SafeListSection2] = [] {
        didSet {
            updateItems(old: oldValue, new: items)
        }
    }

    private let tableView: UITableView

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.setup()
    }

    private func setup() {
        tableView.register(cellClass: SafeCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }

    private func item(for indexPath: IndexPath) -> SafeListItem {
        return items[indexPath.section].items[indexPath.item]
    }

    private func updateItems(old: [SafeListSection2], new: [SafeListSection2]) {
        tableView.reloadData()
    }

    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return items.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellItem = item(for: indexPath)
        let cell = tableView.dequeueCell(for: indexPath) as SafeCell
        cell.configure(cellItem)
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return items[section].name
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SelectableCell {
            cell.didSelect()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
