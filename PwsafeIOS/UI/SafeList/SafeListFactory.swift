//
//  SafeListFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import ReactiveSwift
import ReactiveCocoa

class SafeListFactory {
    let parentContainer: Container

    init(parentContainer: Container) {
        self.parentContainer = parentContainer
    }

    func safeList() -> UIViewController {
        let store = StateStore.store

        let controller = SafeListController()
        let router = WeakRouterContainer()
        router.router = controller

        let context = SafeListContext(parentContainer: parentContainer)

        let commands = SafeListCommandsProvider(
            dispatcher: store,
            router: router,
            context: context,
            dropboxSyncService: parentContainer.resolve(DropboxSyncService.self)!,
            dropboxLinker: parentContainer.resolve(DropboxLinkerProtocol.self)!)

        controller.connect(to: store, commands: commands)

        let service = parentContainer.resolve(SyncedFileStorageProtocol.self)!

        service.files.producer
            .take(duringLifetimeOf: controller)
            .observe(on: UIScheduler())
            .on(value: { files in
                store.dispatch(UpdateSafeFiles(files: files))
            })
            .start()

        return controller
    }
}
