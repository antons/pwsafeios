//
//  SafeListController+MakeProps.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func makeProps(from state: SafeListState?, commands: SafeListCommands) -> SafeListController.Props {
    guard let state = state else { return .init() }

    let storageOrder: [FileStorageType] = [.local, .dropbox, .icloud]

    func fileToItem(_ file: SyncedFile) -> SafeListItem {
        return SafeListItem(
            name: file.name,
            state: syncStateToItemState(file.syncState),
            onSelect: commands.selectFile(file))
    }

    let sections: [SafeListSection2] = storageOrder
        .compactMap { storage in
            guard let files = state.files[storage], !files.isEmpty else { return nil }

            return SafeListSection2(
                name: fileStorageName(storage),
                items: files.map(fileToItem))
        }

    return .init(
        sections: sections,
        onSync: commands.sync(),
        onAddSafe: commands.addSafe(),
        onLinkDropbox: commands.linkDropbox())
}

private func syncStateToItemState(_ syncState: FileSyncState) -> SafeListItem.State {
    switch syncState {
    case .synced:
        return .synced
    case .notSynced:
        return .notSynced
    case .conflict:
        return .conflict
    }
}

private func fileStorageName(_ storageType: FileStorageType) -> String {
    switch storageType {
    case .local:
        return "Local"
    case .dropbox:
        return "Dropbox"
    case .icloud:
        return "iCloud"
    }
}
