//
//  SafeCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 16/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

class SafeCell: UITableViewCell, ConfigurableCell {
    private var props: SafeListItem?

    private let nameLabel: UILabel = .init()

    private let syncStateLabel: UILabel = with(UILabel()) {
        $0.font = .systemFont(ofSize: UIFont.smallSystemFontSize)
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setup()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
        // This is an experiment with layout using layout anchors
        // It's too much code :(

        contentView.addSubview(nameLabel)
        nameLabel.translatesAutoresizingMaskIntoConstraints = false

        contentView.addSubview(syncStateLabel)
        syncStateLabel.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 6),
            nameLabel.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),

            syncStateLabel.leftAnchor.constraint(equalTo: nameLabel.leftAnchor),
            syncStateLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            syncStateLabel.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -4)
            ])
    }

    // MARK: ConfigurableCell
    func configure(_ item: SafeListItem) {
        self.props = item

        nameLabel.text = item.name
        syncStateLabel.text = item.state.name
        syncStateLabel.textColor = item.state.color
    }

    // MARK: SelectableCell
    func didSelect() {
        props?.onSelect.perform()
    }
}

private extension SafeListItem.State {
    var name: String {
        switch self {
        case .synced:
            return "Synced"
        case .notSynced:
            return "Not Synced"
        case .conflict:
            return "Conflict"
        }
    }

    var color: UIColor {
        switch self {
        case .synced, .notSynced:
            return .gray

        case .conflict:
            return .red
        }
    }
}
