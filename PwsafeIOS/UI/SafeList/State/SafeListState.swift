//
//  SafeListState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 15/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct SafeListState {
    var files: [FileStorageType: [SyncedFile]] = [:]
}
