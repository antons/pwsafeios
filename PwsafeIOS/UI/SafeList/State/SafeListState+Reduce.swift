//
//  SafeListState+Reduce.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 18/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

func reduce(state: inout SafeListState, action: Action2) {
    switch action {
    case let action as UpdateSafeFiles:
        state.files = action.files

    default: break
    }
}
