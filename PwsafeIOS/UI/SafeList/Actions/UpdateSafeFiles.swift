//
//  UpdateSafeFiles.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 18/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct UpdateSafeFiles: Action2 {
    let files: [FileStorageType: [SyncedFile]]
}
