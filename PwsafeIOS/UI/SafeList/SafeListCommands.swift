//
//  SafeListCommands.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveCocoa
import ReactiveSwift
import UIKit

protocol SafeListCommands: class {
    func selectFile(_ file: SyncedFile) -> Command

    func sync() -> Command

    func addSafe() -> Command

    func linkDropbox() -> CommandWith<UIViewController>
}

final class SafeListCommandsProvider: SafeListCommands {
    private let dispatcher: Dispatcher
    private let router: RouterProtocol
    private let context: SafeListContextProtocol
    private let dropboxSyncService: DropboxSyncService
    private let dropboxLinker: DropboxLinkerProtocol

    init(dispatcher: Dispatcher,
         router: RouterProtocol,
         context: SafeListContextProtocol,
         dropboxSyncService: DropboxSyncService,
         dropboxLinker: DropboxLinkerProtocol) {
        self.dispatcher = dispatcher
        self.router = router
        self.context = context
        self.dropboxSyncService = dropboxSyncService
        self.dropboxLinker = dropboxLinker
    }

    func selectFile(_ file: SyncedFile) -> Command {
        return Command { [weak self] _ in
            self?.openSafe(file)
        }
    }

    func sync() -> Command {
        return Command { [dropboxSyncService] in
            dropboxSyncService
                .sync()
                .start()
        }
    }

    func addSafe() -> Command {
        return Command { [weak self] in
            self?.doAddSafe()
        }
    }

    func linkDropbox() -> CommandWith<UIViewController> {
        return CommandWith { [dropboxLinker] controller in
            dropboxLinker.link(fromController: controller)
        }
    }

    private func openSafe(_ syncedFile: SyncedFile) {
        dispatcher.dispatch(OpenSafe(file: syncedFile))

        let (output, controller) = context.openSafe(syncedFile)

        router.presentInNavigation(controller)

        output.resultSignal
            .take(first: 1)
            .observe(on: UIScheduler())
            .observeValues { [weak self] result in
                self?.router.dismissController()

                switch result {
                case .openSafe(let safe):
                    self?.manageSafe(safe)

                case .close:
                    break
                }
        }
    }

    private func manageSafe(_ decryptedSafe: DecryptedSafe) {
        let controller = context.manageSafe(decryptedSafe)
        router.push(controller)
    }

    private func doAddSafe() {
        let (output, controller) = context.addSafe()

        router.presentInNavigation(controller)

        output.safeAddedSignal
            // TODO: this is a hack
            .observe(on: UIScheduler())
            .observeValues { [weak self] decryptedSafe in
                self?.router.dismissController()
                self?.manageSafe(decryptedSafe)
            }

        output.safeLinkedSignal
            // TODO: this is a hack
            .observe(on: UIScheduler())
            .observeValues { [weak self] syncedFile in
                self?.router.dismissController()
                self?.openSafe(syncedFile)
            }

        output.cancelSignal.observeValues { [weak self] in
            self?.router.dismissController()
        }
    }
}
