//
//  Router.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 26/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

protocol RouterProtocol: class {
    func present(_ controller: UIViewController)

    func presentInNavigation(_ controller: UIViewController)

    func dismissController()

    func push(_ controller: UIViewController)

    func popController()
}

extension UIViewController: RouterProtocol {
    func present(_ controller: UIViewController) {
        self.present(controller, animated: true, completion: nil)
    }

    func presentInNavigation(_ controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        present(navigationController)
    }

    func dismissController() {
        dismiss(animated: true, completion: nil)
    }

    func push(_ controller: UIViewController) {
        if let nc = self as? UINavigationController {
            nc.pushViewController(controller, animated: true)
        } else {
            navigationController?.pushViewController(controller, animated: true)
        }
    }

    func popController() {
        if let nc = self as? UINavigationController {
            nc.popViewController(animated: true)
        } else {
            _ = navigationController?.popViewController(animated: true)
        }
    }
}

class Router: RouterProtocol {
    private weak var controller: UIViewController?

    init(controller: UIViewController) {
        self.controller = controller
    }

    func present(_ controller: UIViewController) {
        self.controller?.present(controller, animated: true, completion: nil)
    }

    func presentInNavigation(_ controller: UIViewController) {
        let navigationController = UINavigationController(rootViewController: controller)
        self.controller?.present(navigationController)
    }

    func dismissController() {
        self.controller?.dismiss(animated: true, completion: nil)
    }

    func push(_ controller: UIViewController) {
        self.controller?.navigationController?.pushViewController(controller, animated: true)
    }

    func popController() {
        _ = self.controller?.navigationController?.popViewController(animated: true)
    }
}

class WeakRouterContainer: RouterProtocol {
    weak var router: RouterProtocol?

    func present(_ controller: UIViewController) {
        router?.present(controller)
    }

    func presentInNavigation(_ controller: UIViewController) {
        router?.presentInNavigation(controller)
    }

    func dismissController() {
        router?.dismissController()
    }

    func push(_ controller: UIViewController) {
        router?.push(controller)
    }

    func popController() {
        router?.popController()
    }
}
