//
//  AppState.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift

// TODO: this is a temp store storage
enum StateStore {
    static let store: Store<AppState> = .init(
        state: AppState(),
        reducer: reduce(state:action:),
        queue: DispatchQueue(label: "Store", qos: .userInteractive))
}

struct AppState {
    var safeList: SafeListState = .init()
    var manageSafe: ManageSafeState?
    var openSafe: OpenSafeState?
}

func reduce(state: inout AppState, action: Action2) {
    switch action {
    case let action as ManageSafe:
        state.manageSafe = ManageSafeState(
            decryptedSafe: action.decryptedSafe,
            manageRecord: nil,
            screens: [:])

    case is SafeDidClose:
        guard let manageSafe = state.manageSafe, manageSafe.screens.count < 2 else { break }
        state.manageSafe = nil

    case let action as OpenSafe:
        state.openSafe = OpenSafeState(file: action.file, enteredPassword: nil, isValid: false)

    case is OpenSafeDispose:
        state.openSafe = nil

    default:
        break
    }

    reduce(state: &state.safeList, action: action)

    if var manageSafe = state.manageSafe {
        reduce(state: &manageSafe, action: action)
        state.manageSafe = manageSafe
    }

    if var openSafe = state.openSafe {
        reduce(state: &openSafe, action: action)
        state.openSafe = openSafe
    }
}
