//
//  Coordinator.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 26/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

protocol Coordinator: class {
    func start()
}
