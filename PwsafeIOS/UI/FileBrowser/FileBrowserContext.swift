//
//  FileBrowserContext.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

protocol FileBrowserContextProtocol {
    func listFiles<Output: ListOutput>(_ fileEntry: FileEntry, output: Output) -> UIViewController
        where Output.Item == FolderEntry
}

class FileBrowserContext: FileBrowserContextProtocol {
    private let factory: FileBrowserFactory

    init(factory: FileBrowserFactory) {
        self.factory = factory
    }

    func listFiles<Output: ListOutput>(_ fileEntry: FileEntry, output: Output) -> UIViewController where Output.Item == FolderEntry {
        return factory.listFiles(fileEntry, output: output)
    }
}
