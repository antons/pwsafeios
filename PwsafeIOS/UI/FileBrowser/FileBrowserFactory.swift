//
//  FileBrowserFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import Swinject
import ReactiveCocoa


typealias FileListFactory = ListViewFactory<FolderEntry, StringTableCell<FolderEntry>>

class FileBrowserFactory {
    private let browsable: FileBrowsable

    init(browsable: FileBrowsable) {
        self.browsable = browsable
    }

    func listFiles<Output: ListOutput>(_ root: FileEntry, output: Output) -> UIViewController where Output.Item == FolderEntry {
        return FileListFactory().list(title: root.name, output: output, itemsProducer: browsable.listEntries(root))
    }

    func browseFiles() -> (Coordinator & FileBrowserOutput, UIViewController) {
        let context = FileBrowserContext(factory: self)
        let router = WeakRouterContainer()
        let coordinator = FileBrowserCoordinator(router: router, context: context)
        let controller = listFiles(type(of: browsable).rootFolder, output: coordinator)
        router.router = controller

        return (coordinator, controller)
    }
}
