//
//  FileBrowserCoordinator.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol FileBrowserOutput {
    var selectedFileSignal: Signal<FileEntry, Never> { get }

    var cancelSignal: Signal<Void, Never> { get }
}

class FileBrowserCoordinator: Coordinator, FileBrowserOutput, ListOutput {
    private let router: RouterProtocol
    private let context: FileBrowserContextProtocol

    let selectedFileSignal: Signal<FileEntry, Never>
    private let selectedFileObserver: Signal<FileEntry, Never>.Observer

    let cancelSignal: Signal<Void, Never>
    private let cancelObserver: Signal<Void, Never>.Observer

    init(router: RouterProtocol, context: FileBrowserContextProtocol) {
        self.router = router
        self.context = context

        (selectedFileSignal, selectedFileObserver) = Signal.pipe()
        (cancelSignal, cancelObserver) = Signal.pipe()
    }

    func start() {
        // nop?
    }

    private func listFiles(_ fileEntry: FileEntry) {
        let controller = context.listFiles(fileEntry, output: self)

        router.push(controller)
    }

    func selectedItem(_ folderEntry: FolderEntry) {
        switch folderEntry {
        case .file(let entry):
            selectedFileObserver.send(value: entry)

        case .folder(let entry):
            listFiles(entry)
        }
    }

    func listClosed() {
        cancelObserver.send(value: ())
    }
}
