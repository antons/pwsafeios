//
//  FileBrowserService.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

enum FileBrowserError: Error {
    case unknownError
    case internalError(error: Error)
}

enum FolderEntry {
    case folder(FileEntry)
    case file(FileEntry)
}

protocol FileBrowsable {
    static var rootFolder: FileEntry { get }

    func listEntries(_ fileEntry: FileEntry) -> SignalProducer<[FolderEntry], FileBrowserError>
}

extension FolderEntry {
    var isFolder: Bool {
        if case .folder = self {
            return true
        } else {
            return false
        }
    }

    var name: String {
        switch self {
        case .file(let entry):
            return entry.name

        case .folder(let entry):
            return entry.name
        }
    }

    var path: String {
        switch self {
        case .file(let entry):
            return entry.path

        case .folder(let entry):
            return entry.path
        }
    }
}

extension FolderEntry: CustomStringConvertible {
    var description: String {
        return (isFolder ? "/" : "") + name
    }
}
