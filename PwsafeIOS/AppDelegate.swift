//
//  AppDelegate.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appRouter: AppRouter!
    var appAssembly: AppAssembly!
    var dropbox: DropboxIntegrationProtocol!

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        guard NSClassFromString("XCTestCase") == nil else { return true }

        #if targetEnvironment(simulator)
            if let documents = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
                print("Documents Folder: \(documents)")
            }
        #endif

        window = UIWindow(frame: UIScreen.main.bounds)

        Appearance.setup()

        appRouter = AppRouter(window: window!)
        appAssembly = AppAssembly(appRouter: appRouter)

        dropbox = appAssembly.dropboxSystem
        dropbox.setup()

        appAssembly.localSyncProcessor.start()
        appAssembly.remoteSyncProcessor.start()

        appRouter.setup()
        window?.makeKeyAndVisible()
        return true
    }

    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any]) -> Bool {
        dropbox.handleUrl(url)
        return false
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // TODO: Obscure screen before entering background

//        let blankViewController = UIViewController()
//        blankViewController.view.backgroundColor = .black
//
//        window?.rootViewController?.present(blankViewController, animated: false, completion: nil)

        // TODO: addSubview, bringToFront?
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
//        window?.rootViewController?.dismiss(animated: false, completion: nil)
    }
}
