//
//  AppAssembly.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/04/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Swinject
import PwsafeSwift
import UIKit

class AppAssembly {
//    let resolver: ResolverType
    let resolver: Container

    init(appRouter: AppRouter) {
        let container = Container()
        resolver = container

        let assembler = Assembler(container: container)

        assembler.apply(assemblies: [
            StorageAssembly(),
            DropboxAssembly(),
            SyncAssembly(),
            DemoInitializer()
            ])

        appRouter.appAssembly = self
    }
}

extension AppAssembly {
    var dropboxSystem: DropboxIntegrationProtocol {
        return resolver.resolve(DropboxIntegrationProtocol.self)!
    }

    var localSyncProcessor: LocalSyncProcessor {
        return resolver.resolve(LocalSyncProcessor.self)!
    }

    var remoteSyncProcessor: RemoteSyncProcessor {
        return resolver.resolve(RemoteSyncProcessor.self)!
    }
}
