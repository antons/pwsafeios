//
//  AppRouter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/10/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import PureLayout
import ReactiveCocoa


class AppRouter {
    private let window: UIWindow
    private let rootController: UISplitViewController = UISplitViewController()
    private let mainController: UINavigationController = UINavigationController()

    //todo: temp hack! :D
    var appAssembly: AppAssembly!

    init(window: UIWindow) {
        self.window = window
    }

    func setup() {
        window.rootViewController = rootController
        rootController.viewControllers.append(mainController)

        let safeListFactory = SafeListFactory(parentContainer: appAssembly.resolver)
        let controller = safeListFactory.safeList()

        mainController.push(controller)
    }
}
