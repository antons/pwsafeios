//
//  SignalExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 30/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


typealias VoidSignal = Signal<Void, Never>
typealias VoidObserver = Signal<Void, Never>.Observer
