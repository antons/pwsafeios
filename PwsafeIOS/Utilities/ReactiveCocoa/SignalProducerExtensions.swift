//
//  SignalProducerExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/27/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


extension SignalProducer where Value == String? {
    func notEmpty() -> SignalProducer<Bool, SignalProducer.Error> {
        return map({ !($0?.isEmpty ?? true) })
    }
}

extension SignalProducer where Value: Sequence {
    typealias Element = Value.Iterator.Element

    func flattenArray(_ strategy: FlattenStrategy) -> SignalProducer<Element, Error> {
        return flatMap(strategy) { values in
            return SignalProducer<Element, Error>(values)
        }
    }
}

extension SignalProducer {
    func ignoreError() -> SignalProducer<Value, Never> {
        return flatMapError({ _ in .empty })
    }
}

extension SignalProducer {
    static func value(_ value: Value) -> SignalProducer<Value, Error> {
        return SignalProducer(value: value)
    }
    
    func ignoreValues() -> SignalProducer<Void, Error> {
        return flatMap(.latest) { (_) -> SignalProducer<Void, Error> in
            return .empty
        }
    }
}
