//
//  File.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 25/05/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import ReactiveCocoa


extension UIBarButtonItem {
    convenience init<Output, Error>(barButtonSystemItem systemItem: UIBarButtonItem.SystemItem, action: Action<Void, Output, Error>) {
        self.init(barButtonSystemItem: systemItem)
        reactive.pressed = CocoaAction(action)
    }

    convenience init<Output, Error>(title: String, action: Action<Void, Output, Error>) {
        self.init(title: title, style: .plain, target: nil, action: nil)
        reactive.pressed = CocoaAction(action)
    }

    convenience init(barButtonSystemItem systemItem: UIBarButtonItem.SystemItem) {
        self.init(barButtonSystemItem: systemItem, target: nil, action: nil)
    }
}
