//
//  ActionExtension.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/19/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


typealias VoidAction = Action<Void, Void, Never>

extension Action {
    class var empty: Action<Input, Output, Error> {
        return Action { _ in SignalProducer.empty }
    }
}
