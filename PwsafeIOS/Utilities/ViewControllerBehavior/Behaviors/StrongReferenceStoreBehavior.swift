//
//  StrongReferenceStoreBehavior.swift
//
//  Created by Anton Selyanin on 28/04/2018.
//

import Foundation
import UIKit

public final class StrongReferenceStoreBehavior: ViewControllerLifecycleBehavior {
    private let references: [Any]

    public init(references: [Any]) {
        self.references = references
    }
}
