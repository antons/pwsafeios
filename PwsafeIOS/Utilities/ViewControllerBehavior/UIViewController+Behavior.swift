//
//  UIViewController+Behavior.swift
//
//  Created by Anton Selyanin on 20/11/2017.
//

import UIKit

extension UIViewController {
    public func addBehaviorController(_ controller: UIViewController) {
        addChild(controller)
        view.addSubview(controller.view)
        controller.didMove(toParent: self)
    }

    public func addBehaviors(_ behaviors: [ViewControllerLifecycleBehavior]) {
        let behaviorController = LifecycleBehaviorViewController(behaviors: behaviors)
        addBehaviorController(behaviorController)
    }
}
