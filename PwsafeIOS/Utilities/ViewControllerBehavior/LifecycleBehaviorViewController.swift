//
//  LifecycleBehaviorViewController.swift
//
//  Created by Anton Selyanin on 20/11/2017.
//

import UIKit

class LifecycleBehaviorViewController: UIViewController {
    private let behaviors: [ViewControllerLifecycleBehavior]

    init(behaviors: [ViewControllerLifecycleBehavior]) {
        self.behaviors = behaviors
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        view.isHidden = true

        applyBehaviors { behavior, controller in
            behavior.afterLoading(controller)
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        applyBehaviors { behavior, controller in
            behavior.beforeAppearing(controller)
        }
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        applyBehaviors { behavior, controller in
            behavior.afterAppearing(controller)
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        applyBehaviors { behavior, controller in
            behavior.beforeDisappearing(controller)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)

        applyBehaviors { behavior, controller in
            behavior.afterDisappearing(controller)
        }
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()

        applyBehaviors { behavior, controller in
            behavior.beforeLayingOutSubviews(controller)
        }
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        applyBehaviors { behavior, controller in
            behavior.afterLayingOutSubviews(controller)
        }
    }

    private func applyBehaviors(body: (ViewControllerLifecycleBehavior, UIViewController) -> Void) {
        guard let parent = parent else { return }

        for behavior in behaviors {
            body(behavior, parent)
        }
    }
}
