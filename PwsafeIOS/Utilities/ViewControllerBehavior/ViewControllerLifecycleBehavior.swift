//
//  ViewControllerLifecycleBehavior.swift
//
//  Created by Anton Selyanin on 20/11/2017.
//

import UIKit

// Based on this idea: http://irace.me/lifecycle-behaviors
public protocol ViewControllerLifecycleBehavior {
    func afterLoading(_ viewController: UIViewController)

    func beforeAppearing(_ viewController: UIViewController)
    func afterAppearing(_ viewController: UIViewController)

    func beforeDisappearing(_ viewController: UIViewController)
    func afterDisappearing(_ viewController: UIViewController)

    func beforeLayingOutSubviews(_ viewController: UIViewController)
    func afterLayingOutSubviews(_ viewController: UIViewController)
}

extension ViewControllerLifecycleBehavior {
    public func afterLoading(_ viewController: UIViewController) {}

    public func beforeAppearing(_ viewController: UIViewController) {}
    public func afterAppearing(_ viewController: UIViewController) {}

    public func beforeDisappearing(_ viewController: UIViewController) {}
    public func afterDisappearing(_ viewController: UIViewController) {}

    public func beforeLayingOutSubviews(_ viewController: UIViewController) {}
    public func afterLayingOutSubviews(_ viewController: UIViewController) {}
}
