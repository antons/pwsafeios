//
//  ProgressHUD.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/10/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import PureLayout

class ProgressHUD {
    let visible: MutableProperty<Bool> = MutableProperty(false)

    private weak var view: UIView?

    private weak var progressView: UIView?
    private weak var indicator: UIActivityIndicatorView?

    init(view: UIView) {
        self.view = view

        setup()

        self.visible.producer
            .on(value: weakify(self, ProgressHUD.updateState))
            .start()
    }

    private func setup() {
        let progressView = UIView()
        let indicator = UIActivityIndicatorView(style: .gray)

        progressView.backgroundColor = UIColor(white: 1, alpha: 0.8)

        view?.addSubview(progressView)
        progressView.autoPinEdgesToSuperviewEdges()

        progressView.addSubview(indicator)
        indicator.autoCenterInSuperview()

        self.progressView = progressView
        self.indicator = indicator
    }

    private func updateState(_ enabled: Bool) {
        if enabled {
            show()
        } else {
            hide()
        }
    }

    private func show() {
        progressView?.isHidden = false
        indicator?.startAnimating()
    }

    private func hide() {
        progressView?.isHidden = true
        indicator?.stopAnimating()
    }
}
