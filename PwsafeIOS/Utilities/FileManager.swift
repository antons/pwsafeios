//
//  FileManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/3/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

enum FileManagerError: Error {
    case fileNotFound(path: String)
    case fileAlreadyExists(path: String)
    case unknownError(reason: Error)
    case couldNotCreateFile
}

protocol FileManagerProtocol: class {
    func fetchData(_ path: String) -> SignalProducer<Data, FileManagerError>

    func create(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError>

    func update(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError>

    func remove(_ path: String) -> SignalProducer<Void, FileManagerError>

    func listFiles(_ path: String) -> SignalProducer<[String], FileManagerError>
}

extension FileManager: FileManagerProtocol {
    func fetchData(_ path: String) -> SignalProducer<Data, FileManagerError> {
        return SignalProducer { observer, _ in
            guard let result = self.contents(atPath: path) else {
                observer.send(error: .fileNotFound(path: path))
                return
            }

            observer.send(value: result)
            observer.sendCompleted()
        }
    }

    func create(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError> {
        return write(path, data: data, overwrite: false)
    }

    func update(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError> {
        return write(path, data: data, overwrite: true)
    }

    func write(_ path: String, data: Data, overwrite: Bool = true) -> SignalProducer<Void, FileManagerError> {
        return SignalProducer { observer, _ in
            guard overwrite || !self.fileExists(atPath: path) else {
                observer.send(error: .fileAlreadyExists(path: path))
                return
            }

            let result = self.createFile(atPath: path, contents: data, attributes: nil)

            guard result else {
                observer.send(error: .couldNotCreateFile)
                return
            }

            observer.sendCompleted()
        }
    }

    func remove(_ path: String) -> SignalProducer<Void, FileManagerError> {
        return SignalProducer { observer, _ in
            do {
                if self.fileExists(atPath: path) {
                    try self.removeItem(atPath: path)
                }
                observer.sendCompleted()
            } catch let error {
                observer.send(error: .unknownError(reason: error))
            }
        }
    }

    func listFiles(_ path: String) -> SignalProducer<[String], FileManagerError> {
        return SignalProducer { observer, _ in
            do {
                let files = try self.contentsOfDirectory(atPath: path)
                observer.send(value: files.map({ "\(path)/\($0)" }))
                observer.sendCompleted()
            } catch let error {
                observer.send(error: .unknownError(reason: error))
            }
        }
    }
}
