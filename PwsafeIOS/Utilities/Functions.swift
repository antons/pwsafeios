//
//  Functions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

//swiftlint:disable variable_name

//infix operator >>> { associativity left }
//func >>> <A, B, C>(f: @escaping (B) -> C, g: @escaping (A) -> B) -> (A) -> C {
//    return compose(f, g)
//}

func compose <A, B, C>(_ f: @escaping (B) -> C, _ g: @escaping (A) -> B) -> (A) -> C {
    return { x in f(g(x)) }
}

func void<T>(_ t: T) {}

func identity<T>(_ t: T) -> T {
    return t
}

@discardableResult
func with<T>(_ object: T, _ initializer: (inout T) -> Void) -> T {
    var modified = object
    initializer(&modified)
    return modified
}

func weakify<T: AnyObject>(_ object: T, _ f: @escaping (T) -> (() -> Void)) -> () -> Void {
    return { [weak object] () -> Void in
        guard let object = object else { return }
        return f(object)()
    }
}

func weakify<T: AnyObject, U>(_ object: T, _ f: @escaping (T) -> ((U) -> Void)) -> (U) -> Void {
    return { [weak object] (input: U) -> Void in
        guard let object = object else { return }
        return f(object)(input)
    }
}

func weakify<T: AnyObject, A, B>(_ object: T, _ f: @escaping (T) -> ((A, B) -> Void)) -> (A, B) -> Void {
    return { [weak object] (a: A, b: B) -> Void in
        guard let object = object else { return }
        return f(object)(a, b)
    }
}

func compare<T>(lhs: T?, rhs: T?, compare: (_ lhs: T, _ rhs: T) -> Bool) -> Bool {
    switch (lhs, rhs) {
    case let (lValue?, rValue?):
        return compare(lValue, rValue)
    case (nil, nil):
        return true
    default:
        return false
    }
}

func mutate<T>(_ object: T, _ transform: (inout T) -> Void) -> T {
    var mutated = object
    transform(&mutated)
    return mutated
}
