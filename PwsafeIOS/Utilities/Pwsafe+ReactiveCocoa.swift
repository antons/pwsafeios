//
//  PwsafeReactiveExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 08/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift
import ReactiveSwift

extension Pwsafe {
    func toDataProducer(with password: String) -> SignalProducer<Data, AppError> {
        return SignalProducer { observer, _ in
            do {
                let data = try self.toData(with: password)
                observer.send(value: data)
                observer.sendCompleted()
            } catch {
                observer.send(error: .internalError(error))
            }
        }
    }

    static func fromData(_ data: Data, password: String) -> SignalProducer<Pwsafe, AppError> {
        return SignalProducer { observer, _ in
            do {
                let pwsafe = try Pwsafe(data: data, password: password)
                observer.send(value: pwsafe)
                observer.sendCompleted()
            } catch {
                //todo: PwsafeError.CorruptedData - this should be invalid password error
                observer.send(error: .invalidPwsafePassword)
            }
        }
    }
}
