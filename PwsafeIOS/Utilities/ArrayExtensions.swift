//
//  ArrayExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 09/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension Array where Element: AnyObject {
    mutating func removeAllIdenticalTo(_ element: Element) {
        self = filter({ $0 !== element })
    }
}

extension Array {
    static func one(_ element: Element) -> [Element] {
        return [element]
    }

    mutating func removeAll(where condition: (Element) -> Bool) {
        self = filter(condition)
    }
}
