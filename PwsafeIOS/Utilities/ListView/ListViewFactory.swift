//
//  ListViewFactory.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 31/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift


class ListViewFactory<Item, Cell: ConfigurableCell> where Cell.Item == Item, Cell: UITableViewCell {
    typealias ItemListViewModel = ListViewModel<Item>
    typealias ItemListViewController = ListViewController<Cell, Item>

    typealias SectionData = (section: String, items: [Item])

    func list<E, Output: ListOutput>(title: String,
              output: Output,
              sectionsProducer: SignalProducer<[SectionData], E>,
              closable: Bool = true, showProgress: Bool = true) -> UIViewController where Output.Item == Item {
        let viewModel = ItemListViewModel(
            title: title, output: output,
            sectionsProducer: sectionsProducer,
            closable: closable, showProgress: showProgress)
        let controller = ItemListViewController(viewModel: viewModel)

        return controller
    }

    func list<E, Output: ListOutput>(title: String,
              output: Output,
              itemsProducer: SignalProducer<[Item], E>,
              closable: Bool = true, showProgress: Bool = true) -> UIViewController where Output.Item == Item {
        return list(title: title,
                    output: output,
                    sectionsProducer: itemsProducer.map(createSection),
                    closable: closable, showProgress: showProgress)
    }

    func list<Output: ListOutput>(title: String,
              output: Output,
              items: [Item],
              closable: Bool = true, showProgress: Bool = true) -> UIViewController where Output.Item == Item {
        return list(title: title,
                    output: output,
                    itemsProducer: SignalProducer<[Item], Never>(value: items),
                    closable: closable, showProgress: showProgress)
    }
}
