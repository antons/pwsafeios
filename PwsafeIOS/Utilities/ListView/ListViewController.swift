//
//  ListViewController.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import ReactiveCocoa

typealias ListViewController<Cell: ConfigurableCell, Item> =
    ConfigurableListViewController<Item, SimpleTableCellConfigurator<Cell>>
    where Cell.Item == Item, Cell: UITableViewCell

class ConfigurableListViewController<Item, Configurator: TableCellConfigurator>: UIViewController
        where Configurator.Item == Item, Configurator: Creatable {
    typealias ViewModel = ListViewModel<Item>
    
    let viewModel: ViewModel
    
    let tableView: UITableView
    private let tableManager: SectionTableDataManager<Item, Configurator>
    private lazy var progressHUD: ProgressHUD = ProgressHUD(view: self.view)
    
    init(viewModel: ViewModel, tableView: UITableView = UITableView()) {
        self.viewModel = viewModel
        self.tableManager = SectionTableDataManager(tableView: tableView)
        self.tableView = tableView
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.title
        
        view.addSubview(tableView)
        tableView.autoPinEdgesToSuperviewEdges()
        
        progressHUD.visible <~ viewModel.showProgress
        tableManager.sections <~ viewModel.sectionsProducer
        viewModel.selectedItemTarget <~ tableManager.selectedItem
        
        if viewModel.closable {
            let cancelButton = UIBarButtonItem(barButtonSystemItem: .cancel, action: viewModel.cancelAction)
            navigationItem.rightBarButtonItem = cancelButton
        }
    }
}
