//
//  ListOutput.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 31/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


protocol ListOutput {
    associatedtype Item

    func selectedItem(_ item: Item)

    func listClosed()
}

extension ListOutput {
    func listClosed() {
        //nop
    }
}
