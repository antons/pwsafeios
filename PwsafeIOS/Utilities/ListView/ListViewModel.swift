//
//  ListViewModel.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift


func createSection<Item>(_ items: [Item]) -> [(section: String, items: [Item])] {
    return [(section: "", items: items)]
}

class ListViewModel<Item> {
    typealias SectionData = (section: String, items: [Item])

    let title: String
    let sectionsProducer: SignalProducer<[SectionData], Never>
    let showErrorSignal: Signal<String, Never>
    let selectedItemTarget: BindingTarget<Item>
    var cancelAction: VoidAction = .empty
    var closeSignal: Signal<Void, Never> {
        return cancelAction.completed
    }

    let closable: Bool
    let showProgress: Property<Bool>

    private let lifetimeToken: Lifetime.Token

    convenience init<E, Output: ListOutput>(title: String,
                     output: Output,
                     itemsProducer: SignalProducer<[Item], E>,
                     closable: Bool = true,
                     showProgress: Bool = true) where Output.Item == Item {
        self.init(title: title, output: output, sectionsProducer: itemsProducer.map(createSection),
                  closable: closable, showProgress: showProgress)
    }

    convenience init<E, Output: ListOutput>(title: String,
                     output: Output,
                     sectionsProducer: SignalProducer<[SectionData], E>,
                     closable: Bool = true,
                     showProgress: Bool = true) where Output.Item == Item {
        self.init(title: title, output: output, action: Action(execute: { sectionsProducer }),
                  closable: closable, showProgress: showProgress)
    }

    init<E, Output: ListOutput>(title: String,
         output: Output,
         action: Action<Void, [SectionData], E>,
         closable: Bool = true,
         showProgress: Bool = true) where Output.Item == Item {
        self.title = title

        self.showErrorSignal = action.errors.map({ _ in "Error!" })

        if showProgress {
            self.showProgress = action.isExecuting
        } else {
            self.showProgress = Property(value: false)
        }

        self.sectionsProducer = action.apply().ignoreError()
        self.closable = closable

        let token = Lifetime.Token()

        self.selectedItemTarget = BindingTarget(
            lifetime: Lifetime(token),
            action: output.selectedItem)

        self.lifetimeToken = token

        closeSignal.observeValues(output.listClosed)
    }
}
