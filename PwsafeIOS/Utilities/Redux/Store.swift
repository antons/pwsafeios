//
//  Store.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

typealias Reducer<State> = (inout State, Action2) -> Void

final class Store<State>: Dispatcher {
    private struct Subscription {
        let id: UUID
        let command: CommandWith<State>

        init(id: UUID, command: CommandWith<State>) {
            self.id = id
            self.command = command
        }
    }

    private var state: State
    private let reducer: Reducer<State>
    private var subscribers: [Subscription] = []
    private let queue: DispatchQueue

    init(state: State, reducer: @escaping Reducer<State>, queue: DispatchQueue) {
        self.state = state
        self.reducer = reducer
        self.queue = queue
    }

    func dispatch(_ action: Action2) {
        queue.async {
            self.doDispatch(action)
        }
    }

    private func doDispatch(_ action: Action2) {
        #if targetEnvironment(simulator)
        print(action)
        #endif

        reducer(&state, action)
        subscribers.forEach { subscriber in
            //TODO: hack, 
            let state = self.state
            DispatchQueue.main.async {
                subscriber.command.perform(with: state)
            }
        }
    }

    @discardableResult
    func observe(with command: CommandWith<State>) -> Command {
        let id = UUID()

        queue.sync {
            let subscription = Subscription(id: id, command: command)
            subscribers.append(subscription)
            command.perform(with: state)
        }

        return Command(action: unsubscribe(id))
    }

    private func unsubscribe(_ subscriptionId: UUID) -> () -> Void {
        return {
            self.queue.async {
                guard let index = self.subscribers.firstIndex(where: { $0.id == subscriptionId }) else { return }
                self.subscribers.remove(at: index)
            }
        }
    }
}
