//
//  CommandWith.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 24/04/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

typealias Command = CommandWith<Void>

final class CommandWith<T> {
    static var nop: CommandWith<T> {
        return CommandWith { _ in }
    }

    private let action: (T) -> Void

    init(action: @escaping (T) -> Void) {
        self.action = action
    }

    func perform(with value: T) {
        action(value)
    }

    func bind(to value: T) -> Command {
        return Command { self.perform(with: value) }
    }

    func map<U>(_ transform: @escaping (U) -> T) -> CommandWith<U> {
        return CommandWith<U> { value in self.perform(with: transform(value)) }
    }
}

extension CommandWith where T == Void {
    func perform() {
        perform(with: ())
    }
}

extension CommandWith: Equatable {
    static func == <T>(lhs: CommandWith<T>, rhs: CommandWith<T>) -> Bool {
        return true
    }
}
