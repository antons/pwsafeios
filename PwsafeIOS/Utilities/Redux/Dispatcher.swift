//
//  Dispatcher.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

protocol Dispatcher {
    func dispatch(_ action: Action2)
}
