//
//  File.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {
    convenience init(rgb: UInt32) {
        let red = CGFloat(0xff & rgb >> 16) / 255
        let green = CGFloat(0xff & rgb >> 8) / 255
        let blue = CGFloat(0xff & rgb) / 255
        self.init(red: red, green: green, blue: blue, alpha: 1.0)
    }
}
