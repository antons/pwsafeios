//
//  Creatable.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

//TODO: this is a hack, get rid of it
protocol Creatable: class {
    init()
}
