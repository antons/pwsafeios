//
//  KeyboardService.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/08/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

struct KeyboardInfo: Equatable {
    let frameBegin: CGRect
    let frameEnd: CGRect
    let animationCurve: UIView.AnimationCurve
    let animationDuration: TimeInterval
    let isLocal: Bool
}

protocol KeyboardServiceProtocol: class {
    var keyboardWillShow: Signal<KeyboardInfo, Never> { get }
    
    var keyboardWillHide: Signal<KeyboardInfo, Never> { get }
}

class KeyboardService: KeyboardServiceProtocol {
    private let notificationCenter: NotificationCenter
    
    init(notificationCenter: NotificationCenter) {
        self.notificationCenter = notificationCenter
    }
    
    var keyboardWillShow: Signal<KeyboardInfo, Never> {
        return keyboardInfo(for: UIResponder.keyboardWillShowNotification)
    }
    
    var keyboardWillHide: Signal<KeyboardInfo, Never> {
        return keyboardInfo(for: UIResponder.keyboardWillHideNotification)
    }
    
    private func keyboardInfo(for notification: Notification.Name) -> Signal<KeyboardInfo, Never> {
        return notificationCenter.reactive
            .notifications(forName: notification)
            .filterMap { notification -> KeyboardInfo? in
                return notification.userInfo.flatMap(KeyboardInfo.init(userInfo:))
        }
    }
}

extension KeyboardInfo {
    init?(userInfo: [AnyHashable: Any]) {
        guard let frameBegin = userInfo[UIResponder.keyboardFrameBeginUserInfoKey].flatMap({ $0 as? NSValue })?.cgRectValue,
            let frameEnd = userInfo[UIResponder.keyboardFrameEndUserInfoKey].flatMap({ $0 as? NSValue })?.cgRectValue,
            let animationCurve = userInfo[UIResponder.keyboardAnimationCurveUserInfoKey]
                .flatMap({ $0 as? Int }).flatMap(UIView.AnimationCurve.init(rawValue:)),
            let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval,
            let isLocal = userInfo[UIResponder.keyboardIsLocalUserInfoKey] as? Bool else { return nil }

        self.init(
            frameBegin: frameBegin,
            frameEnd: frameEnd,
            animationCurve: animationCurve,
            animationDuration: animationDuration,
            isLocal: isLocal)
    }
}
