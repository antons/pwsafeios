//
//  DictionaryExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension Dictionary where Value: Collection {
    typealias CollectionElement = Value.Iterator.Element

    func valuesMap<T>(_ mapper: (CollectionElement) -> T) -> [Key: [T]] {
        return reduce([:]) { current, element in
            var result = current
            let (key, collection) = element
            result[key] = collection.map(mapper)
            return result
        }
    }
}
