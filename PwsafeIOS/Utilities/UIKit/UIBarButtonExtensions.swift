//
//  UIBarButtonExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

extension UIBarButtonItem {
    convenience init(title: String?) {
        self.init(title: title, style: .plain, target: nil, action: nil)
    }
}
