//
//  UIAlertExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 10/01/2019.
//  Copyright © 2019 Anton Selyanin. All rights reserved.
//

import UIKit

extension UIAlertController {
    func addAction(title: String?, style: UIAlertAction.Style = .default, command: Command = .nop) {
        addAction(UIAlertAction(title: title, style: style, command: command))
    }

    func addAction(title: String?, style: UIAlertAction.Style = .default, handler: ((UIAlertAction) -> Void)?) {
        addAction(UIAlertAction(title: title, style: style, handler: handler))
    }

    func addCancelAction() {
        addAction(title: "Cancel", style: .cancel, handler: nil)
    }
}

extension UIAlertAction {
    convenience init(title: String?, style: UIAlertAction.Style = .default, command: Command) {
        self.init(title: title, style: style, handler: { _ in command.perform() })
    }
}
