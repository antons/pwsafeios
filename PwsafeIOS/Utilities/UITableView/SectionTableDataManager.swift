//
//  SectionTableDataManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift
import ReactiveCocoa


class SectionTableDataManager<Item, Configurator: TableCellConfigurator>: NSObject, UITableViewDataSource, UITableViewDelegate where Configurator.Item == Item {
    typealias SectionData = (section: String, items: [Item])

    let sections: MutableProperty<[SectionData]>
    let selectedIndex: Signal<IndexPath, Never>

    var selectedItem: Signal<Item, Never> {
        return selectedIndex.map { [unowned self] in
            self.sections.value[$0.section].items[$0.item]
        }
    }

    private let cellConfigurator: Configurator
    private let tableView: UITableView
    private let selectedIndexObserver: Signal<IndexPath, Never>.Observer

    init(sections: [SectionData] = [], tableView: UITableView, cellConfigurator: Configurator) {
        self.tableView = tableView
        self.cellConfigurator = cellConfigurator
        self.sections = MutableProperty(sections)
        (selectedIndex, selectedIndexObserver) = Signal.pipe()

        super.init()

        tableView.dataSource = self
        tableView.delegate = self

        cellConfigurator.registerCells(tableView)
        tableView.reactive.reloadData <~ self.sections.signal.map(void)
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections.value[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = sections.value[indexPath.section].items[indexPath.item]
        return cellConfigurator.dequeueCell(tableView: tableView, indexPath: indexPath, item: item)
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        selectedIndexObserver.send(value: indexPath)
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.value.count
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sections.value[section].section
    }
}

extension SectionTableDataManager where Configurator: Creatable {
    convenience init(sections: [SectionData] = [], tableView: UITableView) {
        self.init(sections: sections, tableView: tableView, cellConfigurator: Configurator())
    }
}
