//
//  TableCellConfigurator.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

protocol TableCellConfigurator {
    associatedtype Item

    func registerCells(_ tableView: UITableView)

    func dequeueCell(tableView: UITableView, indexPath: IndexPath, item: Item) -> UITableViewCell
}

protocol ConfigurableTableCellConfigurator: TableCellConfigurator {
    associatedtype Cell: ConfigurableCell
}

extension ConfigurableTableCellConfigurator where Cell: UITableViewCell, Cell.Item == Item {
    func registerCells(_ tableView: UITableView) {
        tableView.register(cellClass: Cell.self)
    }
    
    func dequeueCell(tableView: UITableView, indexPath: IndexPath, item: Item) -> UITableViewCell {
        let cell: Cell = tableView.dequeueCell(for: indexPath)
        cell.configure(item)
        return cell
    }
}

final class SimpleTableCellConfigurator<C: ConfigurableCell>: Creatable, ConfigurableTableCellConfigurator where C: UITableViewCell {
    typealias Item = C.Item
    typealias Cell = C
}
