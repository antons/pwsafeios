//
//  CombinedCache.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 03/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import ReactiveSwift


//TODO: THIS IS EXPERIMENTAL
private class SourceInfo<Item> {
    let producer: SignalProducer<[Item], Never>

    var startIndex: Int = 0
    var endIndex: Int = 0

    var length: Int {
        return endIndex - startIndex
    }

    init(producer: SignalProducer<[Item], Never>) {
        self.producer = producer
    }
}

class CombinedCache<Item> {
    private(set) var items: [Item] = []

    private let sources: [SourceInfo<Item>]

    init(sources: [SignalProducer<[Item], Never>]) {
        self.sources = sources.map(SourceInfo.init)

        for (index, sourceInfo) in self.sources.enumerated() {
            sourceInfo.producer
                .startWithValues { [weak self] newItems in
                    self?.update(items: newItems, forIndex: index)
            }
        }
    }

    private func update(items newItems: [Item], forIndex index: Int) {
        let startIndex: Int = {
            guard index >= 1 else { return 0 }
            return sources[index - 1].endIndex
        }()

        let source = sources[index]
        source.startIndex = startIndex
        source.endIndex = startIndex + newItems.count

        items.insert(contentsOf: newItems, at: source.startIndex)
    }

}
