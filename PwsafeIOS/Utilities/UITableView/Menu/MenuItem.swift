//
//  MenuItem.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

struct MenuSection: Equatable {
    let items: [MenuItem]
}

struct MenuItem: Equatable {
    let title: String?
    let onSelected: Command
}
