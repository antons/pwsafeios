//
//  MenuItemCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class MenuItemCell: UITableViewCell, ConfigurableCell {
    private var props: MenuItem?

    func configure(_ item: MenuItem) {
        self.props = item
        self.textLabel?.text = item.title
    }

    func didSelect() {
        props?.onSelected.perform()
    }
}
