//
//  MenuTableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 12/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import UIKit

final class MenuTableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    var sections: [MenuSection] = [] {
        didSet {
            updateItems(old: oldValue, new: sections)
        }
    }

    private let tableView: UITableView

    init(tableView: UITableView) {
        self.tableView = tableView
        super.init()
        self.setup()
    }

    private func setup() {
        tableView.register(cellClass: MenuItemCell.self)
        tableView.dataSource = self
        tableView.delegate = self
    }

    private func item(for indexPath: IndexPath) -> MenuItem {
        return sections[indexPath.section].items[indexPath.item]
    }

    private func updateItems(old: [MenuSection], new: [MenuSection]) {
        tableView.reloadData()
    }

    // MARK: UITableViewDataSource
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].items.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellItem = item(for: indexPath)
        let cell = tableView.dequeueCell(for: indexPath) as MenuItemCell
        cell.configure(cellItem)
        return cell
    }

    // MARK: UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let cell = tableView.cellForRow(at: indexPath) as? SelectableCell {
            cell.didSelect()
        }

        tableView.deselectRow(at: indexPath, animated: true)
    }
}
