//
//  TableSection.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 21/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

class TableSection {
    let title: String

    let rows: [Row]

    init(title: String = "", rows: [Row]) {
        self.title = title
        self.rows = rows
    }
}
