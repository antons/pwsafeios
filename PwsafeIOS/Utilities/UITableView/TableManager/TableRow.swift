//
//  TableRow.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit
import ReactiveSwift


protocol Row {
    func reuseIdentifier() -> String

    func configure(cell: UITableViewCell)

    func didSelect(cell: UITableViewCell)
}

class TableRow<Item, Cell: UITableViewCell>: Row where Cell: ConfigurableCell, Cell.Item == Item {
    private let item: Item

    let selectedSignal: Signal<Void, Never>
    private let selectedObserver: Signal<Void, Never>.Observer

    init(item: Item) {
        self.item = item

        (selectedSignal, selectedObserver) = Signal.pipe()
    }

    func reuseIdentifier() -> String {
        return Cell.reuseIdentifier
    }

    func configure(cell: UITableViewCell) {
        guard let configurableCell = cell as? Cell else { return }

        configurableCell.configure(item)
    }

    func didSelect(cell: UITableViewCell) {
        guard let configurableCell = cell as? Cell else { return }

        configurableCell.didSelect()
        selectedObserver.send(value: ())
    }
}
