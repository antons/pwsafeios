//
//  TableManager.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

class TableManager: NSObject, UITableViewDataSource, UITableViewDelegate {
    private let tableView: UITableView

    var rows: [Row] {
        get {
            return sections[0].rows
        }
        set {
            sections = [TableSection(rows: newValue)]
        }
    }

    var sections: [TableSection] = [] {
        didSet {
            tableView.reloadData()
        }
    }

    init(tableView: UITableView) {
        self.tableView = tableView

        super.init()

        tableView.dataSource = self
        tableView.delegate = self
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].rows.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRow = row(indexPath)
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRow.reuseIdentifier(), for: indexPath)
        cellRow.configure(cell: cell)
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else { return }
        let cellRow = row(indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        cellRow.didSelect(cell: cell)
    }

    private func row(_ indexPath: IndexPath) -> Row {
        return sections[indexPath.section].rows[indexPath.row]
    }
}
