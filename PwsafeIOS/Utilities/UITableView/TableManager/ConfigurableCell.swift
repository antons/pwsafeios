//
//  ConfigurableCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

protocol ReusableView: class {
    static var reuseIdentifier: String { get }
}

protocol SelectableCell {
    func didSelect()
}

protocol ConfigurableCell: ReusableView, SelectableCell {
    associatedtype Item

    func configure(_ item: Item)

    func didSelect()
}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}

extension SelectableCell {
    func didSelect() {}
}
