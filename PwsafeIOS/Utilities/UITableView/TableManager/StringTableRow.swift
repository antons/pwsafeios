//
//  StringTableRow.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 05/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

typealias StringTableRow<T: CustomStringConvertible> = TableRow<T, StringTableCell<T>>
