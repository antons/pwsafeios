//
//  StringTableCell.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/08/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

class StringTableCell<Item: CustomStringConvertible>: UITableViewCell, ConfigurableCell {
    func configure(_ item: Item) {
        self.textLabel?.text = item.description
    }
}
