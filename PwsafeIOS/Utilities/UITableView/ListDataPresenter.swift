//
//  ListDataPresenter.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 02/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import UIKit

//TODO: THIS IS EXPERIMENTAL
protocol ListDataPresenterProtocol {
    var sectionsCount: Int { get }

    func rowsCount(at: IndexPath) -> Int

    func configure(cell: UITableViewCell, at indexPath: IndexPath)

    func didSelect(cell: UITableViewCell, at indexPath: IndexPath)
}
