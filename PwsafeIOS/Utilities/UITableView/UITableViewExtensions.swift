//
//  UITableViewExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import UIKit

extension UITableView {
    func register(cellClass: ReusableView.Type) {
        register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
    }
    
    func dequeueCell<Cell: ReusableView>(for indexPath: IndexPath) -> Cell {
        guard let cell = dequeueReusableCell(withIdentifier: Cell.reuseIdentifier, for: indexPath) as? Cell else {
            fatalError("Unexpected cell type")
        }
        
        return cell
    }
}
