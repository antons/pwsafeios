//
//  OptionalExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

extension Optional where Wrapped == String {
    var isEmptyOrNil: Bool {
        return map({ $0.isEmpty }) ?? true
    }
}
