//
//  StringExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/3/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension String {
    func cut(prefix: String) -> String {
        guard hasPrefix(prefix) else { return self }
        return String(suffix(count - prefix.count))
    }
}
