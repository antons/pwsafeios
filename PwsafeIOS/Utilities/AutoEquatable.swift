//
//  AutoEquatable.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 25/02/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation

protocol AutoEquatable {}
