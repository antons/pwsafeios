//
//  Debug.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 28/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

func debugLog(_ identifier: String, event: String, fileName: String, functionName: String, lineNumber: Int) {
    // Don't forget to set up the DEBUG symbol (http://stackoverflow.com/a/24112024/491239)
    //    #if DEBUG
    print(event)
    //    #endif
}

// swiftlint:disable large_tuple
func log(_ id: String) -> EventLogger {

    ///        - identifier
    ///        - event
    ///        - fileName
    ///        - functionName
    ///        - lineNumber

    return { identifier, event, fileName, functionName, lineNumber in

//        let (identifier, event, fileName, functionName, lineNumber) = arg
        print("[\(id)] \(event)")
    }
}
