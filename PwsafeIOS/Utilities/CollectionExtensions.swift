//
//  CollectionExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension Collection {
    typealias Element = Iterator.Element

    func group<Key: Hashable>(by keyProducer: (Element) -> Key) -> [Key: [Element]] {
        return reduce([:]) { current, element in
            var result = current
            let key = keyProducer(element)
            var resultArray: [Element] = result[key] ?? []

            resultArray.append(element)
            result[key] = resultArray

            return result
        }
    }
}
