//
//  OptionalExtension.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift

extension Optional {
    func value<E: Error>(errorIfNil error: E) -> SignalProducer<Wrapped, E> {
        return SignalProducer { observer, _ in
            guard case .some(let value) = self else {
                observer.send(error: error)
                return
            }

            observer.send(value: value)
            observer.sendCompleted()
        }
    }
}
