//
//  Error.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 08/11/15.
//  Copyright © 2015 Anton Selyanin. All rights reserved.
//

import Foundation

indirect enum AppError: Error {
    case internalError(Error)
    case unknownError

    case safeAlreadyExists(name: String)

    case safeDoesNotExist(name: String)

    case invalidPwsafePassword
}

extension AppError: Equatable {}

func == (lhs: AppError, rhs: AppError) -> Bool {
    switch (lhs, rhs) {
    case (.unknownError, .unknownError):
        return true

    case (.safeAlreadyExists(let lhsName), .safeAlreadyExists(let rhsName)):
        return lhsName == rhsName

    case (.safeDoesNotExist(let lhsName), .safeDoesNotExist(let rhsName)):
        return lhsName == rhsName

    case (.invalidPwsafePassword, .invalidPwsafePassword):
        return true

    default:
        return false
    }
}
