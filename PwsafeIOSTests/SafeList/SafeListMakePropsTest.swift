//
//  SafeListMakePropsTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 17/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Quick
import Nimble

@testable import PwsafeIOS

class SafeListMakePropsTest: QuickSpec {
    override func spec() {
        describe("SafeList make props") {
            it("") {
                // Given
                let list: [FileStorageType: [SyncedFile]] = [
                    .local: [
                        SyncedFile(id: "id1", name: "file1", syncState: .synced, linkState: .linked),
                        SyncedFile(id: "id2", name: "file2", syncState: .conflict, linkState: .remotelyUnlinked)
                    ],

                    .dropbox: [
                        SyncedFile(id: "id3", name: "file3", syncState: .synced, linkState: .linked),
                        SyncedFile(id: "id4", name: "file4", syncState: .conflict, linkState: .remotelyUnlinked)
                    ]
                ]

                let state = SafeListState(files: list)

                // When
                let props = makeProps(from: state, commands: MockSafeListCommands())

                // Then
                let expected: [SafeListSection2] = [
                    SafeListSection2(name: "Local", items: [
                        SafeListItem(name: "file1", state: .synced, onSelect: .nop),
                        SafeListItem(name: "file2", state: .conflict, onSelect: .nop),
                    ]),
                    SafeListSection2(name: "Dropbox", items: [
                        SafeListItem(name: "file3", state: .synced, onSelect: .nop),
                        SafeListItem(name: "file4", state: .conflict, onSelect: .nop),
                    ]),
                ]

                expect(props.sections) == expected
            }
        }
    }
}

private class MockSafeListCommands: SafeListCommands {
    func selectFile(_ file: SyncedFile) -> Command {
        return .nop
    }

    func sync() -> Command {
        return .nop
    }

    func addSafe() -> Command {
        return .nop
    }

    func linkDropbox() -> CommandWith<UIViewController> {
        return .nop
    }
}
