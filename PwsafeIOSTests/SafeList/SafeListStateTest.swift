//
//  SafeListStateTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 18/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import PwsafeIOS

class SafeListStateTest: QuickSpec {
    override func spec() {
        describe("SafeListState") {
            it("") {
                // Given
                var state = AppState()
                state.safeList = SafeListState(files: [:])

                // When
                let list: [FileStorageType: [SyncedFile]] = [
                    .local: [
                        SyncedFile(id: "id1", name: "file1", syncState: .synced, linkState: .linked),
                        SyncedFile(id: "id2", name: "file2", syncState: .conflict, linkState: .remotelyUnlinked)
                    ],

                    .dropbox: [
                        SyncedFile(id: "id3", name: "file3", syncState: .synced, linkState: .linked),
                        SyncedFile(id: "id4", name: "file4", syncState: .conflict, linkState: .remotelyUnlinked)
                    ]
                ]

                reduce(state: &state, action: UpdateSafeFiles(files: list))

                // Then
                expect(state.safeList.files) == list
            }
        }
    }
}

