//
//  AppStateSpec.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble

import PwsafeSwift
@testable import PwsafeIOS

class AppStateSpec: QuickSpec {
    override func spec() {
        describe("AppState reducer") {
            var decrypted: DecryptedSafe!

            beforeEach {
                var safe = Pwsafe()

                let record1 = with(Record()) {
                    $0.title = "record1"
                }

                safe[record1.uuid] = record1

                let record2 = with(Record()) {
                    $0.title = "record2"
                }

                safe[record2.uuid] = record2

                let syncedFile = SyncedFile(
                    id: "id",
                    name: "safe",
                    syncState: .synced,
                    linkState: .linked)

                decrypted = DecryptedSafe(syncedFile: syncedFile, pwsafe: safe, password: "password")
            }

            it("initializes 'manage safe' state") {
                // Given
                var state = AppState()

                // When
                reduce(state: &state, action: ManageSafe(decryptedSafe: decrypted))

                // Then
                expect(state.manageSafe?.decryptedSafe) == decrypted
                expect(state.manageSafe?.screens) == [:]
            }

            it("closes 'manage safe' state") {
                // Given
                var state = AppState()
                let rootId = UUID()
                state.manageSafe = ManageSafeState(
                    decryptedSafe: decrypted,
                    screens: [rootId: ManageSafeScreen(id: rootId, filter: .all)])

                // When
                reduce(state: &state, action: SafeDidClose(screenId: rootId))

                // Then
                expect(state.manageSafe).to(beNil())
            }

            it("initializes and closes 'open safe' state") {
                // Given
                let file = SyncedFile(id: "file", name: "file", syncState: .synced, linkState: .linked)
                var state = AppState()

                // When
                reduce(state: &state, action: OpenSafe(file: file))

                // Then
                expect(state.openSafe) == OpenSafeState(file: file, enteredPassword: nil, isValid: false)

                // When
                reduce(state: &state, action: OpenSafeDispose())

                // Then
                expect(state.openSafe).to(beNil())
            }
        }
    }
}
