//
//  SyncedFileRecordTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import PwsafeIOS

func syncedFileRecordContent() -> Data {
    return Data(bytes: [0, 1, 2, 3])
}

class SyncedFileRecordTest: QuickSpec {
    override func spec() {
        describe("SyncedFileRecord") {
            var record: SyncedFileRecord!
            let remoteFileData = FileData(data: Data(bytes: [1, 2, 3]), metadata: "remotemeta")
            let syncMetadata = FileSyncMetadata(currentVersion: 1, syncedVersion: 2, syncedMetadata: "meta", remoteFileData: remoteFileData)

            beforeEach {
                record = SyncedFileRecord(id: "id", name: "name", data: Data(bytes: [5, 6, 7]),
                    storageType: .local, storageFile: FileEntry(path: "path"),
                    linkState: .locallyUnlinked, syncState: .notSynced,
                    syncMetadata: syncMetadata)
            }

            it("works with NSCoding") {
                let decoded = NSCoder.encodeAndDecode(SyncedFileRecord.Coding(syncedFileRecord: record))!.syncedFileRecord
                expect(record) == decoded

                expect(decoded.id) == "id"
                expect(decoded.name) == "name"

                expect(decoded.data) == Data(bytes: [5, 6, 7])
                expect(decoded.storageFile) == FileEntry(path: "path")
                expect(decoded.storageType) == FileStorageType.local
                expect(decoded.linkState) == FileLinkState.locallyUnlinked
                expect(decoded.syncState) == FileSyncState.notSynced

                expect(decoded.syncMetadata.currentVersion) == 1
                expect(decoded.syncMetadata.syncedVersion) == 2
                expect(decoded.syncMetadata.syncedMetadata) == "meta"

                expect(decoded.syncMetadata.remoteFileData!.data) == Data(bytes: [1, 2, 3])
                expect(decoded.syncMetadata.remoteFileData!.metadata) == "remotemeta"
            }

            it("create data and can be restored from it") {
                let data = record.serialize()
                let restored = SyncedFileRecord(data: data)!
                expect(record) == restored
            }
        }
    }
}
