//
//  SyncedFileStorageTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 19/05/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa
@testable import PwsafeIOS

//swiftlint:disable function_body_length
class SyncedFileStorageTest: QuickSpec {
    override func spec() {
        describe("SyncedFileStorage") {
            var service: SyncedFileStorage!
            var persistenceService: PersistenceServiceMock!

            beforeEach {
                persistenceService = PersistenceServiceMock()
                service = SyncedFileStorage(persistenceService: persistenceService)
            }

            describe("SyncedFileStorage") {
                it("adds new synced file") {
                    // Given
                    let data = Data.randomData()
                    let file = FileEntry(path: "path/file")
                    let syncedFile = service.addFile(file, storageType: .dropbox, data: data, metadata: "meta")
                        .fetchExpectedNextAndCompleted()!

                    // When
                    let createdFile = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!

                    // Then
                    expect(createdFile.id) == syncedFile.id
                    expect(createdFile.name) == syncedFile.name
                    expect(createdFile.data) == data

                    expect(createdFile.storageFile) == file
                    expect(createdFile.storageType) == FileStorageType.dropbox
                    expect(createdFile.linkState) == FileLinkState.linked

                    expect(createdFile.syncState) == FileSyncState.synced
                    expect(createdFile.syncMetadata.currentVersion) == 1
                    expect(createdFile.syncMetadata.syncedVersion) == 1
                    expect(createdFile.syncMetadata.syncedMetadata) == "meta"
                    expect(createdFile.syncMetadata.remoteFileData).to(beNil())
                }

                it("renews file list after creating a file") {
                    // When
                    let syncedFile1 = service.addFile(FileEntry(path: "path/file1"), storageType: .local, data: .randomData(), metadata: "")
                        .fetchExpectedNextAndCompleted()!

                    let syncedFile2 = service.addFile(FileEntry(path: "path/file2"), storageType: .local, data: .randomData(), metadata: "")
                        .fetchExpectedNextAndCompleted()!

                    // Then
                    expect(service.files.value[.local]!.sorted()) == [syncedFile1, syncedFile2]
                }

                it("updates sync state") {
                    // Given
                    let data = Data.randomData()
                    let syncedFile = service.addFile(FileEntry(path: "1"), storageType: .dropbox, data: data, metadata: "meta")
                        .fetchExpectedNextAndCompleted()!
                    let createdRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!

                    // When
                    let updatedData = Data.randomData()
                    service.updateFile(syncedFile.id, data: updatedData).startAndExpectCompleted()

                    // Then
                    let updatedRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!

                    expect(updatedRecord.data) == updatedData
                    expect(updatedRecord.syncState) == FileSyncState.notSynced
                    expect(updatedRecord.syncMetadata.currentVersion) == createdRecord.syncMetadata.currentVersion + 1
                    expect(updatedRecord.syncMetadata.syncedVersion) == createdRecord.syncMetadata.syncedVersion
                    expect(updatedRecord.syncMetadata.syncedMetadata) == createdRecord.syncMetadata.syncedMetadata
                }

                it("removes file") {
                    // Given
                    let syncedFile = service.addFile(FileEntry(path: "path/file1"), storageType: .local, data: .randomData(), metadata: "")
                        .fetchExpectedNextAndCompleted()!

                    // When
                    service.removeFile(syncedFile.id).startAndExpectCompleted()

                    // Then
                    let observer = persistenceService.fetchRecord(syncedFile.id).startTestObserver()
                    expect(observer.error) == AppError.unknownError
                }

                it("forbids linking files for the same remote files") {
                    // Given
                    let file = FileEntry(path: "path/file1")
                    let storageType = FileStorageType.dropbox

                    _ = service.addFile(file, storageType: storageType, data: .randomData(), metadata: "")
                        .fetchExpectedNextAndCompleted()!

                    // When
                    let observer = service.addFile(file, storageType: storageType, data: .randomData(), metadata: "")
                        .startTestObserver()

                    // Then
                    expect(observer.error) == AppError.safeAlreadyExists(name: file.name)
                }
            }

            describe("LocalFileChangeSource") {
                it("sends changes on start") {
                    // Given
                    let record1 = createRecord("1", syncState: .notSynced, currentVersion: 2, syncedVersion: 1)
                    persistenceService.createRecord(record1).startAndExpectCompleted()

                    let record2 = createRecord("2", syncState: .notSynced, currentVersion: 2, syncedVersion: 1)
                    persistenceService.createRecord(record2).startAndExpectCompleted()

                    let record3 = createRecord("3", syncState: .synced, currentVersion: 1, syncedVersion: 1)
                    persistenceService.createRecord(record3).startAndExpectCompleted()

                    let record4 = createRecord("4", syncState: .conflict, currentVersion: 1, syncedVersion: 1)
                    persistenceService.createRecord(record4).startAndExpectCompleted()

                    let record5 = createRecord("4", linkState: .remotelyUnlinked, syncState: .synced, currentVersion: 1, syncedVersion: 1)
                    persistenceService.createRecord(record5).startAndExpectCompleted()

                    // When
                    let observer = service.localChanges().startTestObserver()

                    // Then
                    expect(observer.values) == [LocalFileChange(syncedFileRecord: record1), LocalFileChange(syncedFileRecord: record2)]
                }

                it("sends local changes for updated files") {
                    // Given
                    let observer = service.localChanges().startTestObserver()

                    let syncedFile = service.addFile(FileEntry(path: "1"), storageType: .dropbox, data: .randomData(), metadata: "meta")
                        .fetchExpectedNextAndCompleted()!

                    // When
                    service.updateFile(syncedFile.id, data: .randomData()).startAndExpectCompleted()

                    // Then
                    let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                    expect(observer.fetchValue()) == LocalFileChange(syncedFileRecord: syncedFileRecord)
                }
            }

            describe("FileStorageSyncApplier") {
                describe("findSyncedFileId") {
                    it("finds file by file and storageType") {
                        // Given
                        let file = FileEntry(path: "1")
                        let storageType = FileStorageType.dropbox

                        let syncedFile = service.addFile(file, storageType: storageType, data: .randomData(), metadata: "meta")
                            .fetchExpectedNextAndCompleted()!

                        _ = service.addFile(file, storageType: .icloud, data: .randomData(), metadata: "meta")
                            .fetchExpectedNextAndCompleted()!

                        // When
                        let foundFile = service.findSyncedFileId(forFile: file, storageType: storageType).fetchExpectedNextAndCompleted()!

                        // Then
                        expect(syncedFile.id) == foundFile
                    }

                    it("responds with error if it can't find file") {
                        // Given
                        let file = FileEntry(path: "1")
                        let storageType = FileStorageType.dropbox

                        // When
                        let observer = service.findSyncedFileId(forFile: file, storageType: storageType).startTestObserver()

                        // Then
                        expect(observer.error) == AppError.safeDoesNotExist(name: file.name)
                    }
                }

                describe("markAsRemotelyUnlinked") {
                    it("marks resource as unlinked") {
                        // Given
                        let file = FileEntry(path: "1")
                        let storageType = FileStorageType.dropbox

                        let syncedFile = service.addFile(file, storageType: storageType, data: .randomData(), metadata: "meta")
                            .fetchExpectedNextAndCompleted()!

                        // When
                        service.markAsRemotelyUnlinked(syncedFile.id)
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.linkState) == FileLinkState.remotelyUnlinked
                    }
                }

                describe("syncedToRemoteStorage") {
                    it("updates file as synced") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .notSynced, currentVersion: 2, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        service.syncedToRemoteStorage(syncedFile.id, syncedLocalVersion: 2, syncedMetadata: "meta2")
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.synced
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta2"
                    }

                    it("updates file, not synced because current version is different") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .notSynced, currentVersion: 3, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        service.syncedToRemoteStorage(syncedFile.id, syncedLocalVersion: 2, syncedMetadata: "meta2")
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.notSynced
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 3
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta2"
                    }

                    it("updates file, conflict") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .conflict, currentVersion: 3, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        service.syncedToRemoteStorage(syncedFile.id, syncedLocalVersion: 2, syncedMetadata: "meta2")
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.conflict
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 3
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta2"
                    }
                }

                describe("markAsConflict") {
                    it("marks as conflict") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .notSynced, currentVersion: 3, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        let remoteData = FileData(data: .randomData(), metadata: "meta3")
                        service.markAsConflict(syncedFile.id, remoteData: remoteData)
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.conflict
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 3
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 1
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta"
                        expect(syncedFileRecord.syncMetadata.remoteFileData) == remoteData
                    }
                }

                describe("updatedFromRemoteStorage") {
                    it("makes clean update if no conflict") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .synced, currentVersion: 1, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        let remoteData = FileData(data: .randomData(), metadata: "remotemeta")
                        service.updatedFromRemoteStorage(syncedFile.id, remoteData: remoteData)
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.synced
                        expect(syncedFileRecord.data) == remoteData.data
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "remotemeta"
                        expect(syncedFileRecord.syncMetadata.remoteFileData).to(beNil())
                    }

                    it("marks as conflict if local changes are not synced") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .notSynced, currentVersion: 2, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        let remoteData = FileData(data: .randomData(), metadata: "remotemeta")
                        service.updatedFromRemoteStorage(syncedFile.id, remoteData: remoteData)
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.conflict
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 1
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta"
                        expect(syncedFileRecord.syncMetadata.remoteFileData) == remoteData
                    }

                    it("leaves as conflict, updates remote data") {
                        // Given
                        let syncedFile = createRecord("1", syncState: .conflict, currentVersion: 2, syncedVersion: 1, syncedMetadata: "meta")
                        persistenceService.createRecord(syncedFile).startAndExpectCompleted()

                        // When
                        let remoteData = FileData(data: .randomData(), metadata: "remotemeta")
                        service.updatedFromRemoteStorage(syncedFile.id, remoteData: remoteData)
                            .startAndExpectCompleted()

                        // Then
                        let syncedFileRecord = persistenceService.fetchRecord(syncedFile.id).fetchExpectedNextAndCompleted()!
                        expect(syncedFileRecord.syncState) == FileSyncState.conflict
                        expect(syncedFileRecord.syncMetadata.currentVersion) == 2
                        expect(syncedFileRecord.syncMetadata.syncedVersion) == 1
                        expect(syncedFileRecord.syncMetadata.syncedMetadata) == "meta"
                        expect(syncedFileRecord.syncMetadata.remoteFileData) == remoteData
                    }
                }
            }
        }
    }
}

private func createRecord(_ id: String,
                          linkState: FileLinkState = .linked,
                          syncState: FileSyncState = .synced,
                          currentVersion: LocalVersion, syncedVersion: LocalVersion,
                          syncedMetadata: FileMetadata = "meta") -> SyncedFileRecord {
    let syncMeta = FileSyncMetadata(currentVersion: currentVersion,
                                    syncedVersion: syncedVersion,
                                    syncedMetadata: syncedMetadata)
    return SyncedFileRecord(id: id, name: id, data: .randomData(),
                      storageType: .dropbox, storageFile: FileEntry(path: id),
                      linkState: .linked, syncState: syncState, syncMetadata: syncMeta)
}
