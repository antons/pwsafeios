//
//  ChangePasswordMakePropsTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble
import PwsafeSwift
@testable import PwsafeIOS

class ChangePasswordMakePropsTest: QuickSpec {
    override func spec() {
        describe("ChangePassword make props") {
            let syncedFile = SyncedFile(
                id: "id",
                name: "safe",
                syncState: .synced,
                linkState: .linked)

            let decrypted = DecryptedSafe(syncedFile: syncedFile, pwsafe: Pwsafe(), password: "password")

            it("makes props for valid password") {
                let state = ChangePasswordState(minPasswordLength: 3, updatedPassword: "test")
                let safeState = ManageSafeState(decryptedSafe: decrypted, changePassword: state)

                let props = makeProps(from: safeState, commands: MockChangePasswordCommands())

                expect(props) == ChangePasswordController.Props(
                    passwordText: "test",
                    isPasswordAcceptable: true,
                    onDisposed: .nop)
            }

            it("makes props for invalid password") {
                let state = ChangePasswordState(minPasswordLength: 3, updatedPassword: "te")
                let safeState = ManageSafeState(decryptedSafe: decrypted, changePassword: state)

                let props = makeProps(from: safeState, commands: MockChangePasswordCommands())

                expect(props) == ChangePasswordController.Props(
                    passwordText: "te",
                    isPasswordAcceptable: false,
                    onDisposed: .nop)
            }
        }
    }
}

private class MockChangePasswordCommands: ChangePasswordCommands {
    func passwordFieldUpdate() -> CommandWith<String?> {
        return .nop
    }

    func dispose() -> Command {
        return .nop
    }

    func updateSafe(_ makeSafe: @escaping () -> DecryptedSafe) -> Command {
        return .nop
    }
}
