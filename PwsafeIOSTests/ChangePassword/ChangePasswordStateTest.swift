//
//  ChangePasswordStateTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import PwsafeIOS

class ChangePasswordStateTest: QuickSpec {
    override func spec() {
        describe("ChangePasswordState") {
            it("") {
                var state = ChangePasswordState(minPasswordLength: 4, updatedPassword: "")

                reduce(state: &state, action: ChangePasswordUpdateField(passwordText: "text"))

                expect(state.updatedPassword) == "text"
            }
        }
    }
}
