//
//  DropboxSyncServiceTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
@testable import PwsafeIOS

class DropboxSyncServiceTest: QuickSpec {
    override func spec() {
        describe("DropboxSyncService") {
            var syncService: DropboxSyncService!
            var fileStateProvider: RemoteFileStateProviderMock!
            var fileStorage: InMemoryFileStorage!

            beforeEach {
                fileStorage = InMemoryFileStorage(type: .dropbox)
                fileStateProvider = RemoteFileStateProviderMock()
                syncService = DropboxSyncService(fileStateProvider: fileStateProvider, fileStorage: fileStorage)
            }

            it("receives remote file updated") {
                // Given
                let updatedData: Data = .randomData()

                fileStorage.addMemoryFile("file", data: updatedData, metadata: 2)
                fileStateProvider.states = [RemoteFileState(file: FileEntry(path: "file"), storageType: .dropbox, metadata: "1")]

                // When
                let observer = syncService.remoteChanges().startTestObserver()
                syncService.sync().startAndExpectCompleted()

                // Then
                expect(observer.completed) == false
                expect(observer.fetchValue()) == RemoteFileChange(
                    file: FileEntry(path: "file"),
                    storageType: .dropbox,
                    change: .updated(data: FileData(data: updatedData, metadata: "2")))
            }

            it("receives remote file unlinked") {
                // Given
                fileStateProvider.states = [RemoteFileState(file: FileEntry(path: "file"), storageType: .dropbox, metadata: "1")]

                // When
                let observer = syncService.remoteChanges().startTestObserver()
                syncService.sync().startAndExpectCompleted()

                // Then
                expect(observer.completed) == false
                expect(observer.fetchValue()) == RemoteFileChange(
                    file: FileEntry(path: "file"),
                    storageType: .dropbox,
                    change: .unlinked)
            }
        }
    }
}

class RemoteFileStateProviderMock: RemoteFileStateProvider {
    var states: [RemoteFileState] = []

    func listStates() -> SignalProducer<[RemoteFileState], AppError> {
        return SignalProducer(value: states)
    }
}
