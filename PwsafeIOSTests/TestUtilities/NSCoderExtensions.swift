//
//  NSCoderExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension NSCoder {
    static func encode(_ encodeBlock: (_ archiver: NSCoder) -> Void) -> NSCoder {
        let data = NSMutableData()
        let archiver = NSKeyedArchiver(forWritingWith: data)
        encodeBlock(archiver)
        archiver.finishEncoding()

        return NSKeyedUnarchiver(forReadingWith: data as Data)
    }

    static func decode<T: NSCoding>(_ encodeBlock: (_ archiver: NSCoder) -> Void) -> T? {
        let archived = encode(encodeBlock)
        return T(coder: archived)
    }

    static func encodeAndDecode<T: NSCoding>(_ object: T) -> T? {
        let archived = NSKeyedArchiver.archivedData(withRootObject: object)
        return NSKeyedUnarchiver.unarchiveObject(with: archived) as? T
    }
}
