//
//  SignalProducerTestingSpec.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 07/04/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import Quick
import Nimble


//todo: Need to test failures!
class SignalProducerTestingSpec: QuickSpec {
    override func spec() {
        describe("fetchExpectedResult") {
            it("fetchExpectedNext") {
                let signalProducer = SignalProducer<String, Never>(value: "value")
                let value = signalProducer.fetchExpectedNext()
                expect(value) == "value"
            }

            it("fetchExpectedNextAndCompleted") {
                let signalProducer = SignalProducer<String, Never>(value: "value")
                let value = signalProducer.fetchExpectedNextAndCompleted()
                expect(value) == "value"
            }

            it("startAndExpectCompleted") {
                //todo: this is a strange test
                let signalProducer = SignalProducer<Void, Never>([])
                signalProducer.startAndExpectCompleted()
            }

            it("fetchExpectedError") {
                let signalProducer = SignalProducer<Void, TestError>(error: TestError())
                let error = signalProducer.fetchExpectedError()
                expect(error).notTo(beNil())
            }
        }

        describe("fetchEvents") {
            it("fetch value and complete") {
                let signalProducer = SignalProducer<String, TestError>(value: "value")
                let events = signalProducer.fetchEvents()

                assertEquals(events, expected: [.value("value"), .completed])
            }
        }
    }
}

func assertEquals<Value: Equatable, Error: Equatable> (_ lhsEvents: [Signal<Value, Error>.Event], expected rhsEvents: [Signal<Value, Error>.Event]) {
    expect(lhsEvents.count) == rhsEvents.count

    for (lhs, rhs) in zip(lhsEvents, rhsEvents) {
        expect(lhs == rhs).to(beTruthy())
    }
}

func assertEquals<Error: Equatable> (_ lhsEvents: [Signal<Void, Error>.Event], expected rhsEvents: [Signal<Void, Error>.Event]) {
    expect(lhsEvents.count) == rhsEvents.count

    for (lhs, rhs) in zip(lhsEvents, rhsEvents) {
        expect(lhs == rhs).to(beTruthy())
    }
}

private struct TestError: Error {}

extension TestError: Equatable {}

private func == (lhs: TestError, rhs: TestError) -> Bool {
    return true
}

public func == <Error: Equatable> (lhs: Signal<Void, Error>.Event, rhs: Signal<Void, Error>.Event) -> Bool {
    switch (lhs, rhs) {
    case (.value, .value):
        return true

    case let (.failed(left), .failed(right)):
        return left == right

    case (.completed, .completed):
        return true

    case (.interrupted, .interrupted):
        return true

    default:
        return false
    }
}
