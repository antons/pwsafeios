//
//  InMemoryFileStorage.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/05/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import PwsafeIOS

class InMemoryFileStorage: FileStorage {
    fileprivate var files: [String: (Data, Int)] = [:]

    let type: FileStorageType

    init(type: FileStorageType = .local) {
        self.type = type
    }

    func addMemoryFile(_ file: String, data: Data, metadata: Int = 1) {
        files[file] = (data, metadata)
    }

    func fileData(_ file: String) -> FileData? {
        return files[file].map { FileData(data: $0, metadata: String($1)) }
    }

    func fetchData(_ file: FileEntry) -> SignalProducer<FileData, FileStorageError> {
        return fileData(file.path).value(errorIfNil: .fileNotFound(file))
    }

    func fetchMetadata(_ file: FileEntry) -> SignalProducer<FileMetadata?, FileStorageError> {
        return fetchData(file).map({ $0.metadata })
    }

    func updateFile(_ file: FileEntry, data: FileData) -> SignalProducer<FileUpdateResult, FileStorageError> {
        return SignalProducer { observer, _ in
            guard let(_, storedMetadata) = self.files[file.path] else {
                observer.send(error: .fileNotFound(file))
                return
            }

            guard String(storedMetadata) == data.metadata else {
                observer.send(value: .conflict)
                observer.sendCompleted()
                return
            }

            let newMetadata = storedMetadata + 1
            self.files[file.path] = (data.data, newMetadata)

            observer.send(value: .success(newMetadata: String(newMetadata)))
            observer.sendCompleted()
        }
    }

    func createFile(_ path: String, data: Data) -> SignalProducer<FileEntry, FileStorageError> {
        return SignalProducer { observer, _ in
            guard self.fileData(path) == nil else {
                observer.send(error: .fileAlreadyExists(FileEntry(path: path)))
                return
            }

            self.addMemoryFile(path, data: data)
            observer.send(value: FileEntry(path: path))
            observer.sendCompleted()
        }
    }
}
