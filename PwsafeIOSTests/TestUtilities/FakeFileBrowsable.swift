//
//  FakeFileBrowsable.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 8/28/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import PwsafeIOS

private let files: [String: [FolderEntry]] = [
    "/": [
        .folder(FileEntry(path: "folder1")),
        .folder(FileEntry(path: "folder2")),
        .file(FileEntry(path: "file1")),
        .file(FileEntry(path: "file2"))
    ],

    "folder1": [
        .file(FileEntry(path: "file11")),
        .file(FileEntry(path: "file12"))
    ],

    "folder2": [
        .file(FileEntry(path: "file21")),
        .file(FileEntry(path: "file22"))
    ]
]

class FakeFileBrowsable: FileBrowsable {
    static var rootFolder: FileEntry = FileEntry(path: "/")

    func listEntries(_ fileItem: FileEntry) -> SignalProducer<[FolderEntry], FileBrowserError> {
        return SignalProducer { observer, _ in
            observer.send(value: files[fileItem.name]!)
            observer.sendCompleted()
        }
    }

}
