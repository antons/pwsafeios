//
//  TestObserver.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 20/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import Nimble

class TestObserver<Value, Error: Swift.Error> {
    private(set) var completed: Bool = false
    private(set) var values: [Value] = []
    private(set) var error: Error?
    private(set) var interrupted: Bool = false

    private var consumableValues: [Value] = []

    private(set) lazy var observer: Signal<Value, Error>.Observer = Signal.Observer { event in
        switch event {
        case .value(let value):
            self.values.append(value)
            self.consumableValues.append(value)

        case .failed(let error):
            self.error = error

        case .completed:
            self.completed = true

        case .interrupted:
            self.interrupted = true
        }
    }

    func fetchValues() -> [Value] {
        let result = consumableValues
        consumableValues.removeAll()
        return result
    }

    func fetchValue() -> Value {
        let value = consumableValues.first
        consumableValues.removeFirst()
        return value!
    }
}

extension TestObserver where Value: Equatable {
    func assertValues(_ expectedValues: [Value], file: FileString = #file, line: UInt = #line) {
        expect(self.values, file: file, line: line) == expectedValues
    }
}
