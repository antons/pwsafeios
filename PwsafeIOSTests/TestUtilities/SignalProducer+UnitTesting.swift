//
//  SignalProducer+UnitTesting.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 07/04/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
import Nimble

extension SignalProducer {
    func startTestObserver() -> TestObserver<Value, Error> {
        let observer = TestObserver<Value, Error>()
        start(observer.observer)
        return observer
    }

    func fetchExpectedNext() -> Value? {
        return startTestObserver().values.first!
    }

    func fetchExpectedNextAndCompleted() -> Value? {
        let observer = startTestObserver()
        expect(observer.values.first).notTo(beNil())
        expect(observer.completed) == true
        return observer.values.first
    }

    func startAndExpectCompleted() {
        expect(self.startTestObserver().completed) == true
    }

    func fetchExpectedError() -> Error? {
        let error = startTestObserver().error
        expect(error).notTo(beNil())
        return error
    }

    func fetchEvents() -> [Signal<Value, Error>.Event] {
        var events: [Signal<Value, Error>.Event] = []

        start { event in
            events.append(event)
        }

        return events
    }
}
