//
//  LocalChangeProcessorTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa
@testable import PwsafeIOS

class LocalChangeProcessorTest: QuickSpec {
    override func spec() {
        describe("LocalChangeProcessor") {
            var processor: LocalChangeProcessor!
            var storage: InMemoryFileStorage!

            let file = FileEntry(path: "path")

            beforeEach {
                storage = InMemoryFileStorage(type: .dropbox)
                processor = LocalChangeProcessor(storageProvider: StorageProviderMock(storage: storage))
            }

            it("updates remote storage if local file changed") {
                // Given
                let localData = Data(bytes: [1, 2, 3])
                let remoteData = Data(bytes: [4, 5, 6])

                storage.addMemoryFile(file.path, data: remoteData, metadata: 1)

                let change = LocalFileChange(fileId: "id", localVersion: 2, data: localData, file: file, storageType: .dropbox, syncedFileMetadata: "1")

                // When
                let result = processor.process(change).fetchExpectedNextAndCompleted()

                // Then
                expect(result) == FileSyncResult.success(fileId: "id", localVersion: 2, serverMetadata: "2")

                let storedData = storage.fileData(file.path)!.data
                expect(storedData) == localData
            }

            it("conflict") {
                // Given
                let localData = Data(bytes: [1, 2, 3])
                let remoteData = Data(bytes: [4, 5, 6])

                storage.addMemoryFile(file.path, data: remoteData, metadata: 3)

                let change = LocalFileChange(fileId: "id", localVersion: 2, data: localData, file: file, storageType: .dropbox, syncedFileMetadata: "1")

                // When
                let result = processor.process(change).fetchExpectedNextAndCompleted()

                // Then
                expect(result) == FileSyncResult.mergeRequired(fileId: "id", remoteData: FileData(data: remoteData, metadata: "3"))

                let storedData = storage.fileData(file.path)!.data
                expect(storedData) == remoteData
            }

            it("remote unlink") {
                // Given
                let localData = Data(bytes: [1, 2, 3])

                let change = LocalFileChange(fileId: "id", localVersion: 2, data: localData, file: file, storageType: .dropbox, syncedFileMetadata: "1")

                // When
                let result = processor.process(change).fetchExpectedNextAndCompleted()

                // Then
                expect(result) == FileSyncResult.remotelyUnlinked(fileId: "id")
            }
        }
    }
}
