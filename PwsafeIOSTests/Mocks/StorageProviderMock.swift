//
//  StorageProviderMock.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa
@testable import PwsafeIOS

class StorageProviderMock: StorageProvider {
    var storage: FileStorage?

    init(storage: FileStorage?) {
        self.storage = storage
    }

    func storage(type: FileStorageType) -> FileStorage? {
        return storage
    }
}
