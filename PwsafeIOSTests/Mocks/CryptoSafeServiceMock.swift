//
//  CryptoSafeServiceMock.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 14/01/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import PwsafeIOS

class CryptoSafeServiceMock: CryptoSafeServiceProtocol {
    var syncedFileRecords: [SyncedFile: DecryptedSafe] = [:]

    func loadSafe(from syncedFile: SyncedFile, password: String) -> SignalProducer<DecryptedSafe, AppError> {
        fatalError()
    }

    func storeSafe(_ safe: DecryptedSafe) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.syncedFileRecords[safe.syncedFile] = safe
            observer.sendCompleted()
        }
    }
}
