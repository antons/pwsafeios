//
//  FileManagerMock.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
@testable import PwsafeIOS

class FileManagerMock: FileManagerProtocol {
    var files: [String] = []
    var data: [String: Data] = [:]

    func addFile(_ path: String, data: Data) {
        guard !files.contains(path) else { return }

        files.append(path)
        self.data[path] = data
    }

    func fetchData(_ path: String) -> SignalProducer<Data, FileManagerError> {
        return data[path].value(errorIfNil: .fileNotFound(path: path))
    }

    func create(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError> {
        return SignalProducer { observer, _ in
            guard !self.data.keys.contains(path) else {
                observer.send(error: .fileAlreadyExists(path: path))
                return
            }

            self.files.append(path)
            self.data[path] = data
            observer.sendCompleted()
        }
    }

    func update(_ path: String, data: Data) -> SignalProducer<Void, FileManagerError> {
        return SignalProducer { observer, _ in
            self.data[path] = data
            observer.sendCompleted()
        }
    }

    func remove(_ path: String) -> SignalProducer<Void, FileManagerError> {
        return SignalProducer { observer, _ in
            self.files = self.files.filter({ $0 != path })
            self.data.removeValue(forKey: path)
            observer.sendCompleted()
        }
    }

    func listFiles(_ path: String) -> SignalProducer<[String], FileManagerError> {
        return SignalProducer { observer, _ in
            let result = self.files
                .filter({ $0.hasPrefix(path) })

            observer.send(value: result)
            observer.sendCompleted()
        }
    }
}
