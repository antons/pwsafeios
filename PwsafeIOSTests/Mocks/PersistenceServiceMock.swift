//
//  PersistentServiceMock.swift
//  PwrecordIOS
//
//  Created by Anton Selyanin on 12/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
@testable import PwsafeIOS

class PersistenceServiceMock: PersistenceServiceProtocol {
    var records: [String: SyncedFileRecord] = [:]

    private(set) lazy var files: Property<[SyncedFileRecord]> = Property(self.filesProperty)
    private let filesProperty: MutableProperty<[SyncedFileRecord]> = MutableProperty([])

    func list() -> SignalProducer<[SyncedFileRecord], AppError> {
        return SignalProducer(value: Array(records.values.sorted { $0.id < $1.id }))
    }

    private func reloadRecords() {
        list()
            .ignoreError()
            .on(value: { self.filesProperty.value = $0 })
            .start()
    }

    func fetchRecord(_ id: String) -> SignalProducer<SyncedFileRecord, AppError> {
        guard let value = records[id] else { return SignalProducer(error: .unknownError) }
        return SignalProducer(value: value)
    }

    func createRecord(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.records[record.id] = record
            observer.sendCompleted()
        }
            .on(completed: self.reloadRecords)
    }

    func update(_ record: SyncedFileRecord) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.records[record.id] = record
            observer.sendCompleted()
        }
            .on(completed: self.reloadRecords)
    }

    func remove(_ id: String) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.records.removeValue(forKey: id)
            observer.sendCompleted()
        }
            .on(completed: self.reloadRecords)
    }
}
