//
//  OpenSafeMakePropsTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble

@testable import PwsafeIOS

class OpenSafeMakePropsTest: QuickSpec {
    override func spec() {
        describe("OpenSafe make props") {
            it("") {
                let file = SyncedFile(id: "file", name: "file", syncState: .synced, linkState: .linked)
                let state = OpenSafeState(file: file, enteredPassword: "test", isValid: true)
                let props = makeProps(from: state, commands: MockOpenSafeCommands())

                expect(props.password) == "test"
                expect(props.isValid) == true
            }
        }
    }
}

private class MockOpenSafeCommands: OpenSafeCommands {
    func passwordUpdated() -> CommandWith<String?> {
        return .nop
    }

    func unlockSafe(file: SyncedFile, password: String) -> Command {
        return .nop
    }

    func close() -> Command {
        return .nop
    }

    func dispose() -> Command {
        return .nop
    }
}
