//
//  OpenSafeStateTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 20/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Quick
import Nimble

@testable import PwsafeIOS

class OpenSafeStateTest: QuickSpec {
    override func spec() {
        describe("OpenSafeState") {
            let file = SyncedFile(id: "file", name: "file", syncState: .synced, linkState: .linked)

            it("makes state valid") {
                // Given
                var state = OpenSafeState(file: file, enteredPassword: nil, isValid: false)

                // When
                reduce(state: &state, action: OpenSafeUpdatePassword(passwordText: "test"))

                // Then
                expect(state.enteredPassword) == "test"
                expect(state.isValid) == true
            }

            it("makes state invalid") {
                // Given
                var state = OpenSafeState(file: file, enteredPassword: "test", isValid: true)

                // When
                reduce(state: &state, action: OpenSafeUpdatePassword(passwordText: ""))

                // Then
                expect(state.enteredPassword) == ""
                expect(state.isValid) == false
            }
        }
    }
}

