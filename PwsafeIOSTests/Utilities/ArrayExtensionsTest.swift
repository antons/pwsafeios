//
//  ArrayExtensionsTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 09/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import PwsafeIOS

private class FooObject {}

class ArrayExtensionsTest: QuickSpec {
    override func spec() {
        describe("Array") {
            it("removeAllIdenticalTo") {
                // Given
                let object1 = FooObject()
                let object2 = FooObject()
                let object3 = FooObject()

                var array = [object1, object2, object2, object3]
                
                // When
                array.removeAllIdenticalTo(object2)

                // Then
                expect(array.count) == 2
                expect(array[0]) === object1
                expect(array[1]) === object3
            }
        }
    }
}
