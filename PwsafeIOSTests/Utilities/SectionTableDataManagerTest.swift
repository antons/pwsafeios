//
//  SectionTableDataManagerTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 17/06/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import UIKit
import ReactiveSwift

@testable import PwsafeIOS

//swiftlint:disable force_cast
class SectionTableDataManagerTest: QuickSpec {
    override func spec() {
        describe("DataManager") {
            var cellConfigurator: CellConfigurator!
            var tableView: UITableView!
            var dataManager: SectionTableDataManager<MockItem, CellConfigurator>!

            var items: [(section: String, items: [MockItem])]!
            var itemsProperty: MutableProperty<[(section: String, items: [MockItem])]>!

            beforeEach {
                cellConfigurator = CellConfigurator()
                tableView = UITableView()
                dataManager = SectionTableDataManager(tableView: tableView, cellConfigurator: cellConfigurator)
                items = [
                    (section: "Section 1", items: [MockItem(data: "data11")]),
                    (section: "Section 2", items: [MockItem(data: "data21"), MockItem(data: "data22")])
                ]
                itemsProperty = MutableProperty(items!)

                dataManager.sections <~ itemsProperty

                expect(tableView.dataSource === dataManager).to(beTrue())
                expect(tableView.delegate === dataManager).to(beTrue())
            }

            it("handles data source requests") {
                let dataSource: UITableViewDataSource = dataManager

                expect(dataSource.numberOfSections!(in: tableView)) == 2

                expect(dataSource.tableView(tableView, numberOfRowsInSection: 0)) == 1
                expect(dataSource.tableView(tableView, numberOfRowsInSection: 1)) == 2

                expect(dataSource.tableView!(tableView, titleForHeaderInSection: 0)) == "Section 1"
                expect(dataSource.tableView!(tableView, titleForHeaderInSection: 1)) == "Section 2"

                func cell(section: Int, item: Int) -> MockCell {
                    return dataSource.tableView(tableView, cellForRowAt: IndexPath(item: item, section: section)) as! MockCell
                }

                expect(cell(section: 0, item: 0).item) == MockItem(data: "data11")
                expect(cell(section: 1, item: 0).item) == MockItem(data: "data21")
                expect(cell(section: 1, item: 1).item) == MockItem(data: "data22")
            }

            it("emits signal for when item is selected ") {
                var selectedIndex: IndexPath?
                var selectedItem: MockItem?

                dataManager.selectedIndex.observeValues {
                    selectedIndex = $0
                }

                dataManager.selectedItem.observeValues {
                    selectedItem = $0
                }

                let tableDelegate: UITableViewDelegate = dataManager
                tableDelegate.tableView!(tableView, didSelectRowAt: IndexPath(item: 0, section: 1))

                expect(selectedIndex) == IndexPath(item: 0, section: 1)
                expect(selectedItem) == MockItem(data: "data21")
            }
        }
    }
}

private struct MockItem: Equatable {
    var data: String
}

private func == (lhs: MockItem, rhs: MockItem) -> Bool {
    return lhs.data == rhs.data
}

private class MockCell: UITableViewCell {
    var item: MockItem?
}

private class CellConfigurator: TableCellConfigurator {
    let cellId: String = "cellId"

    func registerCells(_ tableView: UITableView) {
        tableView.register(MockCell.self, forCellReuseIdentifier: cellId)
    }

    func dequeueCell(tableView: UITableView, indexPath: IndexPath, item: MockItem) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        
        let mockCell = cell as! MockCell
        mockCell.item = item
        
        return mockCell
    }
}
