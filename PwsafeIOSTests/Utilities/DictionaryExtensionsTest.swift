//
//  DictionaryExtensionsTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import PwsafeIOS

class DictionaryExtensionsTest: QuickSpec {
    override func spec() {
        describe("DictionaryExtensions") {
            it("valuesMap") {
                let element1 = Element(key: "1", value: "value1")
                let element2 = Element(key: "1", value: "value2")
                let element3 = Element(key: "2", value: "value3")
                let element4 = Element(key: "2", value: "value4")

                let dict: [String: [Element]] = [
                    "1": [element1, element2],
                    "2": [element3, element4]
                ]

                let result: [String: [String]] = dict.valuesMap({ $0.value })

                expect(result.count) == 2
                expect(result["1"]) == ["value1", "value2"]
                expect(result["2"]) == ["value3", "value4"]
            }
        }
    }
}

private struct Element {
    let key: String
    let value: String
}
