//
//  KeyboardServiceTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 13/08/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import UIKit
import Quick
import Nimble
import ReactiveSwift

@testable import PwsafeIOS

class KeyboardServiceTest: QuickSpec {
    override func spec() {
        describe("KeyboardService") {
            it("parses UIKeyboardWillShow notification") {
                // Given
                let nc = NotificationCenter()
                let observer = TestObserver<KeyboardInfo, Never>()
                let service = KeyboardService(notificationCenter: nc)

                let userInfo: [AnyHashable: Any] = [
                    UIResponder.keyboardFrameBeginUserInfoKey: NSValue(cgRect: CGRect(x: 10, y: 20, width: 100, height: 200)),
                    UIResponder.keyboardFrameEndUserInfoKey: NSValue(cgRect: CGRect(x: 20, y: 40, width: 200, height: 400)),
                    UIResponder.keyboardAnimationCurveUserInfoKey: NSNumber(value: UIView.AnimationCurve.easeIn.rawValue),
                    UIResponder.keyboardAnimationDurationUserInfoKey: NSNumber(value: 100),
                    UIResponder.keyboardIsLocalUserInfoKey: NSNumber(value: true)]
                
                service.keyboardWillShow
                    .observe(observer.observer)

                // When
                nc.post(name: UIResponder.keyboardWillShowNotification, object: nil, userInfo: userInfo)

                // Then
                let expected = KeyboardInfo(
                    frameBegin: CGRect(x: 10, y: 20, width: 100, height: 200),
                    frameEnd: CGRect(x: 20, y: 40, width: 200, height: 400),
                    animationCurve: .easeIn,
                    animationDuration: 100,
                    isLocal: true)
                
                expect(observer.values.first).toEventually(equal(expected))
            }
        }
    }
}
