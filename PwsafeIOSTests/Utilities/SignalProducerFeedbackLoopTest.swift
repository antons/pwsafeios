//
//  SignalProducerFeedbackLoopTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 18/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import UIKit
import ReactiveSwift

@testable import PwsafeIOS

private struct TestState {
    var elements: [String]
    
    enum Action {
        case addElement(String)
        case removeElement(String)
    }
}

extension TestState {
    init() {
        self.elements = []
    }
}

extension TestState: Equatable {
    static func == (lhs: TestState, rhs: TestState) -> Bool {
        return lhs.elements == rhs.elements
    }
}

//swiftlint:disable force_cast
class SignalProducerFeedbackLoopTest: QuickSpec {
    override func spec() {
        describe("SignalProducerFeedbackLoop") {
            
            it("") {
                
                let state = TestState()
                
                let accumulator: (TestState, TestState.Action) -> TestState = { state, action in
                    var newState = state
                    
                    switch action {
                    case .addElement(let element):
                        newState.elements.append(element)
                        
                    case .removeElement(let element):
                        if let index = newState.elements.firstIndex(of: element) {
                            newState.elements.remove(at: index)
                        }
                    }
                    
                    return newState
                }
                
                let numberTranslator: (TestState) -> TestState.Action? = { state in
                    guard let last = state.elements.last else { return nil }
                    
                    switch last {
                    case "one":
                        return .addElement("1")
                        
                    case "two":
                        return .addElement("2")
                        
                    default:
                        return nil
                    }
                }

                let (signal, observer) = Signal<TestState.Action, Never>.pipe()
                
                let inputFeedback: (SignalProducer<TestState, Never>) -> SignalProducer<TestState.Action, Never> = { _ in
                    return SignalProducer(signal)
                }
                
                let stateProducer: SignalProducer<TestState, Never> = SignalProducer<TestState.Action, Never>.system(
                    state,
                    accumulator: accumulator,
                    scheduler: QueueScheduler.main,
                    feedback: inputFeedback, lift(numberTranslator))
                
                let stateObserver = TestObserver<TestState, Never>()
                
                stateProducer
                    .logEvents()
                    .start(stateObserver.observer)
                
                expect(stateObserver.values).toEventually(equal([TestState()]))
                
                observer.send(value: .addElement("one"))
                
                let expectedValues = [TestState(elements: []), TestState(elements: ["one"]), TestState(elements: ["one", "1"])]
                
                expect(stateObserver.values).toEventually(equal(expectedValues))
            }
        }
    }
}

func lift<A, B>(_ transform: @escaping (A) -> B?) -> (SignalProducer<A, Never>) -> SignalProducer<B, Never> {
    return { inputProducer in
        return inputProducer.filterMap(transform)
    }
}
