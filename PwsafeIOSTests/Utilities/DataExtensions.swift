//
//  DataExtensions.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 22/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

extension Data {
    static func randomData(count: Int = 10) -> Data {
        var array = [UInt8]()
        for _ in 0 ..< count {
            array.append(UInt8(truncatingIfNeeded: arc4random()))
        }

        return Data(bytes: array)
    }
}
