//
//  CollectionExtensionTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 29/09/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
@testable import PwsafeIOS

class CollectionExtensionsTest: QuickSpec {
    override func spec() {
        describe("CollectionExtensionsTest") {
            it("group by") {
                let element1 = Element(key: "1", value: "value1")
                let element2 = Element(key: "1", value: "value2")
                let element3 = Element(key: "2", value: "value1")
                let element4 = Element(key: "2", value: "value1")

                let array: [Element] = [element1, element2, element3, element4]

                let result: [String: [Element]] = array.group(by: { $0.key })

                expect(result.count) == 2
                expect(result["1"]) == [element1, element2]
                expect(result["2"]) == [element3, element4]
            }
        }
    }
}

private struct Element {
    let key: String
    let value: String
}

extension Element: Equatable {}
private func == (lhs: Element, rhs: Element) -> Bool {
    return lhs.key == rhs.key
        && lhs.value == rhs.value
}
