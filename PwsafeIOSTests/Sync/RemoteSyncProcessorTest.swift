//
//  RemoteSyncProcessorTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/10/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
@testable import PwsafeIOS

class RemoteSyncProcessorTest: QuickSpec {
    override func spec() {
        describe("RemoteSyncProcessor") {
            var remoteChangeSource: RemoteFileChangeSourceMock!
            var storageSyncApplier: SyncApplierMock!
            var syncProcessor: RemoteSyncProcessor!

            beforeEach {
                remoteChangeSource = RemoteFileChangeSourceMock()
                storageSyncApplier = SyncApplierMock()
                syncProcessor = RemoteSyncProcessor(remoteChangeSource: remoteChangeSource, storageSyncApplier: storageSyncApplier)
            }

            it("saves remote data to local storage") {
                // Given
                let remoteData = FileData(data: .randomData(), metadata: "meta")
                let file = FileEntry(path: "path")
                let storageType = FileStorageType.dropbox
                let change = RemoteFileChange(file: file, storageType: storageType, change: .updated(data: remoteData))
                let fileId = "1"

                storageSyncApplier.safes.append((file, storageType, fileId))

                // When
                syncProcessor.start()
                remoteChangeSource.changesObserver.send(value: change)

                // Then
                let actualRemoteData = storageSyncApplier.updatedFromRemoteStorage[fileId]
                expect(actualRemoteData) == remoteData
            }

            it("marks remotely unlinked files") {
                // Given
                let file = FileEntry(path: "path")
                let storageType = FileStorageType.dropbox
                let change = RemoteFileChange(file: file, storageType: storageType, change: .unlinked)
                let fileId = "1"

                storageSyncApplier.safes.append((file, storageType, fileId))

                // When
                syncProcessor.start()
                remoteChangeSource.changesObserver.send(value: change)

                // Then
                expect(storageSyncApplier.markedAsRemotelyUnlinked.contains(fileId)) == true
            }
        }
    }
}

private class RemoteFileChangeSourceMock: RemoteFileChangeSource {
    let changesObserver: Signal<RemoteFileChange, AppError>.Observer
    private let changesProducer: SignalProducer<RemoteFileChange, AppError>

    init() {
        let changesSignal: Signal<RemoteFileChange, AppError>
        (changesSignal, changesObserver) = Signal.pipe()

        changesProducer = SignalProducer(changesSignal)
    }

    func remoteChanges() -> SignalProducer<RemoteFileChange, AppError> {
        return changesProducer
    }
}
