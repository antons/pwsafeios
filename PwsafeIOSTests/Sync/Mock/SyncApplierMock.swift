//
//  SyncApplierMock.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import ReactiveSwift
@testable import PwsafeIOS

class SyncApplierMock: FileStorageSyncApplier {
    //swiftlint:disable large_tuple
    var safes: [(FileEntry, FileStorageType, SyncedFileId)] = []

    private(set) var markedAsRemotelyUnlinked: Set<SyncedFileId> = Set()
    private(set) var syncedToRemoteStorage: [SyncedFileId: (LocalVersion, FileMetadata?)] = [:]
    private(set) var markedAsConflict: [SyncedFileId: FileData] = [:]
    private(set) var updatedFromRemoteStorage: [SyncedFileId: FileData] = [:]

    func findSyncedFileId(forFile file: FileEntry, storageType: FileStorageType) -> SignalProducer<SyncedFileId, AppError> {
        return SignalProducer { observer, _ in

            guard let (_, _, fileId) = self.safes.filter({ $0.0 == file && $0.1 == storageType }).first else {
                observer.send(error: .unknownError)
                return
            }

            observer.send(value: fileId)
            observer.sendCompleted()
        }
    }

    func markAsRemotelyUnlinked(_ fileId: SyncedFileId) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.markedAsRemotelyUnlinked.insert(fileId)
            observer.sendCompleted()
        }
    }

    func syncedToRemoteStorage(_ fileId: SyncedFileId, syncedLocalVersion: LocalVersion, syncedMetadata: FileMetadata?) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.syncedToRemoteStorage[fileId] = (syncedLocalVersion, syncedMetadata)
            observer.sendCompleted()
        }
    }

    func markAsConflict(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.markedAsConflict[fileId] = remoteData
            observer.sendCompleted()
        }
    }

    func updatedFromRemoteStorage(_ fileId: SyncedFileId, remoteData: FileData) -> SignalProducer<Void, AppError> {
        return SignalProducer { observer, _ in
            self.updatedFromRemoteStorage[fileId] = remoteData
            observer.sendCompleted()
        }
    }
}
