//
//  LocalSyncProcessorTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 01/10/2016.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
@testable import PwsafeIOS

//swiftlint:disable function_body_length
class LocalSyncProcessorTest: QuickSpec {
    override func spec() {
        describe("LocalSyncProcessor") {
            var localChangeSource: LocalFileChangeSourceMock!
            var localChangeProcessor: LocalChangeProcessorMock!
            var storageSyncApplier: SyncApplierMock!

            var processor: LocalSyncProcessor!

            beforeEach {
                localChangeSource = LocalFileChangeSourceMock()
                localChangeProcessor = LocalChangeProcessorMock()
                storageSyncApplier = SyncApplierMock()

                processor = LocalSyncProcessor(
                    localChangeSource: localChangeSource,
                    localChangeProcessor: localChangeProcessor,
                    storageSyncApplier: storageSyncApplier)
            }

            it("syncs successfully") {
                // Given
                let fileId = "1"
                let change = LocalFileChange(fileId: fileId,
                                             localVersion: 2,
                                             data: .randomData(),
                                             file: FileEntry(path: "path"),
                                             storageType: .dropbox,
                                             syncedFileMetadata: "meta")

                localChangeProcessor.results[fileId] = FileSyncResult.success(fileId: fileId, localVersion: 2, serverMetadata: "meta2")

                // When
                processor.start()
                localChangeSource.changesObserver.send(value: change)

                // Then
                let (actualLocalVersion, actualFileMetadata) = storageSyncApplier.syncedToRemoteStorage[fileId]!

                expect(actualLocalVersion) == 2
                expect(actualFileMetadata) == "meta2"
            }

            it("marks as conflict") {
                // Given
                let fileId = "1"
                let change = LocalFileChange(fileId: fileId,
                                             localVersion: 2,
                                             data: .randomData(),
                                             file: FileEntry(path: "path"),
                                             storageType: .dropbox,
                                             syncedFileMetadata: "meta")

                let remoteData = FileData(data: .randomData(), metadata: "meta2")

                localChangeProcessor.results[fileId] = FileSyncResult.mergeRequired(fileId: fileId, remoteData: remoteData)

                // When
                processor.start()
                localChangeSource.changesObserver.send(value: change)

                // Then
                let actualConflictData = storageSyncApplier.markedAsConflict[fileId]
                expect(actualConflictData) == remoteData
            }

            it("marks as conflict") {
                // Given
                let fileId = "1"
                let change = LocalFileChange(fileId: fileId,
                                             localVersion: 2,
                                             data: .randomData(),
                                             file: FileEntry(path: "path"),
                                             storageType: .dropbox,
                                             syncedFileMetadata: "meta")

                localChangeProcessor.results[fileId] = FileSyncResult.remotelyUnlinked(fileId: fileId)

                // When
                processor.start()
                localChangeSource.changesObserver.send(value: change)

                // Then
                expect(storageSyncApplier.markedAsRemotelyUnlinked.contains(fileId)) == true
            }
        }
    }
}

private class LocalFileChangeSourceMock: LocalFileChangeSource {
    let changesObserver: Signal<LocalFileChange, AppError>.Observer
    private let changesProducer: SignalProducer<LocalFileChange, AppError>

    init() {
        let changesSignal: Signal<LocalFileChange, AppError>
        (changesSignal, changesObserver) = Signal.pipe()

        changesProducer = SignalProducer(changesSignal)
    }

    func localChanges() -> SignalProducer<LocalFileChange, AppError> {
        return changesProducer
    }
}

private class LocalChangeProcessorMock: LocalChangeProcessorProtocol {
    var results: [String: FileSyncResult] = [:]

    func process(_ change: LocalFileChange) -> SignalProducer<FileSyncResult, AppError> {
        return results[change.fileId].value(errorIfNil: .unknownError)
    }
}
