//
//  PersistenceService.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 03/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveSwift
import ReactiveCocoa
@testable import PwsafeIOS

//swiftlint:disable function_body_length
class PersistenceServiceTest: QuickSpec {
    override func spec() {
        describe("PersistenceService") {
            var fileManager: FileManagerMock!
            let rootFolder: String = "/ROOT"

            func fromRoot(_ path: String) -> String {
                return rootFolder + "/" + path
            }

            beforeEach {
                fileManager = FileManagerMock()
            }

            it("creates new record") {
                // When
                let persistence = PersistenceService(
                    fileManager: fileManager,
                    rootFolder: rootFolder,
                    scheduler: ImmediateScheduler())

                let syncedFileRecord = SyncedFileRecord(id: "123", name: "name",
                                            data: syncedFileRecordContent(), storageType: .local, storageFile: FileEntry(path: "file"),
                                            linkState: .linked, syncState: .synced,
                                            syncMetadata: FileSyncMetadata(currentVersion: 1, syncedVersion: 2, syncedMetadata: "meta"))
                persistence.createRecord(syncedFileRecord)
                    .startAndExpectCompleted()

                // Then
                let createdFile = fromRoot(syncedFileRecord.id)
                expect(fileManager.files) == [createdFile]

                let data = fileManager.data[createdFile]!
                let resyncedFileRecord = SyncedFileRecord(data: data)!
                expect(syncedFileRecord) == resyncedFileRecord

                expect(persistence.files.value) == [resyncedFileRecord]
            }

            it("lists existing records") {
                // Given
                let record1 = createSyncedFileRecord(1)
                let record2 = createSyncedFileRecord(2)

                fileManager.addFile(fromRoot(record1.id), data: record1.serialize())
                fileManager.addFile(fromRoot(record2.id), data: record2.serialize())

                // When
                let persistence = PersistenceService(
                    fileManager: fileManager,
                    rootFolder: rootFolder,
                    scheduler: ImmediateScheduler())
                let records = persistence.list().fetchExpectedNextAndCompleted()!

                // Then
                expect(records) == [record1, record2]
            }

            it("loads files after instantiation") {
                // Given
                let record1 = createSyncedFileRecord(1)
                let record2 = createSyncedFileRecord(2)

                fileManager.addFile(fromRoot(record1.id), data: record1.serialize())
                fileManager.addFile(fromRoot(record2.id), data: record2.serialize())

                // When
                let persistence = PersistenceService(
                    fileManager: fileManager,
                    rootFolder: rootFolder,
                    scheduler: ImmediateScheduler())

                // Then
                expect(persistence.files.value) == [record1, record2]
            }

            it("updates existing record") {
                // Given
                let record = createSyncedFileRecord(1)
                fileManager.addFile(fromRoot(record.id), data: record.serialize())

                var updatedRecord = record
                updatedRecord.data = Data(bytes: [3, 2, 1])
                updatedRecord.syncMetadata.syncedMetadata = "updated meta"
                updatedRecord.syncMetadata.currentVersion = 2
                updatedRecord.syncMetadata.syncedVersion = 3

                // When
                let persistence = PersistenceService(
                    fileManager: fileManager,
                    rootFolder: rootFolder,
                    scheduler: ImmediateScheduler())
                persistence.update(updatedRecord).startAndExpectCompleted()

                // Then
                let updatedData = fileManager.data[fromRoot(record.id)]!

                let resyncedFileRecord = SyncedFileRecord(data: updatedData)
                expect(resyncedFileRecord) == updatedRecord
                expect(persistence.files.value) == [updatedRecord]
            }

            it("removes record") {
                // Given
                let record1 = createSyncedFileRecord(1)
                fileManager.addFile(fromRoot(record1.id), data: record1.serialize())
                let record2 = createSyncedFileRecord(2)
                fileManager.addFile(fromRoot(record2.id), data: record2.serialize())

                // When
                let persistence = PersistenceService(
                    fileManager: fileManager,
                    rootFolder: rootFolder,
                    scheduler: ImmediateScheduler())
                persistence.remove(record1.id).startAndExpectCompleted()

                // Then
                let records = persistence.list().fetchExpectedNextAndCompleted()!
                expect(records) == [record2]
                expect(fileManager.files) == [fromRoot(record2.id)]
                expect(fileManager.data) == [fromRoot(record2.id): record2.serialize()]

                expect(persistence.files.value) == [record2]
            }
        }
    }
}

private func createSyncedFileRecord(_ id: Int) -> SyncedFileRecord {
    return SyncedFileRecord(id: UUID().uuidString, name: "record\(id)", data: .randomData(),
                      storageType: .local, storageFile: FileEntry(path: "file"),
                      linkState: .linked, syncState: .synced,
                      syncMetadata: FileSyncMetadata(currentVersion: 1, syncedVersion: 2, syncedMetadata: "meta"))
}
