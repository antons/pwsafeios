//
//  SafeSettingsMakePropsTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 13/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble

@testable import PwsafeIOS

class SafeSettingsMakePropsTest: QuickSpec {
    override func spec() {
        describe("SafeSettings MakeProps") {
            it("") {
                // Given
                let lastSave = Date()
                let state = SafeSettingsState(
                    timestampOfLastSave: lastSave,
                    whatPerformedLastSave: "Pwsafe",
                    databaseName: "Database Name",
                    databaseDescription: "Database Description")

                // When
                let props = makeProps(from: state, commands: MockSafeSettingsCommands())

                // Then
                let expectedMenu: [MenuSection] = [
                    MenuSection(items: [
                        MenuItem(title: "Change Password", onSelected: .nop)
                    ]),
                    MenuSection(items: [
                        MenuItem(title: "Last Save: \(lastSave)", onSelected: .nop),
                        MenuItem(title: "Name: Database Name", onSelected: .nop)
                    ])
                ]

                expect(props?.menu) == expectedMenu
            }
        }
    }
}

final class MockSafeSettingsCommands: SafeSettingsCommands {
    func changePassword() -> Command {
        return .nop
    }

    func dispose() -> Command {
        return .nop
    }
}
