//
//  MockError.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 04/09/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation

struct MockError: Error {}
