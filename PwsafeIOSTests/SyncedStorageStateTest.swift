//
//  SyncedStateStorageTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 18/06/2017.
//  Copyright © 2017 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa
@testable import PwsafeIOS

//swiftlint:disable function_body_length
class SyncedStorageStateTest: QuickSpec {
    override func spec() {
        describe("SyncedStorageState") {
            context("add file") {
                let operationId = UUID()
                
                let addFileParams = AddFileParams(
                    storageFile: FileEntry(path: "path"),
                    storageType: .dropbox,
                    data: .randomData(),
                    metadata: "meta")
                
                it("creates new add file") {
                    // Given
                    let initialState = SyncedStorageState()
                    
                    let operation = SyncedStorageOperation(
                        action: .addFile(addFileParams),
                        state: .new)
                    
                    // When
                    let action: SyncedStorageAction = .operation(.newOperation(operationId, .addFile(addFileParams)))
                    let nextState = SyncedStorageState.reduce(initialState, action)
                    
                    // Then
                    expect(nextState) == SyncedStorageState(
                        files: [:],
                        operations: [operationId: operation])
                }
                
                context("complete 'add file' operation") {
                    // Given
                    let operation = SyncedStorageOperation(
                        action: .addFile(addFileParams),
                        state: .new)

                    let existingFile = SyncedFile(
                        id: UUID().uuidString,
                        name: "path",
                        syncState: .notSynced,
                        linkState: .linked)

                    let initialState = SyncedStorageState(
                        files: [.dropbox: [existingFile]],
                        operations: [operationId: operation])
                    
                    let syncedFile = SyncedFile(
                        id: UUID().uuidString,
                        name: "path",
                        syncState: .notSynced,
                        linkState: .linked)
                    
                    it("adds file on success") {
                        // When
                        let completedAction = SyncedStorageAction.operation(.completeOperation(
                            operationId,
                            .addFileCompleted((.dropbox, syncedFile))))
                        
                        let nextState = SyncedStorageState.reduce(initialState, completedAction)
                        
                        // Then
                        let updatedOperation = operation
                            |> SyncedStorageOperation.lens.state *~ .completed
                        
                        expect(nextState) == SyncedStorageState(
                            files: [.dropbox: [existingFile, syncedFile]],
                            operations: [operationId: updatedOperation])
                    }

                    it("marks operation as failed") {
                        // When
                        let completedAction = SyncedStorageAction.operation(.completeOperation(
                            operationId, .failed(.unknownError)))
                        
                        let nextState = SyncedStorageState.reduce(initialState, completedAction)
                        
                        // Then
                        let updatedOperation = operation
                            |> SyncedStorageOperation.lens.state *~ .failed(.unknownError)
                        
                        let expected = SyncedStorageState(
                            files: [.dropbox: [existingFile]],
                            operations: [operationId: updatedOperation])
                        
                        expect(nextState) == expected
                    }
                }
            }
            
            context("delete file") {
                
            }
        }
    }
}
