//
//  FileManagerTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 9/3/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa
@testable import PwsafeIOS

//swiftlint:disable function_body_length force_try
class FileManagerTest: QuickSpec {
    override func spec() {
        let cacheDirectory = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true).first!
        var tempFileDir: String!
        var tempFilePath: String!

        let fileManager = FileManager()

        var testedManager: FileManagerProtocol!

        beforeEach {
            tempFileDir = cacheDirectory + "/" + UUID().uuidString
            tempFilePath = tempFileDir + "/" + UUID().uuidString

            try! fileManager.createDirectory(atPath: tempFileDir, withIntermediateDirectories: true, attributes: nil)

            testedManager = FileManager()
        }

        afterEach {
            try! fileManager.removeItem(atPath: tempFileDir)
        }

        describe("fetchData") {
            it("fetches data") {
                let data: Data = .randomData()
                let result = fileManager.createFile(atPath: tempFilePath, contents: data, attributes: nil)
                expect(result) == true

                let actualData = testedManager.fetchData(tempFilePath).fetchExpectedNextAndCompleted()
                expect(data) == actualData
            }

            it("sends error if no file found") {
                let observer = testedManager.fetchData(tempFilePath).startTestObserver()
                expect(observer.error) == FileManagerError.fileNotFound(path: tempFilePath)
            }
        }

        describe("create new file") {
            it("create file") {
                // When
                let data: Data = .randomData()
                testedManager.create(tempFilePath, data: data)
                    .startAndExpectCompleted()

                // Then
                let actualData = fileManager.contents(atPath: tempFilePath)
                expect(data) == actualData
            }

            it("file already exists") {
                // Given
                let data: Data = .randomData()
                let result = fileManager.createFile(atPath: tempFilePath, contents: data, attributes: nil)
                expect(result) == true

                // When
                let observer = testedManager.create(tempFilePath, data: data).startTestObserver()

                // Then
                expect(observer.error) == FileManagerError.fileAlreadyExists(path: tempFilePath)
            }
        }

        describe("update file") {
            it("updates") {
                // When
                let data = Data.randomData()
                testedManager.update(tempFilePath, data: data).startAndExpectCompleted()

                // Then
                let actualData = fileManager.contents(atPath: tempFilePath)
                expect(data) == actualData
            }

            it("removes existing file") {
                // Given
                let data: Data = .randomData()
                let result = fileManager.createFile(atPath: tempFilePath, contents: data, attributes: nil)
                expect(result) == true

                // When
                testedManager.remove(tempFilePath).startAndExpectCompleted()

                // Then
                expect(fileManager.fileExists(atPath: tempFilePath)) == false
            }

            it("removes non-existing file") {
                testedManager.remove(tempFileDir + "/" + UUID().uuidString)
                    .startAndExpectCompleted()
            }

        }

        describe("listFiles") {
            it("list files") {
                // Given

                func uniqueSubDir(_ dir: String) -> String {
                    return dir + "/" + UUID().uuidString
                }

                let tempFiles: [String] = [
                    uniqueSubDir(tempFileDir),
                    uniqueSubDir(tempFileDir),
                    uniqueSubDir(tempFileDir)
                ]

                for tempFile in tempFiles {
                    fileManager.createFile(atPath: tempFile, contents: .randomData(), attributes: nil)
                }

                // When
                let files = testedManager.listFiles(tempFileDir).fetchExpectedNextAndCompleted()

                // Then
                expect(tempFiles.sorted()) == files?.sorted()
            }
        }
    }
}

extension FileManagerError: Equatable {}

public func == (lhs: FileManagerError, rhs: FileManagerError) -> Bool {
    switch (lhs, rhs) {
    case (.fileNotFound(path: let lhsPath), .fileNotFound(path: let rhsPath)):
        return lhsPath == rhsPath

    case (.fileAlreadyExists(path: let lhsPath), .fileAlreadyExists(path: let rhsPath)):
        return lhsPath == rhsPath

    case (.unknownError(reason: let lhsError), .unknownError(reason: let rhsError)):
        return type(of: lhsError) == type(of: rhsError)

    case (.couldNotCreateFile, .couldNotCreateFile):
        return true

    default:
        return false
    }
}
