//
//  DecryptedSafe+TestValues.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 07/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation
import PwsafeSwift
@testable import PwsafeIOS

extension DecryptedSafe {
    static func makeSimple() -> DecryptedSafe {
        var safe = Pwsafe()

        let record1 = with(Record()) {
            $0.title = "record1"
        }

        safe[record1.uuid] = record1

        let record2 = with(Record()) {
            $0.title = "record2"
        }

        safe[record2.uuid] = record2

        let syncedFile = SyncedFile(
            id: "id",
            name: "safe",
            syncState: .synced,
            linkState: .linked)

        return DecryptedSafe(syncedFile: syncedFile, pwsafe: safe, password: "password")
    }
}
