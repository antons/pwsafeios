//
//  MockManageSafeCommands.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

@testable import PwsafeIOS

final class MockManageSafeCommands: ManageSafeCommands {
    func select(record: PwsafeRecord) -> Command {
        return .nop
    }

    func select(group: PwsafeGroup) -> Command {
        return .nop
    }

    func safeInfo() -> Command {
        return .nop
    }

    func addRecord() -> Command {
        return .nop
    }

    func searchTextUpdate(screenId: UUID) -> CommandWith<String?> {
        return .nop
    }

    func searchCancelled(screenId: UUID) -> Command {
        return .nop
    }

    func dispose(screenId: UUID) -> Command {
        return .nop
    }
}
