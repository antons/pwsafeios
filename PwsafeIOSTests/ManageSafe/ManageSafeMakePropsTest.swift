//
//  ManageSafeMakePropsSpec.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 06/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble

import PwsafeSwift
@testable import PwsafeIOS

class ManageSafeMakePropsTest: QuickSpec {
    override func spec() {
        describe("ManageSafe make props") {
            var decrypted: DecryptedSafe!

            let syncedFile = SyncedFile(
                id: "id",
                name: "safe",
                syncState: .synced,
                linkState: .linked)

            beforeEach {
                var safe = Pwsafe()

                let group1 = Group(segments: ["level1"])
                let group2 = Group(segments: ["level1", "level2"])

                let record1 = with(Record()) {
                    $0.title = "record1"
                    $0.group = group1
                }

                safe[record1.uuid] = record1

                let record2 = with(Record()) {
                    $0.title = "record2"
                    $0.group = group2
                }

                safe[record2.uuid] = record2

                decrypted = DecryptedSafe(syncedFile: syncedFile, pwsafe: safe, password: "password")
            }

            it("makes props with groups and records") {
                let screenId = UUID()
                let filter = SafeItemFilter(type: .all, selectedGroup: Pwsafe.rootGroup, searchText: nil)
                let screen = ManageSafeScreen(id: screenId, filter: filter)
                let state = ManageSafeState(decryptedSafe: decrypted, manageRecord: nil, screens: [screenId: screen])
                let commands = MockManageSafeCommands()

                // When
                let props = makeProps(from: state, screenId: screenId, commands: commands)

                // Then
                let expectedGroupItems = [
                    SafeRecordItem(title: "level1", onSelected: .nop)
                ]

                let expectedRecordItems = [
                    SafeRecordItem(title: "record1", onSelected: .nop),
                    SafeRecordItem(title: "record2", onSelected: .nop)
                ]

                expect(props?.sections) == [
                    ManageSafeController.Props.Section(
                        type: .groups,
                        items: expectedGroupItems),
                    ManageSafeController.Props.Section(
                        type: .records,
                        items: expectedRecordItems)
                ]
            }

            it("makes props with text search") {
                // Given
                let screenId = UUID()
                let filter = SafeItemFilter(type: .all, selectedGroup: Pwsafe.rootGroup, searchText: "record1")
                let screen = ManageSafeScreen(id: screenId, filter: filter)
                let state = ManageSafeState(decryptedSafe: decrypted, manageRecord: nil, screens: [screenId: screen])
                let commands = MockManageSafeCommands()

                // When
                let props = makeProps(from: state, screenId: screenId, commands: commands)

                // Then
                let expectedRecordItems = [
                    SafeRecordItem(title: "record1", onSelected: .nop),
                ]

                expect(props?.sections) == [
                    ManageSafeController.Props.Section(
                        type: .records,
                        items: expectedRecordItems)
                ]
            }

            it("makes props for selected group") {
                let group = Group(segments: ["level1"])
                let rootId = UUID()
                let screenId = UUID()
                let state = ManageSafeState(decryptedSafe: decrypted, manageRecord: nil, screens: [
                    rootId: ManageSafeScreen(id: rootId, filter: .all),
                    screenId: ManageSafeScreen(id: screenId, filter: .contained(in: group))
                    ])
                let commands = MockManageSafeCommands()

                // When
                let props = makeProps(from: state, screenId: screenId, commands: commands)

                // Then
                let expectedGroupItems = [
                    SafeRecordItem(title: "level2", onSelected: .nop)
                ]

                let expectedRecordItems = [
                    SafeRecordItem(title: "record1", onSelected: .nop)
                ]

                expect(props?.sections) == [
                    ManageSafeController.Props.Section(
                        type: .groups,
                        items: expectedGroupItems),
                    ManageSafeController.Props.Section(
                        type: .records,
                        items: expectedRecordItems)
                ]
            }

            it("makes props for empty state") {
                let decrypted = DecryptedSafe(syncedFile: syncedFile, pwsafe: Pwsafe(), password: "password")

                let screenId = UUID()
                let state = ManageSafeState(decryptedSafe: decrypted, manageRecord: nil, screens: [
                    screenId: ManageSafeScreen(id: screenId, filter: .all)
                    ])
                let commands = MockManageSafeCommands()

                // When
                let props = makeProps(from: state, screenId: screenId, commands: commands)

                // Then
                expect(props?.sections) == []
            }
        }
    }
}
