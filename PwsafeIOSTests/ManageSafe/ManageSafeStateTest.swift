//
//  ManageSafeStateTest.swift
//  PwsafeIOSTests
//
//  Created by Anton Selyanin on 09/05/2018.
//  Copyright © 2018 Anton Selyanin. All rights reserved.
//

import Foundation

import Quick
import Nimble

import PwsafeSwift
@testable import PwsafeIOS

class ManageSafeStateTest: QuickSpec {
    override func spec() {
        describe("ManageSafeSpec") {
            var decrypted: DecryptedSafe!

            beforeEach {
                var safe = Pwsafe()

                let group1 = Group(segments: ["level1"])
                let group2 = Group(segments: ["level1", "level2"])

                let record1 = with(Record()) {
                    $0.title = "record1"
                    $0.group = group1
                }

                safe[record1.uuid] = record1

                let record2 = with(Record()) {
                    $0.title = "record2"
                    $0.group = group2
                }

                safe[record2.uuid] = record2

                let syncedFile = SyncedFile(
                    id: "id",
                    name: "safe",
                    syncState: .synced,
                    linkState: .linked)

                decrypted = DecryptedSafe(syncedFile: syncedFile, pwsafe: safe, password: "password")
            }

            describe("Manage Record") {
                it("initializes manage record state") {
                    // Given
                    let screenId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                    // When
                    reduce(state: &state, action: OpenRecord(record: decrypted.pwsafe.records[0]))

                    // Then
                    expect(state.manageRecord) == ManageRecordState.view(record: decrypted.pwsafe.records[0])
                }

                it("initializes add record state") {
                    // Given
                    let screenId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                    // When
                    let recordId = UUID()
                    reduce(state: &state, action: AddRecord(recordId: recordId))

                    // Then
                    expect(state.manageRecord) == ManageRecordState.edit(record: PwsafeRecord(uuid: recordId))
                }

                it("deinits manage record state") {
                    // Given
                    let screenId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: ManageRecordState.view(record: decrypted.pwsafe.records[0]),
                        screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                    // When
                    reduce(state: &state, action: ManageRecordDisposed())

                    // Then
                    expect(state.manageRecord).to(beNil())
                }

                it("updates safe and viewed record") {
                    // Given
                    let screenId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: ManageRecordState.view(record: decrypted.pwsafe.records[0]),
                        safeSettings: SafeSettingsState(header: decrypted.pwsafe.header),
                        screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                    var record = decrypted.pwsafe.records[0]
                    record.title = "Updated title"
                    decrypted.pwsafe[record.uuid] = record
                    decrypted.pwsafe.header.databaseName = "Updated database name"

                    // When
                    reduce(state: &state, action: UpdateSafe(safe: decrypted))

                    // Then
                    expect(state.decryptedSafe) == decrypted
                    expect(state.manageRecord) == ManageRecordState.view(record: record)
                    expect(state.safeSettings) == SafeSettingsState(header: decrypted.pwsafe.header)
                }

                it("removes record") {
                    // Given
                    let screenId = UUID()
                    let recordId = decrypted.pwsafe.records[0].uuid

                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: ManageRecordState.view(record: decrypted.pwsafe.records[0]),
                        safeSettings: SafeSettingsState(header: decrypted.pwsafe.header),
                        screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                    // When
                    reduce(state: &state, action: AskToDeleteRecord(id: recordId))

                    // Then
                    expect(state.manageRecord?.shouldShowConfirmDeleteAlert) == true

                    // When
                    reduce(state: &state, action: DeleteRecord(id: recordId))

                    // Then
                    expect(state.manageRecord).to(beNil())
                }
            } // describe: "Manage Record"

            it("opens group") {
                // Given
                let rootId = UUID()
                var state = ManageSafeState(
                    decryptedSafe: decrypted,
                    manageRecord: nil,
                    screens: [rootId: ManageSafeScreen(id: rootId, filter: .all)])

                // When
                let group = Group(segments: ["level1"])
                let screenId = UUID()
                reduce(state: &state, action: OpenGroup(screenId: screenId, filter: .contained(in: group)))

                // Then
                expect(state.screens) == [
                    rootId: ManageSafeScreen(id: rootId, filter: .all),
                    screenId: ManageSafeScreen(id: screenId, filter: .contained(in: group))
                ]
            }

            it("closes group") {
                // Given
                let group = Group(segments: ["level1"])
                let rootId = UUID()
                let screenId = UUID()
                var state = ManageSafeState(
                    decryptedSafe: decrypted,
                    screens: [rootId: ManageSafeScreen(id: rootId, filter: .all),
                              screenId: ManageSafeScreen(id: screenId, filter: .contained(in: group))])

                // When
                reduce(state: &state, action: SafeDidClose(screenId: screenId))

                // Then
                expect(state.screens) == [rootId: ManageSafeScreen(id: rootId, filter: .all)]
            }

            describe("Safe Settings") {
                it("opens safe settings") {
                    // Given
                    let rootId = UUID()

                    decrypted.pwsafe.header.timestampOfLastSave = Date()
                    decrypted.pwsafe.header.whatPerformedLastSave = "Pwsafe Test"
                    decrypted.pwsafe.header.databaseName = "Database Name"
                    decrypted.pwsafe.header.databaseDescription = "Database Description"

                    let expectedTimestamp = decrypted.pwsafe.header.timestampOfLastSave

                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: nil,
                        screens: [rootId: ManageSafeScreen(id: rootId, filter: .all)])

                    // When
                    reduce(state: &state, action: OpenSafeSettings())

                    // Then
                    expect(state.safeSettings?.timestampOfLastSave) == expectedTimestamp
                    expect(state.safeSettings?.whatPerformedLastSave) == "Pwsafe Test"
                    expect(state.safeSettings?.databaseName) == "Database Name"
                    expect(state.safeSettings?.databaseDescription) == "Database Description"
                }

                it("closes safe settings") {
                    // Given
                    let rootId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: nil,
                        safeSettings: SafeSettingsState(),
                        screens: [rootId: ManageSafeScreen(id: rootId, filter: .all)])

                    // When
                    reduce(state: &state, action: CloseSafeSettings())

                    // Then
                    expect(state.safeSettings).to(beNil())
                }

                it("initializes and close 'change password' state") {
                    // Given
                    let rootId = UUID()
                    var state = ManageSafeState(
                        decryptedSafe: decrypted,
                        manageRecord: nil,
                        safeSettings: SafeSettingsState(),
                        screens: [rootId: ManageSafeScreen(id: rootId, filter: .all)])

                    // When
                    reduce(state: &state, action: OpenChangePassword())

                    // Then
                    expect(state.changePassword) == ChangePasswordState(minPasswordLength: 4, updatedPassword: "")

                    // When
                    reduce(state: &state, action: CloseChangePassword())

                    // Then
                    expect(state.changePassword).to(beNil())
                }

            } // describe: "Safe Settings"

            it("updates item filter") {
                // Given
                let screenId = UUID()
                var state = ManageSafeState(
                    decryptedSafe: decrypted,
                    manageRecord: nil,
                    safeSettings: SafeSettingsState(),
                    screens: [screenId: ManageSafeScreen(id: screenId, filter: .all)])

                // When
                reduce(state: &state, action: RecordFilterUpdated(screenId: screenId, text: "record1"))

                // Then
                let screen = state.screen(by: screenId)
                expect(screen?.filter) == SafeItemFilter(type: .all, selectedGroup: Pwsafe.rootGroup, searchText: "record1")
            }
        }
    }
}
