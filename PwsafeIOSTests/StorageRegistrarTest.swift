//
//  StorageRegistrarTest.swift
//  PwsafeIOS
//
//  Created by Anton Selyanin on 6/18/16.
//  Copyright © 2016 Anton Selyanin. All rights reserved.
//

import Foundation
import Quick
import Nimble
import ReactiveCocoa

@testable import PwsafeIOS

class StorageRegistrarTest: QuickSpec {
    override func spec() {
        describe("StorageRegistrar") {
            var registrar: StorageRegistrar!

            beforeEach {
                registrar = StorageRegistrar()
                assertEqual(registrar.rac_storages.value, [])
            }

            it("add storage to list") {
                // When
                let storage1 = InMemoryFileStorage()
                registrar.register(storage: storage1)

                // Then
                assertEqual(registrar.rac_storages.value, [storage1])
                expect(registrar.storage(type: .local)) === storage1
            }

            it("replaces storage for same type") {
                // Given
                let storage1 = InMemoryFileStorage()
                registrar.register(storage: storage1)

                // When
                let storage2 = InMemoryFileStorage()
                registrar.register(storage: storage2)

                // Then
                assertEqual(registrar.rac_storages.value, [storage2])
            }

            it("unregisters storage") {
                // Given
                let storage1 = InMemoryFileStorage()
                registrar.register(storage: storage1)

                // When
                registrar.unregister(storage: storage1)

                // Then
                assertEqual(registrar.rac_storages.value, [])
            }
        }
    }
}

private func assertEqual(_ lhs: [FileStorage], _ rhs: [FileStorage]) {
    let notSame = zip(lhs, rhs).filter(!==)
    expect(notSame.count) == 0
}
